<table>
    <thead>
    <tr>
        <!-- <th style="text-align:center; background-color:#63b8d5"><b></b></th> -->
        @foreach($columns as $p)
        <th style="text-align:center; background-color:#63b8d5"><b> {{ $p['label'] }}</b></th>
        @endforeach
        
    </tr>
    </thead>
    <tbody>
      @foreach($reports as $o)

        <tr >
            <td >
                @if($o['label'] == 'Total Project Direct Expenses' || $o['label'] == 'Total Other Expenses' || $o['label'] == 'Other Expenses' || $o['label'] == 'Project Direct Expenses')
                    <b> {{ $o['label'] }}   </b>
                @else   
                    {{ $o['label'] }}
                @endif

            </td>
            @foreach($columns as $p)
                 @if($p['id'] > 0)
                    <td >
                        @if($o['label'] != 'Other Expenses' && $o['label'] != 'Project Direct Expenses')
                            {{ isset($o['amount'][$p['id']]) ? $o['amount'][$p['id']] : '' }}
                        @else  
                            {{ '' }}
                        @endif
                    </td>
                @endif
            @endforeach
            <td>
                @if($o['label'] != 'Total Project Direct Expenses' && $o['label'] != 'Total Other Expenses' && $o['label'] != 'Other Expenses' && $o['label'] != 'Project Direct Expenses')
                     {{ isset($o['rowtotal']) ? $o['rowtotal'] : ''  }}   
                @elseif($o['label'] == 'Total Other Expenses')
                    <b> {{ isset($o['rowtotal']) ? $o['rowtotal'] : ''  }} </b>
                @elseif($o['label'] == 'Total Project Direct Expenses')
                    <b> {{ isset($o['rowtotal']) ? $o['rowtotal'] : ''  }} </b>
                @else   
                    {{ '' }}
                @endif

            </td>
             @if($o['label'] == 'Total Other Expenses')
                <td style="text-align:center; background-color:#63b8d5">
                    <b> {{ $OEoverall  }}% </b>
                </td>
            @elseif($o['label'] == 'Total Project Direct Expenses')
                    <td style="text-align:center; background-color:#63b8d5">
                    <b> {{ $PEoverall  }}% </b>
                </td>
            @else   
                <td></td>
            @endif

        </tr>
        
        @endforeach
      </tbody>
    </table>