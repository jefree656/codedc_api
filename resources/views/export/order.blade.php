<table>
    <thead>
    <tr>
        <th style="text-align:center; background-color:#63b8d5"><b>Project</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b>Material Name</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b>Qty</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b>Weight</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b>Length</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b>Width</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b>Height</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b>Total Cbm</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b>Total Weight</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b>Notes</b></th>
    </tr>
    </thead>
    <tbody>
      @foreach($orders as $o)

        <tr >
            <td >{{ $o['project_name'] }}</td>
            <td >{{ $o['material_name'] }}</td>
            <td >{{ $o['qty'] }}</td>
            <td >{{ $o['weight'] }}</td>
            <td >{{ $o['length'] }}</td>
            <td >{{ $o['width'] }}</td>
            <td >{{ $o['height'] }}</td>
            <td >{{ $o['totalcbm'] }}</td>
            <td >{{ $o['totalweight'] }}</td>
            <td >{{ $o['notes'] }}</td>
        </tr>
        
        @endforeach
      </tbody>
    </table>