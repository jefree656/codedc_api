<table>
    <thead>
    <tr>
        
        <th style="text-align:center; background-color:#63b8d5"><b> Status</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b> Task Name</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b> Category</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b> Material Name</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b> Qty</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b> Unit</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b> Unit Price</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b> Total</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b> Srp</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b> Srp Total</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b> Notes</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b> Requested Date</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b> Delivered Date</b></th>
        
        
    </tr>
    </thead>
    <tbody>
      @foreach($reports as $o)

        <tr >
            <td >{{ $o['order_status'] }}</td>
            <td >{{ $o['taskname'] }}</td>
            <td >{{ $o['category'] }}</td>
            <td >{{ $o['matname'] }}</td>
            <td >{{ $o['qty'] }}</td>
            <td >{{ $o['unit'] }}</td>
            <td >{{ $o['unit_price'] }}</td>
            <td >{{ $o['unit_price'] * $o['qty'] }}</td>
            <td >{{ $o['srp'] }}</td>
            <td >{{ $o['srp'] * $o['qty'] }}</td>
            <td >{{ $o['remarks'] }}</td>
            <td >{{ $o['requested_at'] }}</td>
            <td >{{ $o['delivered_at'] }}</td>
        </tr>
        
        @endforeach
      </tbody>
    </table>