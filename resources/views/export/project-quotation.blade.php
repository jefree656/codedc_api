<table>
    <tr>
        <td colspan="10" style="font-size: 22px; color: #63b8d5;"> <b>CODEDC</b></td>
    </tr>
    <tr></tr>

    <tr>
        <td colspan="6"><b>SUBJECT : {{ $project['_subject'] }}</b></td>
        <td colspan="4"><b>QUOTATION DATE : {{ $project['_qdate'] }}</b></td>
    </tr>
    <tr>
        <td colspan="6"><b>TOTAL AREA : {{ $project['_sqm'] }}sqm</b></td>
        <td colspan="4"><b>CONTRACTOR CONTACT : ANDREW PENG</b></td>
    </tr>
    <tr>
        <td colspan="6"><b>LOCATION : {{ $project['_address'] }}</b></td>
        <td colspan="4"><b>OFFICE # : 02-8732-4872</b></td>
    </tr>
    <tr>
        <td colspan="6"><b>ATTENTION : </b></td>
        <td colspan="4"><b>CONTACT # : 0917-810-3117 / 0919-002-6619</b></td>
    </tr>
    <tr>
        <td colspan="6"><b></b></td>
        <td colspan="4"><b>EMAIL@ : </b><b style="color: blue;">inquiry.code@gmail.com</b></td>
    </tr>

</table>

<table>
    <thead>
    <tr>
        <th style="text-align:center; background-color:#63b8d5"><b>品項序號<br>ITEM</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b>工作範圍<br>SCOPE OF WORKS</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b>数量<br>QTY</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b>单位<br>UNIT</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b>主材單價<br>Mater.Unit</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b>輔材單價<br>Mater. Unit</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b>材料金額<br>Mater. Sum</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b>人工單價<br>Labor Unit</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b>人工金額<br>Labor Sum</b></th>
        <th style="text-align:center; background-color:#63b8d5"><b>金额<br>AMOUNT</b></th>
    </tr>
    </thead>
    <tbody>

      @foreach($scope as $o)

        <tr>
            <td colspan="10" style="text-align:center; background-color: grey"><b>{{ $o['scope_name'] }}</b></td>
        </tr>
        @foreach($o['materials'] as $m)
            <tr>
                <td style="text-align:center;">{{ $m['index'] }}</td>
                <td>{{ $m['name'] }}<br>{{ $m['name_cn'] }}</td>
                <td style="text-align:center;">{{ $m['qty'] }}</td>
                <td style="text-align:center;">{{ $m['unit'] }}</td>
                <td style="text-align:center;">{{ $m['srp'] }}</td>
                <td></td>
                <td style="text-align:center;">{{ $m['sum'] }}</td>
                <td></td>
                <td></td>
                <td style="text-align:center;">{{ $m['sum'] }}</td>
            </tr>
        @endforeach
      @endforeach
        <tr>
            <td colspan="9" style="text-align:right; background-color: grey"><b>SUB TOTAL COST 項目工程費用 </b></td>
            <td><b>{{ $subtotal}}</b></td>
        </tr>
        <tr>
            <td colspan="9" style="text-align:right; background-color: grey"><b>TOTAL CONTRACT COST 項目工程合同</b></td>
            <td><b>{{ $subtotal}}</b></td>
        </tr>
      </tbody>
    </table>

    <table>
        <thead>
        <tr>
            <th colspan="10" style="text-align:center; background-color:#63b8d5"><b>NOTES 備註</b></th>
        </tr>
        </thead>
        <tbody>
            @foreach($notes as $n)
            <tr>
                <td style="text-align:center;"><b>{{ $n['step'] }}</b></td>
                <td colspan="9" style="word-wrap: break-word; height: 50px;"><b>{{ $n['detail'] }}<br>{{ $n['detail_cn'] }}</b></td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <table>
        <thead>
        <tr>
            <th colspan="10" style="text-align:center; background-color:#63b8d5"><b>SCHEDULE OF PAYMENT 付款時間表</b></th>
        </tr>
        </thead>
    </table>

    <table>
        <thead>
        <tr>
            <th colspan="2" style="text-align:center; background-color:#cccccc"><b>Description 概述</b></th>
            <th colspan="2" style="text-align:center; background-color:#cccccc"><b>Progressive Billing 階段性金額</b></th>
            <th colspan="2" style="text-align:center; background-color:#cccccc"><b>Request 請款金額</b></th>
            <th colspan="2" style="text-align:center; background-color:#cccccc"><b>Remaining 餘額</b></th>
            <th colspan="2" style="text-align:center; background-color:#cccccc"><b>Received 已收</b></th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="2" style="text-align: right;"><b>50% Down Payment 訂金</b></td>
                <td colspan="2"><b>{{ $subtotal/2 }}</b></td>
                <td colspan="2"></td>
                <td colspan="2"></td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: right;"><b>SUB-TOTAL 加總</b></td>
                <td colspan="2"><b>{{ $subtotal }}</b></td>
                <td colspan="2">0</td>
                <td colspan="2">0</td>
                <td colspan="2">0</td>
            </tr>
        </tbody>
    </table>

    <table>
        <thead>
        <tr>
            <th colspan="4" style="text-align:center; background-color:#63b8d5"><b>乙方 | CODEDC Construction Corp</b></th>
            <th colspan="6" style="text-align:center; background-color:#cccccc"><b>{{ $project['_subject'] }}</b></th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="4" style="height:40px"><b>Prepared By:</b></td>
                <td colspan="6" style="height:40px"><b>Confirmed By:</b></td>
            </tr>
        </tbody>
    </table>