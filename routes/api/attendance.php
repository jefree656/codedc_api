<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->group(function() {

    Route::get('', 'AttendanceController@index');
    
	Route::get('compute-payroll', 'AttendanceController@computePayroll');
	// Route::post('generate-payroll', 'AttendanceController@generatePayroll');

    Route::post('test-kernel', 'AttendanceController@testKernel');
});