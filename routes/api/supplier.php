<?php

use Illuminate\Http\Request;

	Route::get('manage-suppliers', 'SupplierController@manageSuppliers');

  Route::middleware('auth:api')->group(function() {

	Route::get('manage-suppliers-paginate/{perPage?}', 'SupplierController@manageSuppliersPaginate');

	Route::get('load-biller-data/{perPage?}', 'SupplierController@loadBillerData');
	Route::post('add-biller', 'SupplierController@addBiller');
	Route::patch('edit-biller/{id}', 'SupplierController@editBiller');

	Route::get('load-car-data/{perPage?}', 'SupplierController@loadCarData');
	Route::post('add-car', 'SupplierController@addCar');
	Route::patch('edit-car/{id}', 'SupplierController@editCar');

	Route::get('get-all-suppliers', 'SupplierController@getAllSuppliers');

	Route::post('/', 'SupplierController@store');

	Route::get('{id}', 'SupplierController@show');

	Route::patch('{id}', 'SupplierController@update');


});
