<?php

use Illuminate\Http\Request;

	Route::get('manage-projects', 'ProjectController@manageProject');

  Route::middleware('auth:api')->group(function() {

	Route::get('manage-projects-paginate/{perPage?}', 'ProjectController@manageProjectsPaginate');

	Route::get('search', 'ProjectController@projectSearch');

	Route::post('assign-role', 'ProjectController@assignRole');

	Route::post('add-task', 'ProjectController@addTask');
	Route::post('edit-task', 'ProjectController@editTask');
	Route::post('mark-task-complete', 'ProjectController@markTaskComplete');
	Route::post('mark-task-incomplete', 'ProjectController@markTaskIncomplete');
	Route::post('toggle-task-priority', 'ProjectController@toggleTaskPriority');
	Route::post('get-transactions', 'ProjectController@getProjectTransactions');
	Route::get('get-task/{task_id}', 'ProjectController@getTask');
	Route::get('get-task-with-orders/{task_id}', 'ProjectController@getTaskWithOrders');
	Route::get('get-order-details/{order_id}', 'ProjectController@getOrderDetail');
	Route::get('get-order-details2/{order_id}', 'ProjectController@getOrderDetail2');
	Route::post('get-order-detail-multiple', 'ProjectController@getOrderDetailMultiple');
	Route::post('get-order-detail-multiple2', 'ProjectController@getOrderDetailMultiple2');
    Route::post('export-project-quotation', 'ProjectController@exportProjectQuotation');

	Route::post('get-details', 'ProjectController@getProjectDetails');
	Route::get('get-profile/{id}', 'ProjectController@getProfile');
	Route::get('get-all-tasks', 'ProjectController@getAllTasks');
	Route::get('get-perteam-tasks', 'ProjectController@getPerTeamTasks');
	Route::get('get-perteam-count', 'ProjectController@getPerTeamCount');


	Route::post('bank-transfer', 'ProjectController@bankTransfer');


	Route::post('upload-task-images', 'ProjectController@uploadTaskImages');
	Route::post('delete-task-image', 'ProjectController@deleteTaskImage');
	Route::post('delete-material-image', 'ProjectController@deleteMaterialImage');
	Route::post('mark-as-read', 'ProjectController@markAsRead');
	
	Route::post('get-presets', 'ProjectController@getPresets');
	Route::post('get-presets2', 'ProjectController@getPresets2');
	Route::post('get-selected-presets', 'ProjectController@getSelectedPresets');

	Route::post('clear-mob', 'ProjectController@clearMob');
	Route::post('add-purchase-order', 'ProjectController@addPurchaseOrder');
	Route::post('add-purchase-order2', 'ProjectController@addPurchaseOrder2');
	Route::post('request-materials', 'ProjectController@requestMaterials');
	Route::post('request-low-qty', 'ProjectController@requestLowQty');
	Route::post('delete-task', 'ProjectController@deleteTask');
	Route::post('delete-comment', 'ProjectController@deleteComment');
	Route::get('all-users', 'ProjectController@allUsers');

	Route::post('/', 'ProjectController@store');

	Route::get('{id}', 'ProjectController@show');

	Route::patch('{id}', 'ProjectController@update');
});
