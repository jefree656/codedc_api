<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->group(function() {

    Route::post('/add-more-item','InventoryController@addMoreItem');
    Route::post('delete-site-order', 'InventoryController@deleteSiteOrder');
    Route::post('revert-spc', 'InventoryController@revertSPC');
    Route::post('/update-order-detail','InventoryController@updateOrderDetail');
    Route::post('/update-container-detail','InventoryController@updateContainerDetail');
    Route::post('/edit-inventory','InventoryController@editInventory');
    Route::post('/check-od-qty','InventoryController@checkOrderDetailQty');
    Route::post('/check-inventory-qty','InventoryController@checkInventoryQty');
    Route::post('/check-inventory-qty2','InventoryController@checkInventoryQty2');
    Route::post('/get-po-details','InventoryController@getPoDetails');
    Route::post('/to-payment','InventoryController@toPayment');
    Route::post('/load-to-container','InventoryController@loadToContainer');
    Route::post('/save-to-container','InventoryController@saveToContainer');
    Route::post('/rcv-china','InventoryController@rcvChina');
    Route::post('/to-prep','InventoryController@toPrep');
    Route::post('/to-ship','InventoryController@toShip');
    Route::post('/to-rcv','InventoryController@toRcv');
    Route::post('/transfer-delete','InventoryController@transferDelete');
    Route::post('/transfer-to-rcv','InventoryController@transferToRcv');
    Route::post('/freetransfer-to-rcv','InventoryController@freeTransferToRcv');
    Route::post('/freetransfer-rcv','InventoryController@freeTransferRcv');
    Route::post('rcv-transfer/{id}', 'InventoryController@rcvTransfer');
    Route::post('/mark-as-rcv','InventoryController@markAsRcv');

    Route::post('export-poc', 'InventoryController@exportPoc');
    Route::post('export-poc2', 'InventoryController@exportPoc2');

    Route::get('/get-inventory','InventoryController@getInventory');
    Route::get('/get-site-orders','InventoryController@getSiteOrders');
    Route::get('/get-toprep-orders','InventoryController@getToPrepOrders');
    Route::get('/get-toship-orders','InventoryController@getToShipOrders');
    Route::get('/get-toship-transfers','InventoryController@getToShipTransfers');
    Route::get('/get-toship-freetransfers','InventoryController@getToShipFreeTransfers');
    Route::get('/get-torcv-freetransfers','InventoryController@getToRcvFreeTransfers');
    Route::post('/add-free-transfer', 'InventoryController@addFreeTransfer');
    Route::get('/get-poc-payments','InventoryController@getPocPayments');
    Route::get('/get-torcv-orders','InventoryController@getToRcvOrders');
    Route::get('/get-completed-orders','InventoryController@getCompletedOrders');
    Route::get('/get-alll-orders','InventoryController@getAllOrders');
    Route::get('/get-all-categories','InventoryController@getAllCategories');
    Route::get('/get-all-drivers','InventoryController@getAllDrivers');
    Route::get('/get-all-cars','InventoryController@getAllCars');
    Route::post('add-material', 'InventoryController@addMaterial');
    Route::post('edit-material/{id}', 'InventoryController@editMaterial');
    Route::post('edit-order/{id}', 'InventoryController@editOrder');
    Route::post('transfer-materials', 'InventoryController@transferMaterials');
    Route::post('transfer-to-ship', 'InventoryController@transferToShip');
    Route::post('approve-material/{id}', 'InventoryController@approveMaterial');
    Route::post('approve-budget/{id}', 'InventoryController@approveBudget');
    Route::post('upload-payment-documents/{id}', 'InventoryController@uploadPaymentDocuments');
    Route::post('upload-file-documents', 'InventoryController@uploadFileDocuments');
    Route::post('upload-payment-file-documents', 'InventoryController@uploadPaymentFileDocuments');
    Route::post('receive-materials', 'InventoryController@receiveMaterials');
    Route::post('toship-materials', 'InventoryController@toshipMaterials');
    Route::post('change-category', 'InventoryController@changeCategory');
    Route::post('change-user', 'InventoryController@changeUser');
});
