<?php

use Illuminate\Http\Request;

	Route::get('manage-presets', 'PresetController@managePresets');

  Route::middleware('auth:api')->group(function() {

	Route::get('manage-presets-paginate/{perPage?}', 'PresetController@managePresetsPaginate');

	Route::get('search', 'PresetController@presetSearch');

	Route::post('assign-role', 'PresetController@assignRole');

	Route::get('manage-bundles-paginate/{perPage?}', 'PresetController@manageBundlesPaginate');
	Route::post('get-materials', 'PresetController@getMaterials');
	Route::post('load-all-presets', 'PresetController@loadAllPresets');
	Route::post('load-presets-selected', 'PresetController@loadPresetsSelected');
	Route::post('get-selected-materials', 'PresetController@getSelectedMaterials');
	Route::post('get-bundle-presets', 'PresetController@getBundlePresets');
	Route::post('update-preset-materials', 'PresetController@updatePresetMaterials');
	Route::post('update-bundle-presets', 'PresetController@updateBundlePresets');
	Route::post('add-bundle', 'PresetController@addBundle');

	Route::post('/', 'PresetController@store');

	Route::get('{id}', 'PresetController@show');

	Route::patch('{id}', 'PresetController@update');

});
