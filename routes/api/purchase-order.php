<?php

use Illuminate\Http\Request;

  Route::middleware('auth:api')->group(function() {

  	Route::get('/get-all-drafts','PurchaseOrdersController@getAllDrafts');
    Route::get('/get-all-paid-poc','PurchaseOrdersController@getAllPaidPoc');
    Route::get('/get-all-containers','PurchaseOrdersController@getAllContainers');
    Route::get('/get-all-containers2','PurchaseOrdersController@getAllContainers2');
    Route::post('/update-bank-transfer','PurchaseOrdersController@updateBankTransfer');
  	// payments
  	Route::post('pochina', 'PurchaseOrdersController@poChina');
    Route::post('rcv-purchase-order/{id}', 'PurchaseOrdersController@rcvPurchaseOrder');
    Route::post('rcv-spc/{id}', 'PurchaseOrdersController@rcvSpc');
    Route::post('rcv-poc/{id}', 'PurchaseOrdersController@rcvPoc');
  	Route::get('/get-all-purchase-orders','PurchaseOrdersController@getAllPurchaseOrders');
    Route::get('/get-all-spc-orders','PurchaseOrdersController@getAllSpcOrders');
  	Route::get('/get-all-banks','PurchaseOrdersController@getAllBanks');
  	Route::get('/get-all-billers','PurchaseOrdersController@getAllBillers');
  	Route::get('/get-payment-details/{id}','PurchaseOrdersController@getPaymentDetails');
    Route::get('/get-payment-details2/{id}','PurchaseOrdersController@getPaymentDetails2');
  	Route::get('/get-bank-history','PurchaseOrdersController@getBankHistory');
    Route::get('/get-all-payments','PurchaseOrdersController@getAllPayments');
    Route::get('/get-payment-info/{id}','PurchaseOrdersController@getPaymentInfo');
  	Route::get('/get-all-payment-categories','PurchaseOrdersController@getAllPaymentCategories');
  	Route::post('/change-payment-category', 'PurchaseOrdersController@changePaymentCategory');
  	Route::post('/add-payment', 'PurchaseOrdersController@addPayment');
    Route::post('/add-payment-multiple-bank', 'PurchaseOrdersController@addPaymentMultipleBank');
    Route::post('/add-payment2', 'PurchaseOrdersController@addPayment2');
  	Route::post('/edit-payment/{id}', 'PurchaseOrdersController@editPayment');
    Route::post('/edit-payment2/{id}', 'PurchaseOrdersController@editPayment2');
    Route::post('delete-payment', 'PurchaseOrdersController@deletePayment');
    Route::post('delete-payment-comment', 'PurchaseOrdersController@deletePaymentComment');
  	// end payments

	Route::post('/', 'PurchaseOrdersController@store');

	Route::get('{id}', 'PurchaseOrdersController@show');

	Route::patch('{id}', 'PurchaseOrdersController@update');
});
