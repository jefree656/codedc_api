<?php

use Illuminate\Http\Request;

Route::get('check-updated-cost', 'ReportController@checkUpdatedCost');

// Route::get('check-ond-handdocs/{id}', 'ReportController@checkOnHandDocs');

Route::middleware('auth:api')->group(function() {

	Route::get('get-pl-reports/{perPage}', 'ReportController@getPLReports');
	Route::get('get-material-reports/{perPage}', 'ReportController@getMaterialReports');
	Route::post('get-pl-details', 'ReportController@getPLDetails');
	Route::post('get-pl-details2', 'ReportController@getPLDetails2');
	Route::post('export-pl-report', 'ReportController@exportPLReport');
	Route::post('export-material-report', 'ReportController@exportMaterialReport');

});
