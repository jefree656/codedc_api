<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->group(function() {

 	Route::get('get-all-workers', 'AccountsController@getAllWorkers');
 	Route::post('/', 'AccountsController@store');

 	Route::get('get-cpanel-users', 'AccountsController@getCpanelUsers');

 	Route::get('{id}', 'AccountsController@show');

 	Route::patch('{id}', 'AccountsController@update');

 	Route::delete('{id}', 'AccountsController@destroy');

 	Route::get('get-worker/{id}', 'AccountsController@getWorker');

 	Route::post('add-new-worker', 'AccountsController@addNewWorker');
 	Route::patch('update-worker/{id}', 'AccountsController@updateWorker');

 	Route::post('save-worker-attendance/', 'AccountsController@saveWorkerAttendance');
 	Route::post('save-worker-attendance-multiple/', 'AccountsController@saveWorkerAttendanceMultiple');

 	Route::post('get-day-attendance/', 'AccountsController@getDayAttendance');

});