<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderSupplier extends Model
{

    protected $table = 'purchase_order_supplier';

    protected $fillable = ['supplier_id', 'needed_date', 'carrier', 'contact_person', 'contact_number', 'bank', 'bank_account_name', 'bank_account_number', 'notes'];

}
