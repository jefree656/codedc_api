<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankTransfer extends Model
{

    protected $table = 'bank_transfer';
    public $timestamps = false;

    protected $fillable = ['transfer_from', 'transfer_to', 'amount', 'rate', 'notes'];

}
