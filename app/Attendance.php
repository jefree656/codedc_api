<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    
    protected $table = 'workers_attendance';

    protected $fillable = ['user_id', 'project_id',  'day', 'month', 'year', 'time_in', 'time_out', 'timein', 'timeout', 'duration'];

    // public function worker() {
    // 	return $this->belongsTo('App\Worker', 'user_id', 'id');
    // }

}
