<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentDetails extends Model
{
    
    protected $table = 'payment_details';
    public $timestamps = false;
    protected $fillable = ['payment_id', 'category_id', 'project_id', 'description', 'amount'];

}
