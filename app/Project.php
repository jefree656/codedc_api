<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{

    protected $table = 'projects';

    protected $fillable = ['name', 'nick_name', 'address', 'address_link', 'contact_preson', 'contact_number', 'conact_email', 'sqm', 'est_finished_date', 'mob_first_date', 'admin_contact', 'admin_email', 'admin_name', 'remarks'];

}
