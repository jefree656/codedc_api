<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{

    protected $table = 'materials';
    public $timestamps = false;
    protected $fillable = ['name', 'name_cn', 'type', 'specs', 'unit',  'unit_price', 'unit_price_cn', 'retail_price','supplier_id', 'product_img', 'weight', 'length', 'width', 'height', 'remarks', 'tags'];

}
