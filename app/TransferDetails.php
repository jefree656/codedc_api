<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TransferDetails extends Model
{

	protected $table = 'transfer_details';
    public $timestamps = false;
    protected $fillable = ['transfer_id', 'material_id', 'qty'];

}
