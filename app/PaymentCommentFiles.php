<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentCommentFiles extends Model
{

    protected $table = 'payment_comment_files';
    public $timestamps = false;

    protected $fillable = ['comment_id', 'file_path','uploaded_at'];


}
