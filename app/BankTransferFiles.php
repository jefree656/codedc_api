<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankTransferFiles extends Model
{

    protected $table = 'bank_transfer_files';
    public $timestamps = false;

    protected $fillable = ['bank_transfer_id', 'file_path','uploaded_at'];


}
