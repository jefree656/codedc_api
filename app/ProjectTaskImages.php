<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectTaskImages extends Model
{

    protected $table = 'project_task_images';
    public $timestamps = false;

    protected $fillable = ['task_id', 'file_path','uploaded_at'];


}
