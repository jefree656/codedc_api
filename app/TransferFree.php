<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferFree extends Model
{

	protected $table = 'transfer_free';
    public $timestamps = false;
    protected $fillable = ['type', 'transfer_from', 'transfer_to', 'status', 'needed_date', 'needed_ime_from', 'needed_ime_to', 'notes', 'car_details', 'for delivery', 'delivery_date', 'delivered_by'];

}
