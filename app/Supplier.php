<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{

    protected $table = 'suppliers';
    public $timestamps = false;
    protected $fillable = ['full_name', 'nick_name', 'gender', 'notes', 'contact_person', 'contact_number', 'address', 'tin', 'email', 'bank', 'bank_account_name', 'bank_account_number'];

}
