<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BundlePreset extends Model
{

    protected $table = 'bundle_presets';
    public $timestamps = false;
    protected $fillable = ['bundle_id', 'preset_id'];

}
