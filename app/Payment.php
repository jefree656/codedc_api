<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
	use SoftDeletes;
 
    protected $dates = ['deleted_at'];
	protected $table = 'payments';
    public $timestamps = false;
    protected $fillable = ['ref', 'date', 'paytype', 'payout_type','checknum', 'type', 'bank_id', 'description',  'supplier_id', 'purchaser', 'purchase_order_id', 'biller_id', 'payment_category_id', 'total', 'payout', 'payout_return', 'delivery_fee', 'gas', 'toll', 'parking', 'status', 'is_active', 'with_order', 'requested_by', 'approved_by',  'completed_at', 'created_at'];



}
