<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectFinancePayments extends Model
{

    protected $table = 'project_finance_payments';

    protected $fillable = ['user_id', 'project_finance_id', 'payment_amount', 'notes'];

}
