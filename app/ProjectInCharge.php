<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectInCharge extends Model
{

    protected $table = 'project_in_charge';
    public $timestamps = false;
    protected $fillable = [ 'project_id', 'user_id'];


}
