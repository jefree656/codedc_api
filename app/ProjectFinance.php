<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectFinance extends Model
{

    protected $table = 'project_finance';

    protected $fillable = ['user_id', 'project_id', 'total', 'contact_person', 'contact_number', 'contact_email'];

}
