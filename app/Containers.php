<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Containers extends Model
{
    
    protected $table = 'containers';
    public $timestamps = false;
    protected $fillable = ['carrier', 'weight_capacity', 'status', 'type', 'eta', 'etd', 'payment_id', 'paid', 'paid2', 'processor', 'notes'];

}
