<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectWorker extends Model
{
    
    protected $table = 'project_workers';
    public $timestamps = false;

    protected $fillable = ['project_id', 'worker_id'];

    public function projects() {
    	return $this->belongsTo('App\Project', 'project_id', 'id');
    }

    public function worker() {
        return $this->belongsTo('App\Worker', 'worker_id', 'id');
    }

    public function attendance() {
    	return $this->hasMany('App\Attendance', 'user_id', 'worker_id')->orderBy('created_at', 'desc');
    }

}
