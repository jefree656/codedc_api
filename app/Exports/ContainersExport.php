<?php

namespace App\Exports;

use App\Order;
use App\OrderDetails;
use App\Material;
use App\Project;
use App\Containers;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use DB, Response;
use DateTime;

class ContainersExport implements FromView, WithEvents, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct(string $ids)
	{
	  //$this->ids = $ids;
	  $this->ids = $ids;
	}

    public function registerEvents(): array
	  {

	      return [

	          BeforeExport::class => function(BeforeExport $event) {
	            $event->writer->getProperties()->setCreator('Codedc')
	                ->setTitle("Purchase Orders")
	                ->setSubject("POC");
	          },

	          AfterSheet::class    => function(AfterSheet $event) {
	              $cellRange = 'A1:J1'; // All headers
	              $columns = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];

	              $sheet = $event->sheet->getDelegate();

	              $sheet = $event->sheet->getDelegate();
	              $sheet->getColumnDimension('A')->setAutoSize(false);
	              $sheet->getColumnDimension('A')->setWidth(40);

	              $sheet->getColumnDimension('B')->setAutoSize(false);
	              $sheet->getColumnDimension('B')->setWidth(40);

	              $sheet->getColumnDimension('C')->setAutoSize(false);
	              $sheet->getColumnDimension('C')->setWidth(10);

	              $sheet->getColumnDimension('D')->setAutoSize(false);
	              $sheet->getColumnDimension('D')->setWidth(10);

	              $sheet->getColumnDimension('E')->setAutoSize(false);
	              $sheet->getColumnDimension('E')->setWidth(10);

	              $sheet->getColumnDimension('F')->setAutoSize(false);
	              $sheet->getColumnDimension('F')->setWidth(10);

	              $sheet->getColumnDimension('G')->setAutoSize(false);
	              $sheet->getColumnDimension('G')->setWidth(10);

	              $sheet->getColumnDimension('H')->setAutoSize(false);
	              $sheet->getColumnDimension('H')->setWidth(10);

	              $sheet->getColumnDimension('I')->setAutoSize(false);
	              $sheet->getColumnDimension('I')->setWidth(10);

	              $sheet->getColumnDimension('J')->setAutoSize(false);
	              $sheet->getColumnDimension('J')->setWidth(50);

	          },
	      ];
	  }

	// public function orders($ids){

	//     // $response = DB::table('payments as p')
	//     //               ->select(DB::raw('
	//     //                   a.amount,a.type, a.created_at'))
	//     //                   ->whereIn('id', $ids)
	//     //                   ->orderBy('created_at','DESC')
	//     //                   ->get();

	//     // foreach($response as $s){
	//     //   $datetime = new DateTime($s->created_at);
	//     //   $getdate = $datetime->format('M d,Y');
	//     //   $s->amount = number_format($s->amount,2);


	//     // }


	//     return $response;
	//   }

    public function view(): View
    {

    	//$orders = $this->orders($this->ids);
    	\Log::info($this->ids);
    	if($this->ids != ''){
           $pay_ids = explode(',', $this->ids); //350
        }else{
           $pay_ids = [];
        }

        $orders = OrderDetails::whereIn('container_id', $pay_ids)->get();

        foreach($orders as $pr){
        		$mat = Material::findorfail($pr->product_id);
                $order = Order::findOrFail($pr->order_id);
                $proj = Project::findOrFail($order->project_id);
                if($order->project_id == 0){
                    $pr->project_name = 'Warehouse';
                }
                else{
                    $pr->project_name = $proj->name;
                }
                $pr->material_unit = $mat->unit;
                $pr->material_name = $mat->name.' ('.$mat->name_cn.')';
                $pr->totalcbm = floatval($pr->qty) * ( floatval($pr->length) * floatval($pr->width) * floatval($pr->height) );
                $pr->totalweight = floatval($pr->qty) * floatval($pr->weight);
                $pr->notes = $order->remarks;
        }

        return view('export.order', [
            'orders' => $orders
        ]);
    }
}
