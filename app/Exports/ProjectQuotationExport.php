<?php

namespace App\Exports;

use App\Order;
use App\OrderDetails;
use App\Material;
use App\Project;
use App\ProjectTask;
use App\QuotationNotes;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use DB, Response;
use DateTime;

class ProjectQuotationExport implements FromView, WithEvents, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct(string $id)
	{
	  //$this->ids = $ids;
	  $this->id = $id;
	}

    public function registerEvents(): array
	  {

	      return [

	          BeforeExport::class => function(BeforeExport $event) {
	            $event->writer->getProperties()->setCreator('Codedc')
	                ->setTitle("Quotation")
	                ->setSubject("Project");
	          },

	          AfterSheet::class    => function(AfterSheet $event) {
	              $cellRange = 'A1:J1'; // All headers
	              $columns = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];

	              $sheet = $event->sheet->getDelegate();

	              $sheet = $event->sheet->getDelegate();
	              $sheet->getColumnDimension('A')->setAutoSize(false);
	              $sheet->getColumnDimension('A')->setWidth(10);

	              $sheet->getColumnDimension('B')->setAutoSize(false);
	              $sheet->getColumnDimension('B')->setWidth(50);

	              $sheet->getColumnDimension('C')->setAutoSize(false);
	              $sheet->getColumnDimension('C')->setWidth(10);

	              $sheet->getColumnDimension('D')->setAutoSize(false);
	              $sheet->getColumnDimension('D')->setWidth(10);

	              $sheet->getColumnDimension('E')->setAutoSize(false);
	              $sheet->getColumnDimension('E')->setWidth(10);

	              $sheet->getColumnDimension('F')->setAutoSize(false);
	              $sheet->getColumnDimension('F')->setWidth(10);

	              $sheet->getColumnDimension('G')->setAutoSize(false);
	              $sheet->getColumnDimension('G')->setWidth(10);

	              $sheet->getColumnDimension('H')->setAutoSize(false);
	              $sheet->getColumnDimension('H')->setWidth(10);

	              $sheet->getColumnDimension('I')->setAutoSize(false);
	              $sheet->getColumnDimension('I')->setWidth(10);

	              $sheet->getColumnDimension('J')->setAutoSize(false);
	              $sheet->getColumnDimension('J')->setWidth(10);

	          },
	      ];
	  }

	// public function orders($ids){

	//     // $response = DB::table('payments as p')
	//     //               ->select(DB::raw('
	//     //                   a.amount,a.type, a.created_at'))
	//     //                   ->whereIn('id', $ids)
	//     //                   ->orderBy('created_at','DESC')
	//     //                   ->get();

	//     // foreach($response as $s){
	//     //   $datetime = new DateTime($s->created_at);
	//     //   $getdate = $datetime->format('M d,Y');
	//     //   $s->amount = number_format($s->amount,2);


	//     // }


	//     return $response;
	//   }

    public function view(): View
    {

    	$id = $this->id;

        $details = Project::findorfail($id);
        $q = QuotationNotes::orderBy('step')->get();

        $orders = Order::where('project_id', $id)->pluck('id');

        $od = OrderDetails::whereIn('order_id', $orders)->groupBy('preset_id')->get();

        $pt = ProjectTask::where('project_id', $id)->get();

        $scope = [];
        $additionals = [];
        $ctr = 0;
        $ctr2 = 0;
        $subtotal = 0;

        foreach($pt as $o){

                $checkOr = Order::where('task_id', $o->id)->first();
                if($checkOr){
                $scope[$ctr]['scope_name'] = $o->label;

                    $or = Order::where('task_id', $o->id)->pluck('id');
                    $pm = OrderDetails::whereIn('order_id', $or)->get();
                    $materials = [];
                    $ctr2 = 1;
                    foreach($pm as $p){
                        $mat = Material::findOrFail($p->product_id);
                        $od = OrderDetails::where('order_id', $p->order_id)
                                ->where('product_id', $mat->id)->get();
                        $mat->index = $ctr2;
                        $mat->name_cn = $mat->name_cn == null ? ' ' : $mat->name_cn;
                        $mat->qty = $od->sum('qty');
                        $mat->unit = $mat->unit;
                        $mat->srp = $mat->retail_price;
                        $mat->sum = $mat->retail_price * $od->sum('qty');
                        $subtotal += $mat->sum;
                        $materials[] = $mat;
                        $ctr2++;
                    }
                    $scope[$ctr]['materials'] = $materials;

                    $ctr++;
                }
        }

        $project = [];
        $project['_subject'] = $details->name .' (' .$details->nick_name. ')';
        $project['_sqm'] = $details->sqm;
        $project['_address'] = $details->address;
        $project['_qdate'] = date("m/d/Y").", ".date("l");


        return view('export.project-quotation', [
            'project' => $project,
            'notes' => $q,
            'scope' => $scope,
            'additionals' => $additionals,
            'subtotal' => $subtotal,

        ]);
    }
}
