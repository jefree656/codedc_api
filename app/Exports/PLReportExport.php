<?php

namespace App\Exports;

use App\Order;
use App\OrderDetails;
use App\Material;
use App\Project;
use App\Payments;
use App\PaymentCategories;
use App\PaymentDetails;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use DB, Response;
use DateTime;

class PLReportExport implements FromView, WithEvents, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct(object $data)
	{
	  //$this->ids = $ids;
	  $this->data = $data;
	}

    public function registerEvents(): array
	  {

	      return [

	          BeforeExport::class => function(BeforeExport $event) {
	            $event->writer->getProperties()->setCreator('Codedc')
	                ->setTitle("PL Reports")
	                ->setSubject("PL");
	          },

	          AfterSheet::class    => function(AfterSheet $event) {
	              $cellRange = 'B1:AB1'; // All headers
	              $columns = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];

	              $sheet = $event->sheet->getDelegate();

	              $sheet = $event->sheet->getDelegate();
	              $sheet->getColumnDimension('A')->setAutoSize(false);
	              $sheet->getColumnDimension('A')->setWidth(40);

	              $sheet->getColumnDimension('B')->setAutoSize(false);
	              $sheet->getColumnDimension('B')->setWidth(10);

	              $sheet->getColumnDimension('C')->setAutoSize(false);
	              $sheet->getColumnDimension('C')->setWidth(10);

	              $sheet->getColumnDimension('D')->setAutoSize(false);
	              $sheet->getColumnDimension('D')->setWidth(10);

	              $sheet->getColumnDimension('E')->setAutoSize(false);
	              $sheet->getColumnDimension('E')->setWidth(10);

	              $sheet->getColumnDimension('F')->setAutoSize(false);
	              $sheet->getColumnDimension('F')->setWidth(10);

	              $sheet->getColumnDimension('G')->setAutoSize(false);
	              $sheet->getColumnDimension('G')->setWidth(10);

	              $sheet->getColumnDimension('H')->setAutoSize(false);
	              $sheet->getColumnDimension('H')->setWidth(10);

	              $sheet->getColumnDimension('I')->setAutoSize(false);
	              $sheet->getColumnDimension('I')->setWidth(10);

	              $sheet->getColumnDimension('J')->setAutoSize(false);
	              $sheet->getColumnDimension('J')->setWidth(10);

	          },
	      ];
	  }

	// public function orders($ids){

	//     // $response = DB::table('payments as p')
	//     //               ->select(DB::raw('
	//     //                   a.amount,a.type, a.created_at'))
	//     //                   ->whereIn('id', $ids)
	//     //                   ->orderBy('created_at','DESC')
	//     //                   ->get();

	//     // foreach($response as $s){
	//     //   $datetime = new DateTime($s->created_at);
	//     //   $getdate = $datetime->format('M d,Y');
	//     //   $s->amount = number_format($s->amount,2);


	//     // }


	//     return $response;
	//   }

    public function view(): View
    {
    	// \Log::info($this->data['reports']);
        return view('export.report-pl', [
            'reports' => $this->data['reports'],
            'columns' => $this->data['columns'],
            'projects' => $this->data['projects'],
            'superoverall' => $this->data['superoverall'],
            'PEoverall' => $this->data['PEoverall'],
            'OEoverall' => $this->data['OEoverall'],
        ]);
    }
}
