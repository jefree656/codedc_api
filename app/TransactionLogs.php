<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionLogs extends Model
{

    protected $table = 'transaction_logs';
    public $timestamps = false;
    protected $fillable = ['bank_id','payment_id', 'transfer_id', 'user_id', 'type','rate', 'payment', 'deposit', 'balance', 'detail', 'detail_cn'];

}
