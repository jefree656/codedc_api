<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{

	protected $table = 'transfer';
    public $timestamps = false;
    protected $fillable = ['transfer_from', 'transfer_to', 'status', 'car_details', 'for delivery', 'delivery_date', 'delivered_by'];

}
