<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TransferFreeDetails extends Model
{

	protected $table = 'transfer_free_details';
    public $timestamps = false;
    protected $fillable = ['transfer_id', 'detail'];

}
