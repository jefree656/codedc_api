<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialImages extends Model
{

    protected $table = 'material_images';
    public $timestamps = false;

    protected $fillable = ['material_id', 'file_path','uploaded_at'];


}
