<?php

namespace App;
use App\ContactNumber;
use App\RoleUser;
use App\BranchUser;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Worker extends Authenticatable
{
    use Notifiable, SoftDeletes, HasApiTokens;

    protected $fillable = ['email', 'first_name', 'middle_name', 'last_name', 'position', 'basic_rate', 'ot_rate', 'op_allowance', 'meal_allowance', 'load_allowance', 'sss', 'pagibig', 'philhealth', 'cash_advance', 'wca', 'start_date', 'end_date'];


    public function attendance() {
        return $this->hasMany('App\Attendance', 'user_id', 'id');
    }

    public function department() {
        return $this->hasOne('App\DepartmentUser', 'user_id', 'id');
    }

    public function roles() {
        return $this->belongsToMany('App\Role', 'role_user', 'user_id', 'role_id');
    }

    public function rolesname()
    {
        return $this->belongsToMany(Role::class);
    }



    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        return !! $role->intersect($this->roles)->count();
    }

    public function permissions()
    {
        return $this->belongsToMany('App\Permission', 'permission_user', 'user_id', 'permission_id');
    }

    public function givePermission($permission)
    {
        if (is_string($permission)) {
            $this->permissions()->save(
                Permission::whereName($permission)->firstOrFail()
            );
        } elseif (is_array($permission)) {
            $this->permissions()
                ->attach($permission);
        }
    }

    public function hasPermission($permission)
    {
        if (is_string($permission)) {
            return $this->permissions->contains('name', $permission);
        }

        return !! $permission->intersect($this->permissions)->count();
    }

}
