<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentCategories extends Model
{
    
    protected $table = 'payment_categories';
    public $timestamps = false;
    protected $fillable = ['label', 'type'];

}
