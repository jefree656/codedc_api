<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use JustBetter\PaginationWithHavings\PaginationWithHavings;

class Inventory extends Model
{
    // use PaginationWithHavings;
    protected $table = 'inventory';
    public $timestamps = false;

    protected $fillable = ['material_id','category_id', 'qty','status','created_at','updated_at'];

    // public function units(){
    //     return $this->hasMany('App\InventoryPurchaseUnit', 'inv_id','inventory_id')
    //         ->leftJoin('inventory_unit', 'inventory_unit.unit_id', '=', 'inventory_purchase_unit.unit_id')
    //         ->orderBy("inventory_purchase_unit.id", "ASC");
    // }

    // public function selling(){
    //     return $this->hasMany('App\InventorySellingUnit', 'inv_id','inventory_id')
    //         ->leftJoin('inventory_unit', 'inventory_unit.unit_id', '=', 'inventory_selling_unit.unit_id')
    //         ->orderBy("inventory_selling_unit.id", "ASC");
    // }
}
