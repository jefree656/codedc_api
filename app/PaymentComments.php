<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentComments extends Model
{

    protected $table = 'payment_comments';

    protected $fillable = ['comment', 'payment_id', 'user_id', 'is_read'];


}
