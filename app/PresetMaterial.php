<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PresetMaterial extends Model
{

    protected $table = 'preset_materials';
    public $timestamps = false;
    protected $fillable = ['preset_id', 'material_id', 'ratio', 'reminder', 'section', 'unit'];

}
