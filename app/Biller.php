<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Biller extends Model
{

	protected $table = 'billers';
    public $timestamps = false;
    protected $fillable = ['name', 'account_number'];

}
