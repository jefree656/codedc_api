<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectTaskComments extends Model
{

    protected $table = 'project_task_comments';

    protected $fillable = ['comment', 'proj_task_id', 'user_id', 'is_read'];


}
