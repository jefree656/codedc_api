<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{

    protected $table = 'purchase_orders';

    protected $fillable = ['prepared_by', 'prepared_at', 'supplier_id', 'needed_date', 'status', 'carrier', 'contact_person', 'contact_number', 'bank', 'bank_account_name', 'bank_account_number', 'notes'];

}
