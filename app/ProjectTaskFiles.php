<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectTaskFiles extends Model
{

    protected $table = 'project_task_files';
    public $timestamps = false;

    protected $fillable = ['comment_id', 'file_path','uploaded_at'];


}
