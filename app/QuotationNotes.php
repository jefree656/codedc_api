<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotationNotes extends Model
{

    protected $table = 'quotation_notes';

    protected $fillable = ['detail', 'detail_cn'];

}
