<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectTask extends Model
{

    protected $table = 'project_tasks';

    protected $fillable = ['label', 'project_id', 'parent_id', 'assigned_to', 'progress_status', 'task_desc', 'type', 'status', 'prio', 'site_started', 'site_completion', 'is_read', 'start', 'end' , 'progress', 'dependent', 'last_mod'];


    public function tasks()
    {
        return $this->hasMany(self::class, 'parent_id', 'id')->with('tasks')
                ->orderBy('id', 'asc');
    }

}
