<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectOrderFiles extends Model
{

    protected $table = 'project_order_files';
    public $timestamps = false;

    protected $fillable = ['order_id', 'file_path','uploaded_at'];


}
