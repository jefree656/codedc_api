<?php

namespace App\Http\Controllers;

use App\Helpers\ArrayHelper;
use App\Inventory;
use App\InventoryLogs;
use App\InventoryLocation;
use App\Role;
use App\User;
use App\Material;
use App\MaterialImages;
use App\Order;
use App\OrderDetails;
use App\Project;
use App\Payment;
use App\PaymentComments;
use App\PaymentCommentFiles;
use App\PaymentDetails;
use App\PaymentFiles;
use App\Preset;
use App\ProjectTask;
use App\ProjectTaskComments;
use App\ProjectTaskFiles;
use App\ProjectOrderFiles;
use App\Categories;
use App\Cars;
use App\Drivers;
use App\Containers;
use App\Transfer;
use App\TransferDetails;
use App\TransferFree;
use App\TransferFreeDetails;
use App\BankAccounts;
use App\TransactionLogs;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Helpers\PageHelper;
use Auth;
use Carbon\Carbon;
use Storage;


//User Defined
use App\Exports\OrdersExport;
use App\Exports\ContainersExport;

class InventoryController extends Controller
{
    public function getInventory(Request $request, $perPage = 20){
        //jepandan
        $sort = $request->input('sort');
        $inv_id  = $request->input('inv_id');
        $perPage  = $request->input('perPage');
        $search  = $request->input('search');
        $tags  = $request->input('tags');

        // if($inv_id == 0){
        $srt = '';
        if($sort != ''){
            $srt = explode('-' , $sort);
            $srt = $srt[0];
        }

        $list = '';
        $mids = '';
        $tempTags = [];
        if($tags != '' && $tags != null){
            $tags = explode(',', $tags);
            // $collectTags = [];
            // foreach($tags as $t){
            //     $collectTags[]  = {'tags','LIKE', '%'.$t.'%'};
            // }
            $collectTags = [];
            $tagtemp = [];
            foreach ($tags as $value)
            {

                if(count($tempTags) == 0){
                    $tempTags = Material::where('tags','LIKE','%'.$value.'%')->pluck('id');
                }
                else{
                    $tagtemp = Material::whereIn('id', $tempTags)->where('tags','LIKE','%'.$value.'%')->pluck('id');
                    //\Log::info($tagtemp);
                    $tempTags = $tagtemp;
                }
            }
            //\Log::info($tempTags);
        }

        $item_ids =  Inventory::where('address_id', $inv_id)->where('status', 1)
                        ->when(count($tempTags) > 0, function ($q) use($tempTags){
                                return $q->whereIn('material_id',$tempTags);
                            })
                        ->orderBy('id','ASC')->pluck('material_id');
        //\Log::info(count($item_ids)); 
        //\Log::info($search); 
        //whereIn('id', $item_ids)
        if($sort != ''){
            $sort = explode('-' , $sort);
            if($sort[0] == 'name' || $sort[0] == 'type' || $srt == 'id') {
                    $list = Material::when($search != '' && count($item_ids) == 0 , function ($q) use($search){
                                return $q->where('name','LIKE', '%'.$search.'%')->orwhere('type','LIKE', '%'.$search.'%');
                            })
                            ->when(($search == '' || $search == null) && count($item_ids) > 0, function ($q) use($item_ids){
                                return $q->whereIn('id', $item_ids);
                            })
                            ->when(($search != '' && $search != null) && count($item_ids) > 0, function ($q) use($item_ids, $search){
                                return $q->whereIn('id', $item_ids)
                                            ->where(function ($query) use($search){
                                                $query->where('name','LIKE', '%'.$search.'%')
                                                ->orWhere(function($query_two) use($search){
                                                   $query_two->where('type', 'LIKE', '%'.$search.'%');
                                                   });
                                            });
                            })
                            ->orderBy($sort[0], $sort[1])->get()->pluck('id');
            }
        }
        //\Log::info($list);
        if($list != ''){
            $mids = "'".implode("', '", $list->toArray())."'";
        }

        //\Log::info('mids :'.$mids);

        $sort = $request->input('sort');
        $inv = Inventory::where('address_id', $inv_id)->where('status', 1)
                    ->when($list != '', function ($q) use($list) {
                                return $q->whereIn('material_id', $list);
                        })
                    ->when($list != '', function ($q) use($mids) {
                                return $q->orderByRaw(DB::raw("FIELD(material_id, $mids)"));
                        })
                    ->when($srt == 'qty' , function ($q) use($sort) {
                            $sort = explode('-' , $sort);
                            //if($sort[0] == 'qty') {
                                return $q->orderBy($sort[0], $sort[1]);
                            //}
                        })
                    ->get()
                    ->pluck('material_id');

        //\Log::info($inv);
        //$ids = implode(',', $inv);
        $ids = "'".implode("', '", $inv->toArray())."'";

        $itemsPaginated = Material::whereIn('id', $inv)
                            ->when($search != '' && count($item_ids) == 0 , function ($q) use($search){
                                return $q->where('name','LIKE', '%'.$search.'%')->orwhere('type','LIKE', '%'.$search.'%');
                            })
                            ->when(($search == '' || $search == null) && count($item_ids) > 0, function ($q) use($item_ids){
                                return $q->whereIn('id', $item_ids);
                            })
                            ->when(($search != '' && $search != null) && count($item_ids) > 0, function ($q) use($item_ids, $search){
                                return $q->whereIn('id', $item_ids)
                                            ->where(function ($query) use($search){
                                                $query->where('name','LIKE', '%'.$search.'%')
                                                ->orWhere(function($query_two) use($search){
                                                   $query_two->where('type', 'LIKE', '%'.$search.'%');
                                                   });
                                            });
                            })->orderByRaw(DB::raw("FIELD(id, $ids)"))
                            ->paginate($perPage);

        $itemsTransformed = $itemsPaginated
            ->getCollection()
            ->each(function($item) use($inv_id){
                    $invmat = Inventory::where('address_id', $inv_id)->where('status', 1)
                                ->where('material_id', $item['id'])
                                ->first();
                    $item['qty'] = 0;
                    if($invmat){
                        $item['qty'] = $invmat->qty;
                        $item['updated_at'] = $invmat->updated_at;
                        $item['files'] = MaterialImages::where('material_id', $item['id'])->get();
                    }
                
        })->toArray();

        $itemsTransformedAndPaginated = new \Illuminate\Pagination\LengthAwarePaginator(
            $itemsTransformed,
            $itemsPaginated->total(),
            $itemsPaginated->perPage(),
            $itemsPaginated->currentPage(), [
                'path' => \Request::url(),
                'query' => [
                    'page' => $itemsPaginated->currentPage()
                ]
            ]
        );

        return Response::json($itemsTransformedAndPaginated);
    }

    public function transferToShip(Request $request){
         $transfer_to = ($request->location_id == 'Warehouse' ? 0 : $request->location_id);
         $transfer_from = ($request->transfer_from == 'Warehouse' ? 0 : $request->transfer_from);
         $qtyArray = $request->qtyArray;
         $ids = $request->selectedmaterials;
         //$invs = Inventory::whereIn('id', $ids)->get();

        $trans = new Transfer;
        $trans->transfer_from = $transfer_from;
        $trans->transfer_to = $transfer_to;
        $trans->status = 'to ship';
        $trans->save();

        for($i = 0; $i < count($ids); $i++){
            $inv = Inventory::where('material_id',$ids[$i])->where('address_id',$transfer_from)->first();
            if($inv){
                $inv->qty -= $qtyArray[$i];
                $inv->save();
            }

            $transdet = new TransferDetails;
            $transdet->transfer_id = $trans->id;
            $transdet->material_id = $inv->material_id;
            $transdet->qty = $qtyArray[$i];
            $transdet->save();
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        // $response['data'] = $invs;

        return Response::json($response);
    }

    public function toshipMaterials(Request $request) {
        $arrayTest = [];
        $ids = $request->ids;
        $taskid = 0;

        //$orders = Order::whereIn('id',$ids)->groupBy('project_id')->get();
        foreach($request->project as $p) {
            foreach($p['orders'] as $o) {
                // foreach($o['presets'] as $pre) {
                    foreach($o['materials'] as $premat) {
                        $ordet = OrderDetails::findorfail($premat['order_detail_id']);
                        if($ordet){
                            if($ordet->qty == $premat['qty']){
                                $ordet->order_status = 'to ship';
                                $ordet->prepared_at = date("Y-m-d H:i:s");
                                $ordet->save();
                            }
                            else if($premat['qty'] > 0){
                                $qty = $ordet->qty - $premat['qty'];
                                $orderdet = new OrderDetails;
                                $orderdet->order_id = $ordet->order_id;
                                $orderdet->preset_id = $ordet->preset_id;
                                $orderdet->product_id = $ordet->product_id;
                                $orderdet->qty = $qty;
                                $orderdet->preset_unit = $ordet->preset_unit;
                                $orderdet->preset_qty = $ordet->preset_qty;
                                $orderdet->order_status = 'to prep';
                                $orderdet->unit_price = $ordet->unit_price;
                                $orderdet->total_price = $ordet->unit_price * $qty;
                                $orderdet->requested_at = $ordet->requested_at;
                                $orderdet->requested_by = $ordet->requested_by;
                                $orderdet->save();

                                $ordet->qty = $premat['qty'];
                                $ordet->order_status = 'to ship';
                                $ordet->prepared_at = date("Y-m-d H:i:s");
                                $ordet->save();
                            }

                            $order = Order::findorfail($ordet->order_id);
                            if($taskid == 0){
                                $taskid = $order->task_id;
                            }
                                $checkInv = Inventory::where('address_id', $order->project_id)
                                                ->where('material_id', $ordet->product_id)
                                                ->first();
                                if($checkInv){
                                    $checkInv->qty += $premat['qty'];
                                    $checkInv->save();
                                }
                                else{
                                    $newInv = new Inventory;
                                    $newInv->material_id = $ordet->product_id;
                                    $newInv->address_id = $order->project_id;
                                    $newInv->qty = $premat['qty'];
                                    $newInv->status = 1;
                                    $newInv->save();
                                }
                        }
                    }
                // }

                foreach($o['addmaterials'] as $add) {
                    $ordet = OrderDetails::findorfail($add['id']);
                    if($ordet){
                        if($ordet->qty == $add['qty']){
                            $ordet->order_status = 'to ship';
                            $ordet->delivered_at = date("Y-m-d H:i:s");
                            $ordet->received_by = Auth::User()->first_name.' '.Auth::User()->last_name;
                            $ordet->save();
                        }
                        else if($add['qty'] > 0){
                            $qty = $ordet->qty - $add['qty'];
                            $orderdet = new OrderDetails;
                            $orderdet->order_id = $ordet->order_id;
                            $orderdet->preset_id = $ordet->preset_id;
                            $orderdet->product_id = $ordet->product_id;
                            $orderdet->qty = $qty;
                            $orderdet->preset_unit = $ordet->preset_unit;
                            $orderdet->preset_qty = $ordet->preset_qty;
                            $orderdet->order_status = 'to prep';
                            $orderdet->unit_price = $ordet->unit_price;
                            $orderdet->total_price = $ordet->unit_price * $qty;
                            $orderdet->requested_at = $ordet->requested_at;
                            $orderdet->requested_by = $ordet->requested_by;
                            $orderdet->save();

                            $ordet->qty = $add['qty'];
                            $ordet->order_status = 'to ship';
                            $ordet->prepared_at = date("Y-m-d H:i:s");
                            $ordet->save();
                        }

                        $order = Order::findorfail($ordet->order_id);
                        if($taskid == 0){
                                $taskid = $order->task_id;
                            }
                        $checkInv = Inventory::where('address_id', $order->project_id)
                                        ->where('material_id', $ordet->product_id)
                                        ->first();
                        if($checkInv){
                            $checkInv->qty += $add['qty'];
                            $checkInv->save();
                        }
                        else{
                            $newInv = new Inventory;
                            $newInv->material_id = $ordet->product_id;
                            $newInv->address_id = $order->project_id;
                            $newInv->qty = $add['qty'];
                            $newInv->status = 1;
                            $newInv->save();
                        }
                    }
                }

                $toship = $this->updateProjectTaskStatus($o['id']);
            }
        }

        return json_encode([
            'success' => true,
            'message' => 'Successfully saved.'
        ]);
    }

    public function transferMaterials(Request $request) {
        $location_id = ($request->location_id == 'Warehouse' ? 0 : $request->location_id);
        // return $location_id;
        $qtyArray = $request->qtyArray;
        $ids = $request->selectedmaterials;

        $invs = Inventory::whereIn('id', $ids)->get();

        for($i = 0; $i < count($invs); $i++){
            $inv = Inventory::findorfail($invs[$i]->id);
            $inv->qty -= $qtyArray[$i];
            $inv->save();

            $checkInv = Inventory::where('address_id', $location_id)->where('material_id', $invs[$i]->material_id)->first();
            if($checkInv){
                $checkInv->qty += $qtyArray[$i];
                $checkInv->save();
            }
            else{
                $newInv = new Inventory;
                $newInv->material_id = $invs[$i]->material_id;
                $newInv->address_id = $location_id;
                $newInv->qty = $qtyArray[$i];
                $newInv->status = 0;
                $newInv->save();
            }
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        // $response['data'] = $invs;

        return Response::json($response);
    }

    public function checkOrderDetailQty(Request $request) {
        $validator = Validator::make($request->all(), [
            'data' => 'required',
            //'label' => 'required'
        ]);
        
        $mode = $request->mode;
        if($mode == 'Site Order'){
            $mode = 'site order';
        }

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;
        } else {
            $ids = [];
            $mat = $request->data;
            foreach($mat as $b){
                $ids[] = $b['id'];
            }
            //$od = Order::whereIn('id',$ids)->pluck('id');
            $od = OrderDetails::whereIn('id',$ids)->where('order_status', $mode)->get();
            //\Log::info($od->pluck('product_id'));
            //$inv = Inventory::where('status',1)->get();
            // \Log::info($od);
            //foreach ($inv as $k=>$v){       
                foreach($od as $a=>$b){
                    //\Log::info($b->product_id);
                    $inv = Inventory::where('status',1)->where('material_id',$b->product_id)->where('address_id', 0)->first();
                    $mat = Material::findorfail($inv->material_id);
                    // \Log::info($b->product_id." ".$v->material_id);
                    if($b->product_id == $inv->material_id){
                        $b->name = $mat->name;
                        $b->name_cn = $mat->name_cn;
                        //\Log::info($mat->name);
                        //\Log::info($inv->qty);
                        //\Log::info($b->qty);
                        $inv->qty -= $b->qty;
                        //\Log::info($inv->qty);
                        $b->enough = 1;
                        $b->toprep = $b->qty;
                        if($inv->qty <0){
                            $q = $inv->qty + $b->qty;
                            $b->enough = 0;
                            $b->toprep = $q > 0 ? $q : 0;
                        }
                    }

                }
            //}

            $response['status'] = 'Success';
            $response['data'] = $od;
            $response['code'] = 200;
        }

        return Response::json($response);
    }


    public function checkInventoryQty(Request $request) {
        $validator = Validator::make($request->all(), [
            'data' => 'required',
            //'label' => 'required'
        ]);
        
        $mode = $request->mode;
        if($mode == 'Site Order'){
            $mode = 'site order';
        }

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;
        } else {
            $ids = [];
            $mat = $request->data;
            foreach($mat as $b){
                $ids[] = $b['id'];
            }
            $od = Order::whereIn('id',$ids)->pluck('id');
            $od = OrderDetails::whereIn('order_id',$ids)->where('order_status', $mode)->get();
            //\Log::info($od->pluck('product_id'));
            //$inv = Inventory::where('status',1)->get();
            // \Log::info($od);
            //foreach ($inv as $k=>$v){       
                foreach($od as $a=>$b){
                    \Log::info($b->product_id);
                    $inv = Inventory::where('status',1)->where('material_id',$b->product_id)->where('address_id', 0)->first();
                    $mat = Material::findorfail($inv->material_id);
                    // \Log::info($b->product_id." ".$v->material_id);
                    if($b->product_id == $inv->material_id){
                        $b->name = $mat->name;
                        $b->name_cn = $mat->name_cn;
                        //\Log::info($mat->name);
                        //\Log::info($inv->qty);
                        //\Log::info($b->qty);
                        $inv->qty -= $b->qty;
                        //\Log::info($inv->qty);
                        $b->enough = 1;
                        $b->toprep = $b->qty;
                        if($inv->qty <0){
                            $q = $inv->qty + $b->qty;
                            $b->enough = 0;
                            $b->toprep = $q > 0 ? $q : 0;
                        }
                    }

                }
            //}

            $response['status'] = 'Success';
            $response['data'] = $od;
            $response['code'] = 200;
        }

        return Response::json($response);
    }

    public function checkInventoryQty2(Request $request) {
        $validator = Validator::make($request->all(), [
            'data' => 'required',
            //'label' => 'required'
        ]);
        

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;
        } else {
            $ids = [];
            $mat = $request->data;
            foreach($mat as $b){
                $ids[] = $b['id'];
            }
            $od = Order::whereIn('id',$ids)->pluck('id');
            $od = OrderDetails::whereIn('order_id',$ids)->where('order_status', 'to prep')->get();
            //\Log::info($od->pluck('product_id'));
            $inv = Inventory::where('status',1)->get();
            // \Log::info($od);
            //foreach ($inv as $k=>$v){       
                foreach($od as $a=>$b){
                    $inv = Inventory::where('status',1)->where('material_id',$b->product_id)->where('address_id', 0)->first();
                    $mat = Material::findorfail($inv->material_id);
                    // \Log::info($b->product_id." ".$v->material_id);
                    if($b->product_id == $inv->material_id){
                        $b->name = $mat->name;
                        $b->name_cn = $mat->name_cn;
                        //\Log::info($mat->name);
                        //\Log::info($inv->qty);
                        //\Log::info($b->qty);
                        $inv->qty -= $b->qty;
                        //\Log::info($inv->qty);
                        $b->enough = 1;
                        $b->toprep = $b->qty;
                        if($inv->qty <0){
                            $q = $inv->qty + $b->qty;
                            $b->enough = 0;
                            $b->toprep = $q > 0 ? $q : 0;
                        }
                    }

                }
            //}

            $response['status'] = 'Success';
            $response['data'] = $od;
            $response['code'] = 200;
        }

        return Response::json($response);
    }

    public function revertSPC(Request $request){
        $id = $request->id;
        $order = Order::where('id',$id)->first();
        $paymentIds = OrderDetails::where('order_id',$id)->where('order_status', 'SPC')->pluck('payment_id');

        // update order details order_status
        $data = array('order_status' => 'site order', 'payment_id' => null);
        DB::table('order_details')->where('order_id',$id)->where('order_status', 'SPC')->update($data);

        // delete payment
        $payment = Payment::whereIn('id', $paymentIds)->delete();

        // delete transaction logs
        $translogs = TransactionLogs::whereIn('payment_id', $paymentIds)->get();
        foreach($translogs as $t){
            $bank = BankAccounts::where('id', $t->bank_id)->first();
            $bank->balance = $bank->balance + $t->payment - $t->deposit;
            $bank->save();
        }
        $translogs = TransactionLogs::whereIn('payment_id', $paymentIds)->delete();

        // update task status
        $data2 = array('status' => 'site order');
        DB::table('project_tasks')
            ->where('project_id', $order->project_id)
            ->where('id', $order->task_id)
            ->update($data2);
        //$res2=ProjectTask::where('parent_id',$id)->delete();
        $response['status'] = 'Success';
        $response['code'] = 200;
        return Response::json($response);
    }

    public function deleteSiteOrder(Request $request){
        $id = $request->id;
        $order=Order::where('id',$id)->first();
        $res2 = OrderDetails::where('order_id',$id)->where('order_status', 'site order')->delete();
        $orderExist = OrderDetails::where('order_id',$id)->first();
        $task_id = $order->task_id;
        $project_id = $order->project_id;

        if(!$orderExist){
            $res=Order::where('id',$id)->delete();
        }
        $this->updateProjectTaskStatus2($id, $task_id, $project_id);

        // $data = array('status' => '');

        // DB::table('project_tasks')
        //     ->where('project_id', $order->project_id)
        //     ->where('id', $order->task_id)
        //     ->update($data);
        //$res2=ProjectTask::where('parent_id',$id)->delete();
        $response['status'] = 'Success';
        $response['code'] = 200;
        return Response::json($response);
    }

    public function getPoDetails(Request $request) {
        $validator = Validator::make($request->all(), [
            'data' => 'required',
            //'label' => 'required'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;
        } else {
            $ids = [];
            $mats = $request->data;
            $ctr = 0;
            $mat = [];
            foreach($mats as $b){
                $ids[] = $b['id'];
                //$po = Order::findOrFail($id);
            }
            //if( $po ) {
                $pod = OrderDetails::whereIn('id', $ids)->groupBy('product_id')->where('order_status','site order')->get();
                $addmat = $pod->pluck('product_id');
                //$addmat = $pod->pluck('product_id');

                
                    foreach($pod as $p){
                        
                        $pur = OrderDetails::whereIn('id', $ids)->where('product_id',$p->product_id)->where('order_status','site order')->get();
                        foreach($pur as $pr){
                            $mmm = Material::findorfail($pr->product_id);
                            $po = Order::findOrFail($pr->order_id);
                            $ppp = Project::findOrFail($po->project_id);
                            if($po->project_id == 0){
                                $pr->project_id = $po->project_id;
                                $pr->project_name = 'Warehouse';
                            }
                            else{
                                $pr->project_id = $po->project_id;
                                $pr->project_name = $ppp->name;
                            }
                            $pr->unit = $mmm->unit;
                            // $pr->weight = $mmm->weight;
                        }
                        $mat[$ctr]['material_name'] = $mmm->name;
                        $mat[$ctr]['material_id'] = $p->product_id;
                        $mat[$ctr]['items'] = $pur;
                        $ctr++;
                    }
            //}
        //}
            //$od = Order::whereIn('id',$ids)->pluck('id');
            //$od = OrderDetails::whereIn('order_id',$ids)->get();

            $response['status'] = 'Success';
            $response['data'] = [
                'po' => $po,
                'addmat' => $addmat,
                'mat' => $mat,
            ];
            $response['code'] = 200;
        }

        return Response::json($response);
    }

    public static function updateProjectTaskStatus($id){
        $status = null; // empty

        $order = Order::where('id', $id)->first();
        if($order){
            $order_ids = Order::where('project_id', $order->project_id)->where('task_id', $order->task_id)->pluck('id');

            $mob = DB::table('order_details')
                ->select('*')
                ->whereIn('order_id', $order_ids)
                ->where('order_status', 'mob')
                ->count();

            $siteOrders = DB::table('order_details')
                ->select('*')
                ->whereIn('order_id', $order_ids)
                ->where('order_status', 'site order')
                ->count();

            $toPrep = DB::table('order_details')
                ->select('*')
                ->whereIn('order_id', $order_ids)
                ->where('order_status', 'to prep')
                ->count();

            $toShip = DB::table('order_details')
                ->select('*')
                ->whereIn('order_id', $order_ids)
                ->where('order_status', 'to ship')
                ->count();

            $toRcv = DB::table('order_details')
                ->select('*')
                ->whereIn('order_id', $order_ids)
                ->where('order_status', 'to received')
                ->count();

            $completed = DB::table('order_details')
                ->select('*')
                ->whereIn('order_id', $order_ids)
                ->where('order_status', 'completed')
                ->count();

            $po = DB::table('order_details')
                ->select('*')
                ->whereIn('order_id', $order_ids)
                ->count();

            if($completed > 0){
                $status = "completed";
            }
            if($toRcv > 0){
                $status = "to received";
            }
            if($toShip > 0){
                $status = "to ship";
            }
            if($toPrep > 0){
                $status = "to prep";
            }
            if($siteOrders > 0){
                $status = "site order";
            }
            if($mob > 0){
                $status = "mob";
            }
            if($po == 0){
                $status = "purchase order";
            }
        }

        // if($completed == 0 && $toRcv == 0 && $toShip ==0 && $toPrep ==0 && $siteOrders ==0){
        //     $status = 'purchase order';
        // }

        $data = array('status' => $status);

        DB::table('project_tasks')
            ->where('project_id', $order->project_id)
            ->where('id', $order->task_id)
            ->update($data);
    }

    public static function updateProjectTaskStatus2($id, $task_id, $project_id){
        $status = null; // empty

        $order = Order::where('id', $id)->first();
        if($order){
            $order_ids = Order::where('project_id', $order->project_id)->where('task_id', $order->task_id)->pluck('id');

            $mob = DB::table('order_details')
                ->select('*')
                ->whereIn('order_id', $order_ids)
                ->where('order_status', 'mob')
                ->count();

            $siteOrders = DB::table('order_details')
                ->select('*')
                ->whereIn('order_id', $order_ids)
                ->where('order_status', 'site order')
                ->count();

            $toPrep = DB::table('order_details')
                ->select('*')
                ->whereIn('order_id', $order_ids)
                ->where('order_status', 'to prep')
                ->count();

            $toShip = DB::table('order_details')
                ->select('*')
                ->whereIn('order_id', $order_ids)
                ->where('order_status', 'to ship')
                ->count();

            $toRcv = DB::table('order_details')
                ->select('*')
                ->whereIn('order_id', $order_ids)
                ->where('order_status', 'to received')
                ->count();

            $completed = DB::table('order_details')
                ->select('*')
                ->whereIn('order_id', $order_ids)
                ->where('order_status', 'completed')
                ->count();

            $po = DB::table('order_details')
                ->select('*')
                ->whereIn('order_id', $order_ids)
                ->count();

            if($completed > 0){
                $status = "completed";
            }
            if($toRcv > 0){
                $status = "to received";
            }
            if($toShip > 0){
                $status = "to ship";
            }
            if($toPrep > 0){
                $status = "to prep";
            }
            if($siteOrders > 0){
                $status = "site order";
            }
            if($mob > 0){
                $status = "mob";
            }
            if($po == 0){
                $status = "purchase order";
            }
        }

        // if($completed == 0 && $toRcv == 0 && $toShip ==0 && $toPrep ==0 && $siteOrders ==0){
        //     $status = 'purchase order';
        // }

        $data = array('status' => $status);

        DB::table('project_tasks')
            ->where('project_id', $project_id)
            ->where('id', $task_id)
            ->update($data);
    }

    public function toPayment(Request $request) {
        $validator = Validator::make($request->all(), [
            'data' => 'required',
            //'label' => 'required'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;
        } else {
            $ids = [];
            $mats = $request->data;
            $ctr = 0;
            $mat = [];
            foreach($mats as $b){
                $ids[] = $b['id'];
            }
            $ordet = OrderDetails::whereIn('id', $ids)->pluck('order_id');
            $orders = Order::whereIn('id', $ordet)->get();
            foreach($orders as $o){
                $pod = OrderDetails::whereIn('id', $ids)->where('order_id', $o->id)->where('order_status','site order')->get();
                $addmat = $pod->pluck('product_id');
                $totalsrp = $pod->sum('total_price');

                $ctrPay = Payment::whereDate('created_at', Carbon::today())->where('paytype',$request->type)->count();
                $ctr = str_pad($ctrPay+1, 3, '0', STR_PAD_LEFT); 
                $pay = new Payment;
                $pay->ref = $request->type.date("mdy").$ctr;
                $pay->date = null;
                $pay->paytype = $request->type;
                $pay->type = 'Expense';
                $pay->description = null;
                $pay->payout_type = null;
                $pay->payout = 0;
                $pay->total = $totalsrp;
                $pay->bank_id = $request->type == 'SPC' ? 1 : null;
                $pay->supplier_id = 0;
                $pay->biller_id = 0;
                $pay->payment_category_id = null;
                $pay->purchase_order_id = null;
                $pay->purchaser = null;
                $pay->is_active = 1;
                $pay->with_order = 1;
                $pay->status = 'for approval';
                $pay->save();

                foreach($pod as $p){
                    $ordet = OrderDetails::findOrFail($p->id);    
                    $ordet->payment_id = $pay->id; 
                    $ordet->prepared_by = Auth::User()->first_name.' '.Auth::User()->last_name; 
                    $ordet->order_status = $request->type;  
                    $ordet->save(); 
                }
            }

            $response['status'] = 'Success';
            $response['data'] = $pod;
            $response['code'] = 200;
        }

        return Response::json($response);
    }

    public function loadToContainer(Request $request) {
        if($request->container == 'New Container' || $request->container == 'Air Cargo' || $request->container == 'Shipping'){
            $payid = 0;
            if($request->container == 'New Container'){
                $totalsrp = 0;

                $ctrPay = Payment::whereDate('created_at', Carbon::today())->where('paytype','POC')->count();
                $ctr = str_pad($ctrPay+1, 3, '0', STR_PAD_LEFT); 
                $pay = new Payment;
                $pay->ref = 'POC'.date("mdy").$ctr;
                $pay->date = null;
                $pay->paytype = 'POC';
                $pay->type = 'Expense';
                $pay->description = '*Fee*';
                $pay->payout_type = null;
                $pay->payout = 0;
                $pay->total = 1000;
                $pay->bank_id = null;
                $pay->supplier_id = 0;
                $pay->biller_id = 0;
                $pay->payment_category_id = null;
                $pay->purchase_order_id = null;
                $pay->purchaser = null;
                $pay->is_active = 1;
                $pay->with_order = 0;
                $pay->status = 'for approval';
                $pay->save();

                $payid = $pay->id;

                $orderdet = new PaymentDetails;
                $orderdet->payment_id = $pay->id;
                $orderdet->project_id = 0;
                $orderdet->category_id = 3;
                $orderdet->amount = 1000;
                $orderdet->description = 'Shipping Fee';
                $orderdet->save();
            }
            $con = new Containers;
            $con->status = 'for loading';
            $con->weight_capacity = $request->weight_limit;
            $con->type = $request->container == 'New Container' ? 'Container' : $request->container;
            $con->payment_id = $payid;
            $con->etd = date("Y-m-d"); 
            $con->processor = Auth::User()->first_name ;
            $con->save();
        }
        else{
            $con = Containers::findorfail($request->container);
        }

        foreach($request->materials as $m){
            $ordet = OrderDetails::findorfail($m['id']);
            $ordet->container_id = $con->id;
            $ordet->po_status = 'READY';
            $ordet->save();
        }

        $response['status'] = 'Success';;
        $response['code'] = 200;
        return Response::json($response);
    }

    public function saveToContainer(Request $request) {
        if($request->container == 'New Container' || $request->container == 'Air Cargo' || $request->container == 'Shipping'){
            $payid = 0;
            if($request->container == 'New Container'){
                $totalsrp = 0;

                $ctrPay = Payment::whereDate('created_at', Carbon::today())->where('paytype','POC')->count();
                $ctr = str_pad($ctrPay+1, 3, '0', STR_PAD_LEFT); 
                $pay = new Payment;
                $pay->ref = 'POC'.date("mdy").$ctr;
                $pay->date = null;
                $pay->paytype = 'POC';
                $pay->type = 'Expense';
                $pay->description = '*Fee*';
                $pay->payout_type = null;
                $pay->payout = 0;
                $pay->total = 1000;
                $pay->bank_id = null;
                $pay->supplier_id = 0;
                $pay->biller_id = 0;
                $pay->payment_category_id = null;
                $pay->purchase_order_id = null;
                $pay->purchaser = null;
                $pay->is_active = 1;
                $pay->with_order = 0;
                $pay->status = 'for approval';
                $pay->save();

                $payid = $pay->id;

                $orderdet = new PaymentDetails;
                $orderdet->payment_id = $pay->id;
                $orderdet->project_id = 0;
                $orderdet->category_id = 3;
                $orderdet->amount = 1000;
                $orderdet->description = 'Shipping Fee';
                $orderdet->save();
            }
            $con = new Containers;
            $con->status = 'for loading';
            $con->weight_capacity = $request->weight_limit;
            $con->type = $request->container == 'New Container' ? 'Container' : $request->container;
            $con->payment_id = $payid;
            $con->etd = date("Y-m-d"); 
            $con->processor = Auth::User()->first_name ;
            $con->save();
        }
        else{
            $con = Containers::findorfail($request->container);
        }

        foreach($request->materials as $m){
            $ordet = OrderDetails::findorfail($m['id']);
            $ordet->container_id = $con->id;
            $ordet->po_status = 'HOLD';
            $ordet->save();
        }

        $response['status'] = 'Success';;
        $response['code'] = 200;
        return Response::json($response);
    }

    public function rcvChina(Request $request) {
        $con = Containers::findOrFail($request->materials['id']);

        $totalsrp = 0;

        $ctrPay = Payment::whereDate('created_at', Carbon::today())->where('paytype','POC')->count();
        $ctr = str_pad($ctrPay+1, 3, '0', STR_PAD_LEFT); 
        $pay = new Payment;
        $pay->ref = 'POC'.date("mdy").$ctr;
        $pay->date = null;
        $pay->paytype = 'POC';
        $pay->type = 'Expense';
        $pay->description = '*PH Fee*';
        $pay->payout_type = null;
        $pay->payout = 0;
        $pay->total = 1000;
        $pay->bank_id = null;
        $pay->supplier_id = 0;
        $pay->biller_id = 0;
        $pay->payment_category_id = null;
        $pay->purchase_order_id = null;
        $pay->purchaser = null;
        $pay->is_active = 1;
        $pay->with_order = 0;
        $pay->status = 'for approval';
        $pay->save();

        $payid = $pay->id;

        $orderdet = new PaymentDetails;
        $orderdet->payment_id = $pay->id;
        $orderdet->project_id = 0;
        $orderdet->category_id = 3;
        $orderdet->amount = 1000;
        $orderdet->description = 'Shipping Fee';
        $orderdet->save();

        if($con->status == 'for loading'){
            $con->carrier = $request->materials['carrier'];
            $con->eta = $request->materials['eta'];
            $con->status = 'to_received_ph';
            $con->payment_id = $payid;
            $con->save();
        }
        else{
            $con->carrier = $request->materials['carrier'];
            $con->eta = $request->materials['eta'];
            $con->status = 'to_received_ph';
            $con->payment_id = $payid;
            $con->save();
        }

        $response['status'] = 'Success';;
        $response['data'] = $con;
        $response['code'] = 200;
        return Response::json($response);
    }

    public function toPrep(Request $request) {
        $validator = Validator::make($request->all(), [
            'data' => 'required',
            //'label' => 'required'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;
        } else {
            // $ids = [];
            $mat = $request->data;
            $order_ids = [];
            foreach($mat as $b){
                $order_ids[] = $b['id'];
                $od = OrderDetails::where('id',$b['id'])->first();
                if($b['enough'] ){
                    $od->order_status = "to prep";
                    $od->prepared_by = Auth::User()->first_name.' '.Auth::User()->last_name; 
                    $od->save();
                    $inv = Inventory::where('status',1)->where('material_id',$b['product_id'])->first();
                    $inv->qty -= $b['qty'];
                    $inv->save();
                }
                else if(!$b['enough'] && $b['toprep'] > 0){
                    $orderdet = new OrderDetails;
                    $orderdet->order_id = $b['order_id'];
                    $orderdet->preset_id = $b['preset_id'];
                    $orderdet->product_id = $b['product_id'];
                    $orderdet->qty = $b['toprep'];
                    $orderdet->preset_unit = $b['preset_unit'];
                    $orderdet->preset_qty = $b['preset_qty'];
                    $orderdet->order_status = 'to prep';
                    $orderdet->prepared_by = Auth::User()->first_name.' '.Auth::User()->last_name; 
                    $orderdet->prepared_at = date("Y-m-d H:i:s"); 
                    $orderdet->unit_price = $b['unit_price'];
                    $orderdet->total_price = $b['unit_price'] * $b['toprep'];
                    $orderdet->save();

                    $od->qty -= $b['toprep'];
                    $od->save();
                    $inv = Inventory::where('status',1)->where('material_id',$b['product_id'])->first();
                    $inv->qty -= $b['toprep'];
                    $inv->save();
                }
            }

            $od = OrderDetails::whereIn('id',$order_ids)->groupBy('order_id')->get();
            
            foreach($od as $o){
                $order = Order::where('id',$o->order_id)->where('project_id','>',0)->where('task_id','>',0)->first();
                if($order){
                    $completed = $this->updateProjectTaskStatus($o->order_id);
                }
            }

            $response['status'] = 'Success';
            $response['data'] = $od;
            $response['code'] = 200;
        }

        return Response::json($response);
    }

    public function toShip(Request $request) {
        $validator = Validator::make($request->all(), [
            'data' => 'required',
            //'label' => 'required'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;
        } else {
            // $ids = [];
            $mat = $request->data;
            $order_ids = [];
            foreach($mat as $b){
                $order_ids[] = $b['id'];
                $od = OrderDetails::where('id',$b['id'])->first();
                $od->order_status = "to ship";
                $od->prepared_at = date("Y-m-d H:i:s");
                $od->save();
            }

            $od = OrderDetails::whereIn('id',$order_ids)->groupBy('order_id')->get();
            
            foreach($od as $o){
                $order = Order::where('id',$o->order_id)->where('project_id','>',0)->where('task_id','>',0)->first();
                if($order){
                    $completed = $this->updateProjectTaskStatus($o->order_id);
                }
            }

            $response['status'] = 'Success';
            $response['data'] = $od;
            $response['code'] = 200;
        }

        return Response::json($response);
    }

    public function transferToRcv(Request $request) {
        $validator = Validator::make($request->all(), [
            'data' => 'required',
            'selected_driver' => 'required',
            'selected_car' => 'required',
            'delivery_date' => 'required'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;
        } else {
            $car = '';
            $driver = '';

            if(!is_numeric($request->selected_car)){
                $car = new Cars;
                $car->name = $request->selected_car;
                $car->save();
            }
            else{
                $car = Cars::findorfail($request->selected_car);
            }

            if(!is_numeric($request->selected_driver)){
                $driver = new Drivers;
                $driver->name = $request->selected_driver;
                $driver->save();
            }
            else{
                $driver = Drivers::findorfail($request->selected_driver);
            }

            //foreach($request->data as $d){
                $trans = Transfer::where('id', $request->data['id'])
                            ->update([ 
                                'status' => 'to received' , 
                                'for_delivery' => date("Y-m-d H:i:s"),
                                'delivery_date' => $request->delivery_date,
                                'car_details' => $car->name,
                                'delivered_by' => $driver->name
                            ]);
            //}
            

            $response['status'] = 'Success';
            // $response['data'] = $trans;
            $response['code'] = 200;
        }

        return Response::json($response);
    }

    public function transferDelete(Request $request) {
        $validator = Validator::make($request->all(), [
            'data' => 'required'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;
        } else {
            $trans = Transfer::where('id', $request->data['id'])->first();
            $transdet = TransferDetails::where('transfer_id', $request->data['id'])->get();

            foreach($transdet as $d){
                $inv = Inventory::where('material_id',$d->material_id)->where('address_id',$trans->transfer_from)->first();
                if($inv){
                    $inv->qty += $d->qty;
                    $inv->save();
                    $td = TransferDetails::where('id', $d->id)->delete();
                }

            }
                $trans = Transfer::where('id', $request->data['id'])->delete();

            

            $response['status'] = 'Success';
            // $response['data'] = $trans;
            $response['code'] = 200;
        }

        return Response::json($response);
    }

    public function freeTransferToRcv(Request $request) {
        $validator = Validator::make($request->all(), [
            'data' => 'required',
            'selected_driver' => 'required',
            'selected_car' => 'required',
            'delivery_date' => 'required'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;
        } else {
            $car = '';
            $driver = '';

            if(!is_numeric($request->selected_car)){
                $car = new Cars;
                $car->name = $request->selected_car;
                $car->save();
            }
            else{
                $car = Cars::findorfail($request->selected_car);
            }

            if(!is_numeric($request->selected_driver)){
                $driver = new Drivers;
                $driver->name = $request->selected_driver;
                $driver->save();
            }
            else{
                $driver = Drivers::findorfail($request->selected_driver);
            }

            //foreach($request->data as $d){
                $trans = TransferFree::where('id', $request->data['id'])
                            ->update([ 
                                'status' => 'to received' , 
                                'for_delivery' => date("Y-m-d H:i:s"),
                                'delivery_date' => $request->delivery_date,
                                'car_details' => $car->name,
                                'delivered_by' => $driver->name
                            ]);
            //}
            

            $response['status'] = 'Success';
            // $response['data'] = $trans;
            $response['code'] = 200;
        }

        return Response::json($response);
    }

    public function freeTransferRcv(Request $request) {
        $validator = Validator::make($request->all(), [
            'data' => 'required'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;
        } else {
            $car = '';
            $driver = '';

            //foreach($request->data as $d){
                $trans = TransferFree::where('id', $request->data['id'])
                            ->update([ 
                                'status' => 'completed' 
                            ]);
            //}
            

            $response['status'] = 'Success';
            // $response['data'] = $trans;
            $response['code'] = 200;
        }

        return Response::json($response);
    }

    public function rcvTransfer($id){

        $trans = Transfer::findorfail($id);
        $trans->status = 'completed';
        $trans->save();

        $details = TransferDetails::where('transfer_id', $id)->get();

        foreach($details as $d){
            $checkInv = Inventory::where('address_id', $trans->transfer_to)->where('material_id', $d->material_id)->first();
            if($checkInv){
                $checkInv->qty += $d->qty;
                $checkInv->save();
            }
            else{
                $newInv = new Inventory;
                $newInv->material_id = $d->material_id;
                $newInv->address_id = $trans->transfer_to;
                $newInv->qty = $d->qty;
                $newInv->status = 1;
                $newInv->save();
            }

        }


        $response['status'] = 'Success';
        $response['code'] = 200;

        return Response::json($response);
    }

    public function toRcv(Request $request) {
        $validator = Validator::make($request->all(), [
            'ids' => 'required',
            'selected_driver' => 'required',
            'selected_car' => 'required',
            'delivery_date' => 'required'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;
        } else {
            $ids = [];
            $car = '';

            if(!is_numeric($request->selected_car)){
                $car = new Cars;
                $car->name = $request->selected_car;
                $car->save();
            }
            else{
                $car = Cars::findorfail($request->selected_car);
            }

            if(!is_numeric($request->selected_driver)){
                $driver = new Drivers;
                $driver->name = $request->selected_driver;
                $driver->save();
            }
            else{
                $driver = Drivers::findorfail($request->selected_driver);
            }

            $ordet = OrderDetails::whereIn('order_id',$request->ids)
                        ->where('order_status','to ship')
                        ->update([ 
                            'order_status' => 'to received' , 
                            'for_delivery' => date("Y-m-d H:i:s"),
                            'delivery_date' => $request->delivery_date,
                            'car_details' => $car->name,
                            'delivered_by' => $driver->name
                        ]);
            // $mat = $request->data;
            // $order_ids = [];
            // foreach($mat as $b){
            //     $od = OrderDetails::where('id',$b['id'])->first();
            //     $order_ids[] = $b['id'];
            //     $od->order_status = "to received";
            //     $od->shipped_by = $b['shipped_by'];
            //     $od->save();
            // }

            $od = OrderDetails::whereIn('order_id',$request->ids)
                        ->groupBy('order_id')->get();
            
            foreach($od as $o){
                $order = Order::where('id',$o->order_id)->where('project_id','>',0)->where('task_id','>',0)->first();
                if($order){
                    $completed = $this->updateProjectTaskStatus($o->order_id);
                }
            }

            $response['status'] = 'Success';
            $response['data'] = $od;
            $response['code'] = 200;
        }

        return Response::json($response);
    }

    public function markAsRcv(Request $request) {
        $validator = Validator::make($request->all(), [
            'data' => 'required',
            //'label' => 'required'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;
        } else {
            // $ids = [];
            $mat = $request->data;
            $order_ids = [];
            foreach($mat as $b){
                $od = OrderDetails::where('id',$b['id'])->first();
                $order_ids[] = $b['id'];
                $od->order_status = "completed";
                $od->save();
            }

            $od = OrderDetails::whereIn('id',$order_ids)->groupBy('order_id')->get();
            
            foreach($od as $o){
                $order = Order::where('id',$o->order_id)->where('project_id','>',0)->where('task_id','>',0)->first();
                if($order){
                    $completed = $this->updateProjectTaskStatus($o->order_id);
                }
            }

            $response['status'] = 'Success';
            $response['data'] = $od;
            $response['code'] = 200;
        }

        return Response::json($response);
    }


    public function getSiteOrders(Request $request){
        //jepandan
        $mode = ($request->mode == 'Site Order' ? 'site order' : $request->mode);

        $sort = $request->input('sort');
        $perPage  = $request->input('perPage');
        $search  = $request->input('search');


        $srt = '';
        if($sort != ''){
            $srt = explode('-' , $sort);
            $srt = $srt[0];
        }
        $list = '';
        $mids = '';

        if($search != ''){
            $list = Project::when($search != '', function ($q) use($search){
                                return $q->where('name','LIKE', '%'.$search.'%')->orwhere('nick_name','LIKE', '%'.$search.'%');
                            })->get()->pluck('id');
        }

        if($sort != ''){
            $sort = explode('-' , $sort);
            if($sort[0] == 'name') {
                    $list = Project::when($search != '', function ($q) use($search){
                                return $q->where('name','LIKE', '%'.$search.'%');
                            })->orderBy($sort[0], $sort[1])->get()->pluck('id');
            }
        }


        if($list != ''){
            $mids = "'".implode("', '", $list->toArray())."'";
        }

        // if($inv_id == 0){
    
        $od = OrderDetails::where('order_status',$mode)->pluck('order_id');
        $order_ids = Order::whereIn('id',$od)
                    ->when($list != '', function ($q) use($list) {
                                return $q->whereIn('project_id', $list);
                        })
                    ->when($list != '', function ($q) use($mids) {
                                return $q->orderByRaw(DB::raw("FIELD(project_id, $mids)"));
                        })
                    ->when($srt == 'id' || $srt == 'needed_date' || $srt == 'remarks', function ($q) use($sort) {
                            // $sort = explode('-' , $sort);
                            //if($sort[0] == 'id') {
                                return $q->orderBy($sort[0], $sort[1]);
                            //}
                        })
                    ->pluck('id');
        $orders = OrderDetails::whereIn('order_id', $order_ids)->where('order_status',$mode)->get();

        foreach ($orders as $k=>$v){
            $orid = $v->order_id;
            $ord = Order::findOrFail($orid);
            $proj = Project::findorfail($ord->project_id);
            $v->name = $proj->nick_name;
            $v->needed_date = $ord->needed_date;
            $v->project_id = $ord->project_id;
            $v->task_id = $ord->task_id;
            $od = OrderDetails::where('order_id',$v->order_id)->where('order_status', $mode)->get();
            // \Log::info($od);
            $reqat = null;
            $reqby = null;
            $payment_id = null;
            $ctr = 0;
            $isRcv = 0;
            //foreach ($od as $a=>$b){
                $reqat = ($reqat == null ? $v->requested_at : $reqat);
                $reqby = ($reqby == null ? $v->requested_by : $reqby);
                $payment_id = ($payment_id == null ? $v->payment_id : $payment_id);
                $mat = Material::findorfail($v->product_id);
                $v->material_name = $mat->name;
                $v->unit = $mat->unit;
                if($v->preset_id > 0){
                $pre = Preset::findorfail($v->preset_id);
                    $v->preset_name = $pre->name;
                }
                else{
                    $v->preset_name = "--";
                }

                if($v->payment_id > 0){
                $payment = Payment::findorfail($v->payment_id);
                    $v->ref = $payment->ref;
                }
                else{
                    $v->ref = "--";
                }

                if($v->is_receive == 1){
                    $isRcv++;
                }
                $ctr++;
            //}
            $v->isRcvFlag = 0;
            if($ctr == $isRcv){
                $v->isRcvFlag = 1;
            }
            $v->requested_at = $reqat;
            $v->requested_by = $reqby;
            $v->payment_id = $payment_id;
            $v->orders = $od;
            $v->order_status = $mode;
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $orders;

        return Response::json($response);
    }

    public function getToPrepOrders(Request $request){
        //jepandan
        $mode = 'to prep';

        $sort = $request->input('sort');
        $perPage  = $request->input('perPage');
        $search  = $request->input('search');


        $srt = '';
        if($sort != ''){
            $srt = explode('-' , $sort);
            $srt = $srt[0];
        }
        $list = '';
        $mids = '';

        if($search != ''){
            $list = Project::when($search != '', function ($q) use($search){
                                return $q->where('name','LIKE', '%'.$search.'%')->orwhere('nick_name','LIKE', '%'.$search.'%');
                            })->get()->pluck('id');
        }

        if($sort != ''){
            $sort = explode('-' , $sort);
            if($sort[0] == 'name') {
                    $list = Project::when($search != '', function ($q) use($search){
                                return $q->where('name','LIKE', '%'.$search.'%');
                            })->orderBy($sort[0], $sort[1])->get()->pluck('id');
            }
        }


        if($list != ''){
            $mids = "'".implode("', '", $list->toArray())."'";
        }

        // if($inv_id == 0){
    
        $od = OrderDetails::where('order_status',$mode)->pluck('order_id');
        $orders = Order::whereIn('id',$od)
                    ->when($list != '', function ($q) use($list) {
                                return $q->whereIn('project_id', $list);
                        })
                    ->when($list != '', function ($q) use($mids) {
                                return $q->orderByRaw(DB::raw("FIELD(project_id, $mids)"));
                        })
                    ->when($srt == 'id' || $srt == 'needed_date' || $srt == 'remarks', function ($q) use($sort) {
                            // $sort = explode('-' , $sort);
                            //if($sort[0] == 'id') {
                                return $q->orderBy($sort[0], $sort[1]);
                            //}
                        })
                    ->get();
        foreach ($orders as $k=>$v){
            $proj = Project::findorfail($v->project_id);
            $v->name = $proj->nick_name;
            $od = OrderDetails::where('order_id',$v->id)->where('order_status', $mode)->get();
            // \Log::info($od);
            $reqat = null;
            $reqby = null;
            $prepat = null;
            $prepby = null;
            $payment_id = null;
            $ctr = 0;
            $isRcv = 0;
            foreach ($od as $a=>$b){
                $reqat = ($reqat == null ? $b->requested_at : $reqat);
                $reqby = ($reqby == null ? $b->requested_by : $reqby);
                $prepat = ($prepat == null ? $b->prepared_at : $prepat);
                $prepby = ($prepby == null ? $b->prepared_by : $prepby);
                $payment_id = ($payment_id == null ? $b->payment_id : $payment_id);
                $mat = Material::findorfail($b->product_id);
                $b->material_name = $mat->name;
                if($b->preset_id > 0){
                $pre = Preset::findorfail($b->preset_id);
                    $b->preset_name = $pre->name;
                }
                else{
                    $b->preset_name = "--";
                }

                if($b->is_receive == 1){
                    $isRcv++;
                }
                $ctr++;
            }
            $v->isRcvFlag = 0;
            if($ctr == $isRcv){
                $v->isRcvFlag = 1;
            }
            $v->requested_at = $reqat;
            $v->requested_by = $reqby;
            $v->prepared_at = $prepat;
            $v->prepared_by = $prepby;
            $v->payment_id = $payment_id;
            $v->orders = $od;
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $orders;

        return Response::json($response);
    }

    public function getToPrepOrders2(Request $request){
        // return 0;
        $od = OrderDetails::where('order_status','to prep')->pluck('order_id');
        $orders = Order::whereIn('id',$od)->get();
        foreach ($orders as $k=>$v){
            $proj = Project::findorfail($v->project_id);
            $v->name = $proj->nick_name;
            $od = OrderDetails::where('order_id',$v->id)->where('order_status','to prep')->get();
            $reqat = null;
            $reqby = null;
            $prepat = null;
            $prepby = null;
            foreach ($od as $a=>$b){
                $reqat = ($reqat == null ? $b->requested_at : $reqat);
                $reqby = ($reqby == null ? $b->requested_by : $reqby);
                $prepat = ($prepat == null ? $b->prepared_at : $prepat);
                $prepby = ($prepby == null ? $b->prepared_by : $prepby);
                $mat = Material::findorfail($b->product_id);
                $b->material_name = $mat->name;
                if($b->preset_id > 0){
                $pre = Preset::findorfail($b->preset_id);
                    $b->preset_name = $pre->name;
                }
                else{
                    $b->preset_name = "--";
                }
            }
            $v->requested_at = $reqat;
            $v->requested_by = $reqby;
            $v->prepared_at = $prepat;
            $v->prepared_by = $prepby;
            $v->orders = $od;
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $orders;

        return Response::json($response);
    }

    public function getToShipOrders(Request $request){
        //jepandan
        $mode = 'to ship';

        $sort = $request->input('sort');
        $perPage  = $request->input('perPage');
        $search  = $request->input('search');


        $srt = '';
        if($sort != ''){
            $srt = explode('-' , $sort);
            $srt = $srt[0];
        }
        $list = '';
        $mids = '';

        if($search != ''){
            $list = Project::when($search != '', function ($q) use($search){
                                return $q->where('name','LIKE', '%'.$search.'%')->orwhere('nick_name','LIKE', '%'.$search.'%');
                            })->get()->pluck('id');
        }

        if($sort != ''){
            $sort = explode('-' , $sort);
            if($sort[0] == 'name') {
                    $list = Project::when($search != '', function ($q) use($search){
                                return $q->where('name','LIKE', '%'.$search.'%');
                            })->orderBy($sort[0], $sort[1])->get()->pluck('id');
            }
        }


        if($list != ''){
            $mids = "'".implode("', '", $list->toArray())."'";
        }

        // if($inv_id == 0){
    
        $od = OrderDetails::where('order_status',$mode)->pluck('order_id');
        $orders = Order::whereIn('id',$od)
                    ->when($list != '', function ($q) use($list) {
                                return $q->whereIn('project_id', $list);
                        })
                    ->when($list != '', function ($q) use($mids) {
                                return $q->orderByRaw(DB::raw("FIELD(project_id, $mids)"));
                        })
                    ->when($srt == 'id' || $srt == 'needed_date' || $srt == 'remarks', function ($q) use($sort) {
                            // $sort = explode('-' , $sort);
                            //if($sort[0] == 'id') {
                                return $q->orderBy($sort[0], $sort[1]);
                            //}
                        })
                    ->get();
        foreach ($orders as $k=>$v){
            $proj = Project::findorfail($v->project_id);
            $v->name = $proj->nick_name;
            $od = OrderDetails::where('order_id',$v->id)->where('order_status', $mode)->get();
            // \Log::info($od);
            $reqat = null;
            $reqby = null;
            $prepat = null;
            $prepby = null;
            $payment_id = null;
            $ctr = 0;
            $isRcv = 0;
            foreach ($od as $a=>$b){
                $reqat = ($reqat == null ? $b->requested_at : $reqat);
                $reqby = ($reqby == null ? $b->requested_by : $reqby);
                $prepat = ($prepat == null ? $b->prepared_at : $prepat);
                $prepby = ($prepby == null ? $b->prepared_by : $prepby);
                $payment_id = ($payment_id == null ? $b->payment_id : $payment_id);
                $mat = Material::findorfail($b->product_id);
                $b->material_name = $mat->name;
                if($b->preset_id > 0){
                $pre = Preset::findorfail($b->preset_id);
                    $b->preset_name = $pre->name;
                }
                else{
                    $b->preset_name = "--";
                }

                if($b->is_receive == 1){
                    $isRcv++;
                }
                $ctr++;
            }
            $v->isRcvFlag = 0;
            if($ctr == $isRcv){
                $v->isRcvFlag = 1;
            }
            $v->requested_at = $reqat;
            $v->requested_by = $reqby;
            $v->prepared_at = $prepat;
            $v->prepared_by = $prepby;
            $v->payment_id = $payment_id;
            $v->orders = $od;
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $orders;

        return Response::json($response);
    }

    public function getToShipOrders2(Request $request){
        // return 0;
        $od = OrderDetails::where('order_status','to ship')->pluck('order_id');
        $orders = Order::whereIn('id',$od)->get();
        foreach ($orders as $k=>$v){
            $proj = Project::findorfail($v->project_id);
            $v->name = $proj->nick_name;
            $od = OrderDetails::where('order_id',$v->id)->where('order_status','to ship')->get();
            $reqat = null;
            $reqby = null;
            $prepat = null;
            $prepby = null;
            foreach ($od as $a=>$b){
                $reqat = ($reqat == null ? $b->requested_at : $reqat);
                $reqby = ($reqby == null ? $b->requested_by : $reqby);
                $prepat = $b->prepared_at;
                $prepby = $b->prepared_by;
                $v->updated_at = $b->updated_at;
                $mat = Material::findorfail($b->product_id);
                $b->material_name = $mat->name;
                if($b->preset_id > 0){
                $pre = Preset::findorfail($b->preset_id);
                    $b->preset_name = $pre->name;
                }
                else{
                    $b->preset_name = "--";
                }
            }
            $v->requested_at = $reqat;
            $v->requested_by = $reqby;
            $v->prepared_at = $prepat;
            $v->prepared_by = $prepby;
            $v->orders = $od;
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $orders;

        return Response::json($response);
    }    

    public function getToShipTransfers(Request $request){
        $transfers = Transfer::where('status', $request->input('status'))->orderBy('id','DESC')->get();

        foreach($transfers as $t){
            $details = [];
            if($t->transfer_from > 0){
                $t->transfer_from = Project::findorfail($t->transfer_from)->name;
            }
            else{
                $t->transfer_from = 'Warehouse';
            }

            if($t->transfer_to > 0){
                $t->transfer_to = Project::findorfail($t->transfer_to)->name;
            }
            else{
                $t->transfer_to = 'Warehouse';
            }

            $details = TransferDetails::where('transfer_id', $t->id)->get();
            foreach($details as $d){
                $mat = Material::findorfail($d->material_id);
                $d->material_name = $mat->name;
                $d->weight = $mat->weight;
            }
            $t->details = $details;
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $transfers;

        return Response::json($response);
    }  

    public function getToShipFreeTransfers(Request $request){
        $transfers = TransferFree::where('status', $request->input('status'))->orderBy('id','DESC')->get();

        foreach($transfers as $t){
            $details = [];
            $details = TransferFreeDetails::where('transfer_id', $t->id)->get();
            $t->details = $details;
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $transfers;

        return Response::json($response);
    } 

    public function getToRcvFreeTransfers(Request $request){
        $transfers = TransferFree::where('status', $request->input('status'))->orderBy('id','DESC')->get();

        foreach($transfers as $t){
            $details = [];
            $details = TransferFreeDetails::where('transfer_id', $t->id)->get();
            $t->details = $details;
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $transfers;

        return Response::json($response);
    } 

    public function addFreeTransfer(Request $request){  
        $transfer = new TransferFree;
        $transfer->type = $request->type;
        $transfer->transfer_from = $request->transfer_from;
        $transfer->transfer_to = $request->transfer_to;
        $transfer->needed_date = $request->needed_date;
        $transfer->needed_time_from = $request->needed_time[0];
        $transfer->needed_time_to = $request->needed_time[1];
        $transfer->notes = $request->notes;
        $transfer->save();


        foreach($request->item as $item){
            $transdet = new TransferFreeDetails;
            $transdet->transfer_id = $transfer->id;
            $transdet->detail = $item['detail'];
            $transdet->save();
        }


        $response['status'] = 'Success';
        $response['code'] = 200;
        return Response::json($response);
    }

    public function getPocPayments(Request $request){
        $sort = $request->input('sort');
        $search  = $request->input('search');

        $srt = '';
        $srt1 = '';
        if($sort != ''){
            $srts = explode('-' , $sort);
            $srt = $srts[0];
            $srt1 = $srts[1];
        }

        $payids = OrderDetails::where('order_status','POC')->pluck('payment_id');
        $pocs = Payment::where('paytype', 'POC')->whereIn('id', $payids)
                    ->where(function ($query) {
                    $query->where('description','!=','*Fee*')
                          ->orwhereNull('description');
                    })
                    // ->when($search != '', function ($q) use($search){
                    //     return $q->where('ref','LIKE', '%'.$search.'%');
                    // })
                    ->when($srt != '' && ($srt == 'ref'), function ($q) use($srt, $srt1) {
                        return $q->orderBy($srt, $srt1);
                    })
                    ->get();

        foreach($pocs as $k=>$p){
            $fdet = OrderDetails::where('order_status', 'POC')->where('payment_id', $p->id)->first();
            if($fdet){
                $order = Order::where('id', $fdet->order_id)->first();
                // \Log::info($order);
                $details = OrderDetails::where('order_status', 'POC')->where('payment_id', $p->id)->get();
                foreach($details as $d){
                    $mat = Material::findorfail($d->product_id);
                    $d->material_name = $mat->name;
                }
                $p->needed_date = '';
                $p->name = '';
                $p->project_id = '';
                $p->task_id = '';
                $p->ref2 = strtolower($p->ref);

                if($order){
                    $p->needed_date = $order->needed_date;
                    $proj = Project::where('id', $order->project_id)->first();
                    $p->name = $proj->name;
                    $p->project2 = strtolower($p->name);
                    if($p->project2 == null){
                        $p->project2 = 'a';
                    }
                    $p->project_id = $order->project_id;
                    $p->task_id = $order->task_id;
                }
                $p->remarks = $fdet->remarks;
                $p->order_id = $fdet->order_id;
                $p->remarks2 = strval($fdet->remarks);
                $p->status2 = '';
                if($p->status == 'approved'){
                    $p->status2 = 'partial payment';
                }
                if($p->status == 'for approval'){
                    $p->status2 = 'waiting for payment';
                }
                if($p->status == 'completed'){
                    $p->status2 = 'Completed';
                }
                $p->requested_by = $fdet->requested_by;
                $p->requested_at = $fdet->requested_at;
                $p->details = $details;
            }
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $pocs;

        return Response::json($response);
    }

    public function getToRcvOrders(Request $request){
        //jepandan
        $mode = 'to received';

        $sort = $request->input('sort');
        $perPage  = $request->input('perPage');
        $search  = $request->input('search');


        $srt = '';
        if($sort != ''){
            $srt = explode('-' , $sort);
            $srt = $srt[0];
        }
        $list = '';
        $mids = '';

        if($search != ''){
            $list = Project::when($search != '', function ($q) use($search){
                                return $q->where('name','LIKE', '%'.$search.'%')->orwhere('nick_name','LIKE', '%'.$search.'%');
                            })->get()->pluck('id');
        }

        if($sort != ''){
            $sort = explode('-' , $sort);
            if($sort[0] == 'name') {
                    $list = Project::when($search != '', function ($q) use($search){
                                return $q->where('name','LIKE', '%'.$search.'%');
                            })->orderBy($sort[0], $sort[1])->get()->pluck('id');
            }
        }


        if($list != ''){
            $mids = "'".implode("', '", $list->toArray())."'";
        }

        // if($inv_id == 0){
    
        $od = OrderDetails::where('order_status',$mode)->pluck('order_id');
        $orders = Order::whereIn('id',$od)
                    ->when($list != '', function ($q) use($list) {
                                return $q->whereIn('project_id', $list);
                        })
                    ->when($list != '', function ($q) use($mids) {
                                return $q->orderByRaw(DB::raw("FIELD(project_id, $mids)"));
                        })
                    ->when($srt == 'id' || $srt == 'needed_date' || $srt == 'remarks', function ($q) use($sort) {
                            // $sort = explode('-' , $sort);
                            //if($sort[0] == 'id') {
                                return $q->orderBy($sort[0], $sort[1]);
                            //}
                        })
                    ->get();
        foreach ($orders as $k=>$v){
            $proj = Project::findorfail($v->project_id);
            $v->name = $proj->nick_name;
            $od = OrderDetails::where('order_id',$v->id)->where('order_status', $mode)->get();
            // \Log::info($od);
            $reqat = null;
            $reqby = null;
            $prepat = null;
            $prepby = null;
            $fordel = null;
            $deldate = null;
            $delby = null;
            $delat = null;
            $cardet = null;
            $payment_id = null;
            $ctr = 0;
            $isRcv = 0;
            foreach ($od as $a=>$b){
                $reqat = ($reqat == null ? $b->requested_at : $reqat);
                $reqby = ($reqby == null ? $b->requested_by : $reqby);
                $prepat = ($prepat == null ? $b->prepared_at : $prepat);
                $prepby = ($prepby == null ? $b->prepared_by : $prepby);
                $fordel = ($fordel == null ? $b->for_delivery : $fordel);
                $deldate = ($deldate == null ? $b->delivery_date : $deldate);
                $delby = ($delby == null ? $b->delivered_by : $delby);
                $delat = ($delat == null ? $b->delivered_at : $delat);
                $cardet = ($cardet == null ? $b->car_details : $cardet);
                $payment_id = ($payment_id == null ? $b->payment_id : $payment_id);
                $mat = Material::findorfail($b->product_id);
                $b->material_name = $mat->name;
                if($b->preset_id > 0){
                $pre = Preset::findorfail($b->preset_id);
                    $b->preset_name = $pre->name;
                }
                else{
                    $b->preset_name = "--";
                }

                if($b->is_receive == 1){
                    $isRcv++;
                }
                $ctr++;
            }
            $v->isRcvFlag = 0;
            if($ctr == $isRcv){
                $v->isRcvFlag = 1;
            }
            $v->requested_at = $reqat;
            $v->requested_by = $reqby;
            $v->prepared_at = $prepat;
            $v->prepared_by = $prepby;
            $v->for_delivery = $fordel;
            $v->delivery_date = $deldate;
            $v->delivered_by = $delby;
            $v->delivered_at = $delat;
            $v->car_details = $cardet;
            $v->payment_id = $payment_id;
            $v->orders = $od;
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $orders;

        return Response::json($response);
    }

    public function getToRcvOrders2(Request $request){
        // return 0;
        $od = OrderDetails::where('order_status','to received')->pluck('order_id');
        $orders = Order::whereIn('id',$od)->get();
        foreach ($orders as $k=>$v){
            $proj = Project::findorfail($v->project_id);
            $v->name = $proj->nick_name;
            $od = OrderDetails::where('order_id',$v->id)->where('order_status','to received')->get();
            $fordel = null;
            $deldate = null;
            $delby = null;
            $delat = null;
            $cardet = null;
            foreach ($od as $a=>$b){
                $fordel = $b->for_delivery;
                $deldate = $b->delivery_date;
                $delby = $b->delivered_by;
                $delat = $b->delivered_at;
                $cardet = $b->car_details;
                $v->updated_at = $b->updated_at;
                $mat = Material::findorfail($b->product_id);
                $b->material_name = $mat->name;
                if($b->preset_id > 0){
                $pre = Preset::findorfail($b->preset_id);
                    $b->preset_name = $pre->name;
                }
                else{
                    $b->preset_name = "--";
                }
            }
            $v->for_delivery = $fordel;
            $v->delivery_date = $deldate;
            $v->delivered_by = $delby;
            $v->delivered_at = $delat;
            $v->car_details = $cardet;
            $v->orders = $od;
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $orders;

        return Response::json($response);
    }

    public function getCompletedOrders(Request $request){
        //jepandan
        $mode = 'completed';

        $sort = $request->input('sort');
        $perPage  = $request->input('perPage');
        $search  = $request->input('search');


        $srt = '';
        if($sort != ''){
            $srt = explode('-' , $sort);
            $srt = $srt[0];
        }
        $list = '';
        $mids = '';

        if($search != ''){
            $list = Project::when($search != '', function ($q) use($search){
                                return $q->where('name','LIKE', '%'.$search.'%')->orwhere('nick_name','LIKE', '%'.$search.'%');
                            })->get()->pluck('id');
        }

        if($sort != ''){
            $sort = explode('-' , $sort);
            if($sort[0] == 'name') {
                    $list = Project::when($search != '', function ($q) use($search){
                                return $q->where('name','LIKE', '%'.$search.'%');
                            })->orderBy($sort[0], $sort[1])->get()->pluck('id');
            }
        }


        if($list != ''){
            $mids = "'".implode("', '", $list->toArray())."'";
        }

        // if($inv_id == 0){
    
        $od = OrderDetails::where('order_status',$mode)->pluck('order_id');
        $orders = Order::whereIn('id',$od)
                    ->when($list != '', function ($q) use($list) {
                                return $q->whereIn('project_id', $list);
                        })
                    ->when($list != '', function ($q) use($mids) {
                                return $q->orderByRaw(DB::raw("FIELD(project_id, $mids)"));
                        })
                    ->when($srt == 'id' || $srt == 'needed_date' || $srt == 'remarks', function ($q) use($sort) {
                            // $sort = explode('-' , $sort);
                            //if($sort[0] == 'id') {
                                return $q->orderBy($sort[0], $sort[1]);
                            //}
                        })
                    ->get();
        foreach ($orders as $k=>$v){
            $proj = Project::findorfail($v->project_id);
            $v->name = $proj->nick_name;
            $od = OrderDetails::where('order_id',$v->id)->where('order_status', $mode)->get();
            // \Log::info($od);
            $reqat = null;
            $reqby = null;
            $prepat = null;
            $prepby = null;
            $fordel = null;
            $deldate = null;
            $delby = null;
            $delat = null;
            $cardet = null;
            $rcvby = null;
            $payment_id = null;
            $ctr = 0;
            $isRcv = 0;
            foreach ($od as $a=>$b){
                $reqat = ($reqat == null ? $b->requested_at : $reqat);
                $reqby = ($reqby == null ? $b->requested_by : $reqby);
                $prepat = ($prepat == null ? $b->prepared_at : $prepat);
                $prepby = ($prepby == null ? $b->prepared_by : $prepby);
                $fordel = ($fordel == null ? $b->for_delivery : $fordel);
                $deldate = ($deldate == null ? $b->delivery_date : $deldate);
                $delby = ($delby == null ? $b->delivered_by : $delby);
                $delat = ($delat == null ? $b->delivered_at : $delat);
                $cardet = ($cardet == null ? $b->car_details : $cardet);
                $rcvby = $b->received_by;
                $payment_id = ($payment_id == null ? $b->payment_id : $payment_id);
                $mat = Material::findorfail($b->product_id);
                $b->material_name = $mat->name;
                if($b->preset_id > 0){
                $pre = Preset::findorfail($b->preset_id);
                    $b->preset_name = $pre->name;
                }
                else{
                    $b->preset_name = "--";
                }

                if($b->is_receive == 1){
                    $isRcv++;
                }
                $ctr++;
            }
            $v->isRcvFlag = 0;
            if($ctr == $isRcv){
                $v->isRcvFlag = 1;
            }
            $v->requested_at = $reqat;
            $v->requested_by = $reqby;
            $v->prepared_at = $prepat;
            $v->prepared_by = $prepby;
            $v->for_delivery = $fordel;
            $v->delivery_date = $deldate;
            $v->delivered_by = $delby;
            $v->delivered_at = $delat;
            $v->car_details = $cardet;
            $v->received_by = $rcvby;
            $v->payment_id = $payment_id;
            $v->orders = $od;
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $orders;

        return Response::json($response);
    }

    public function getCompletedOrders2(Request $request){
        // return 0;
        $od = OrderDetails::where('order_status','completed')->pluck('order_id');
        $orders = Order::whereIn('id',$od)->get();
        foreach ($orders as $k=>$v){
            $proj = Project::findorfail($v->project_id);
            $v->name = $proj->nick_name;
            $od = OrderDetails::where('order_id',$v->id)->where('order_status','completed')->get();
            $rcvby = null;
            $delat = null;
            foreach ($od as $a=>$b){
                $rcvby = $b->received_by;
                $delat = $b->delivered_at;
                $v->updated_at = $b->updated_at;
                $mat = Material::findorfail($b->product_id);
                $b->material_name = $mat->name;
                if($b->preset_id > 0){
                $pre = Preset::findorfail($b->preset_id);
                    $b->preset_name = $pre->name;
                }
                else{
                    $b->preset_name = "--";
                }
            }
            $v->received_by = $rcvby;
            $v->delivered_at = $delat;
            $v->orders = $od;
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $orders;

        return Response::json($response);
    }

    public function getAllOrders(Request $request){
        $orders = [];
        $od = OrderDetails::where('paid',0)->pluck('order_id');
        $orders = Order::whereIn('id',$od)->where('project_id','>',0)->get();
        foreach ($orders as $k=>$v){
            $proj = Project::findorfail($v->project_id);
            $v->name = $proj->nick_name;
            $od = OrderDetails::where('order_id',$v->id)->where('paid',0)->get();
            foreach ($od as $a=>$b){                
                $mat = Material::findorfail($b->product_id);
                $b->material_name = $mat->name;
                if($b->preset_id > 0){
                    $pre = Preset::findorfail($b->preset_id);
                    $b->preset_name = $pre->name;
                }
                else{
                    $b->preset_name = "--";
                }
            }
            $v->orders = $od;
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $orders;

        return Response::json($response);
    }

    public function addMaterial(Request $request) {
            $m = new Material;
            $m->name = $request->name;
            $m->name_cn = $request->name_cn;
            $m->specs = $request->specs;
            $m->type = $request->category;
            $m->unit = $request->unit;
            $m->unit_price = $request->unit_price;
            $m->unit_price_cn = $request->unit_price_cnme;
            $m->retail_price = $request->retail_price;
            $m->weight = $request->weight;
            $m->length = $request->length;
            $m->width = $request->width;
            $m->height = $request->height;
            $m->save();
            

            Inventory::create([
                'material_id' => $m->id,
                'qty' => $request->qty,
                'status' => 1
            ]);

            foreach($request->uplPhotos as $item) {
                    //$expired_at = ($item['expired_at'] === null) ? '' : $item['expired_at'];
                    //$documentType = ClientDocumentType::where('id', $item['client_document_type_id'])->first();
                    if($item['file_path'] == 'image.png'){
                        $item['file_path'] = time().'image.png';
                    }
                    $path = $request->type.'/' . $m->id . '/'.$item['file_path'];
                    $pfile = MaterialImages::create([
                        'material_id' => $m->id,
                        'file_path' => $path,
                    ]);
                    //jeff
                    // DB::table('order_details')->where('order_id', $id)->update(array('paid' => 1));

                    $imgData = [
                        'imgBase64' => $item['imgBase64'],
                        'file_path' => $m->id,
                        'img_name' => $item['file_path']
                    ];

                    $this->uploadDocuments($imgData, $request->type);
                }

            $response['status'] = 'Success';
            $response['code'] = 200;
        

        return Response::json($response);
    }

    public function editMaterial($id, Request $request) {
            $m = Material::findOrFail($id);
            $m->name = $request->name;
            $m->name_cn = $request->name_cn;
            $m->specs = $request->specs;
            $m->type = $request->type;
            $m->unit = $request->unit;
            $m->unit_price = $request->unit_price;
            $m->unit_price_cn = $request->unit_price_cn;
            $m->retail_price = $request->retail_price;
            $m->weight = $request->weight;
            $m->length = $request->length;
            $m->width = $request->width;
            $m->height = $request->height;
            $m->remarks = $request->remarks;
            $m->tags = null;
            if(count($request->tags)>0){
                $m->tags = implode (",", $request->tags);
            }
            $m->save();

            foreach($request->uplPhotos as $item) {
                    //$expired_at = ($item['expired_at'] === null) ? '' : $item['expired_at'];
                    //$documentType = ClientDocumentType::where('id', $item['client_document_type_id'])->first();
                    if($item['file_path'] == 'image.png'){
                        $item['file_path'] = time().'image.png';
                    }
                    $path = $request->type.'/' . $m->id . '/'.$item['file_path'];
                    $pfile = MaterialImages::create([
                        'material_id' => $m->id,
                        'file_path' => $path,
                    ]);
                    //jeff
                    // DB::table('order_details')->where('order_id', $id)->update(array('paid' => 1));

                    $imgData = [
                        'imgBase64' => $item['imgBase64'],
                        'file_path' => $m->id,
                        'img_name' => $item['file_path']
                    ];

                    $this->uploadDocuments($imgData, $request->type);
                }

            $inv = Inventory::where('material_id', $request->id)->where('address_id',$request->location)->first();
            if($inv){
                $inv->qty = $request->qty;
                $inv->save();        
            }

            $response['status'] = 'Success';
            $response['code'] = 200;
            
        

        return Response::json($response);
    }  

    public function editOrder(Request $request) {

            $m = OrderDetails::findOrFail($request->id);
            if($m){
                $m->weight = $request->weight;
                $m->length = $request->length;
                $m->width = $request->width;
                $m->height = $request->height;
                $m->save();
            }

            $response['status'] = 'Success';
            $response['code'] = 200;
            
        return Response::json($response);
    }  

    public function updateOrderDetail(Request $request) {

            $type = $request->type;
            $m = OrderDetails::findOrFail($request->id);
            if($type == 'po_status'){
                $m->po_status = $request->po_status;
            }
            if($type == 'notes'){
                $m->notes = $request->notes;
            }
            if($type == 'carrier'){
                $m->carrier = $request->carrier;
            }
            $m->save();

            $response['status'] = 'Success';
            $response['code'] = 200;
            
        return Response::json($response);
    }

    public function updateContainerDetail(Request $request) {

            $type = $request->type;
            $m = Containers::findOrFail($request->id);
            if($type == 'eta'){
                $m->eta = $request->eta;
            }
            if($type == 'notes'){
                $m->notes = $request->notes;
            }
            $m->save();

            $response['status'] = 'Success';
            $response['code'] = 200;
            
        return Response::json($response);
    }

    public function approveMaterial($id) {


            $m = OrderDetails::findOrFail($id);
            $m->order_status = 'approved';
            $m->save();

            $inv = Inventory::where('material_id',$m->product_id)->first();
            if($inv){            
                $inv->qty = $inv->qty + $m->qty;
                $inv->save();
            }
                

            $response['status'] = 'Success';
            $response['code'] = 200;
        

        return Response::json($response);
    }

    public function approveBudget($id) {


            $m = OrderDetails::findOrFail($id);
            $m->order_status = 'budget_approved';
            $m->save();

            // $inv = Inventory::where('material_id',$m->product_id)->first();
            // if($inv){            
            //     $inv->qty = $inv->qty + $m->qty;
            //     $inv->save();
            // }
                

            $response['status'] = 'Success';
            $response['code'] = 200;
        

        return Response::json($response);
    }

    public function uploadPaymentDocuments(Request $request, $id) {
        $arrayTest = [];

        foreach($request->data as $item) {
            //$expired_at = ($item['expired_at'] === null) ? '' : $item['expired_at'];
            //$documentType = ClientDocumentType::where('id', $item['client_document_type_id'])->first();

            $count = PaymentFiles::where('payment_id', $id)->count();

            $count +=1;

            $path_parts = pathinfo($item['file_path']);
            $filename = $path_parts['filename'].'_'.$count.'.'.strtolower($path_parts['extension']);

            $path = 'payments/' . $id . '/'.$filename;

            PaymentFiles::create([
                'payment_id' => $id,
                'file_path' => $path
            ]);
            //DB::table('order_details')->where('order_id', $id)->update(array('paid' => 1));

            $imgData = [
                'imgBase64' => $item['imgBase64'],
                'file_path' => $id,
                'img_name' => $filename
            ];

            $this->uploadDocuments2($imgData);
        }

        return json_encode([
            'success' => true,
            'message' => 'Successfully saved.'
        ]);
    }

    public function uploadDocuments2($data) {
        $img64 = $data['imgBase64'];

        list($type, $img64) = explode(';', $img64);
        list(, $img64) = explode(',', $img64); 
        
        if($img64!=""){ // storing image in storage/app/public Folder 
                \Storage::disk('public')->put('payments/' . $data['file_path'] . '/' . $data['img_name'], base64_decode($img64)); 
        } 
    }

    public function receiveMaterials(Request $request) {
        $arrayTest = [];
        $ids = $request->ids;
        $taskid = 0;

        //$orders = Order::whereIn('id',$ids)->groupBy('project_id')->get();
        foreach($request->project as $p) {
            foreach($p['orders'] as $o) {
                // foreach($o['presets'] as $pre) {
                    foreach($o['materials'] as $premat) {
                        $ordet = OrderDetails::findorfail($premat['order_detail_id']);
                        if($ordet){
                            if($ordet->qty == $premat['qty']){
                                $ordet->order_status = 'completed';
                                $ordet->delivered_at = date("Y-m-d H:i:s");
                                $ordet->received_by = Auth::User()->first_name.' '.Auth::User()->last_name;
                                $ordet->save();

                            }
                            else if($premat['qty'] > 0){
                                $qty = $ordet->qty - $premat['qty'];
                                $orderdet = new OrderDetails;
                                $orderdet->order_id = $ordet->order_id;
                                $orderdet->preset_id = $ordet->preset_id;
                                $orderdet->product_id = $ordet->product_id;
                                $orderdet->qty = $qty;
                                $orderdet->preset_unit = $ordet->preset_unit;
                                $orderdet->preset_qty = $ordet->preset_qty;
                                $orderdet->order_status = 'to received';
                                $orderdet->unit_price = $ordet->unit_price;
                                $orderdet->total_price = $ordet->unit_price * $qty;
                                $orderdet->requested_at = $ordet->requested_at;
                                $orderdet->requested_by = $ordet->requested_by;
                                $orderdet->prepared_at = $ordet->prepared_at;
                                $orderdet->prepared_by = $ordet->prepared_by;
                                $orderdet->shipped_by = $ordet->shipped_by;
                                $orderdet->for_delivery = $ordet->for_delivery;
                                $orderdet->delivery_date = $ordet->delivery_date;
                                $orderdet->car_details = $ordet->car_details;
                                $orderdet->delivered_by = $ordet->delivered_by;
                                $orderdet->save();

                                $ordet->qty = $premat['qty'];
                                $ordet->order_status = 'completed';
                                $ordet->delivered_at = date("Y-m-d H:i:s");
                                $ordet->save();
                            }

                            $order = Order::findorfail($ordet->order_id);
                            if($taskid == 0){
                                $taskid = $order->task_id;
                            }
                                $checkInv = Inventory::where('address_id', $order->project_id)
                                                ->where('material_id', $ordet->product_id)
                                                ->first();
                                if($checkInv){
                                    $checkInv->qty += $premat['qty'];
                                    $checkInv->save();
                                }
                                else{
                                    $newInv = new Inventory;
                                    $newInv->material_id = $ordet->product_id;
                                    $newInv->address_id = $order->project_id;
                                    $newInv->qty = $premat['qty'];
                                    $newInv->status = 1;
                                    $newInv->save();
                                }
                        }
                    }
                // }

                foreach($o['addmaterials'] as $add) {
                    $ordet = OrderDetails::findorfail($add['id']);
                    if($ordet){
                        if($ordet->qty == $add['qty']){
                            $ordet->order_status = 'completed';
                            $ordet->delivered_at = date("Y-m-d H:i:s");
                            $ordet->received_by = Auth::User()->first_name.' '.Auth::User()->last_name;
                            $ordet->save();
                        }
                        else if($add['qty'] > 0){
                            $qty = $ordet->qty - $add['qty'];
                            $orderdet = new OrderDetails;
                            $orderdet->order_id = $ordet->order_id;
                            $orderdet->preset_id = $ordet->preset_id;
                            $orderdet->product_id = $ordet->product_id;
                            $orderdet->qty = $qty;
                            $orderdet->preset_unit = $ordet->preset_unit;
                            $orderdet->preset_qty = $ordet->preset_qty;
                            $orderdet->order_status = 'to received';
                            $orderdet->unit_price = $ordet->unit_price;
                            $orderdet->total_price = $ordet->unit_price * $qty;
                            $orderdet->requested_at = $ordet->requested_at;
                            $orderdet->requested_by = $ordet->requested_by;
                            $orderdet->prepared_at = $ordet->prepared_at;
                            $orderdet->prepared_by = $ordet->prepared_by;
                            $orderdet->shipped_by = $ordet->shipped_by;
                            $orderdet->for_delivery = $ordet->for_delivery;
                            $orderdet->delivery_date = $ordet->delivery_date;
                            $orderdet->car_details = $ordet->car_details;
                            $orderdet->delivered_by = $ordet->delivered_by;
                            $orderdet->save();

                            $ordet->qty = $add['qty'];
                            $ordet->order_status = 'completed';
                            $ordet->delivered_at = date("Y-m-d H:i:s");
                            $ordet->received_by = Auth::User()->first_name.' '.Auth::User()->last_name;
                            $ordet->save();
                        }

                        $order = Order::findorfail($ordet->order_id);
                        if($taskid == 0){
                                $taskid = $order->task_id;
                            }
                        $checkInv = Inventory::where('address_id', $order->project_id)
                                        ->where('material_id', $ordet->product_id)
                                        ->first();
                        if($checkInv){
                            $checkInv->qty += $add['qty'];
                            $checkInv->save();
                        }
                        else{
                            $newInv = new Inventory;
                            $newInv->material_id = $ordet->product_id;
                            $newInv->address_id = $order->project_id;
                            $newInv->qty = $add['qty'];
                            $newInv->status = 1;
                            $newInv->save();
                        }
                    }
                }

                $completed = $this->updateProjectTaskStatus($o['id']);

                $pcom = new ProjectTaskComments;
                $pcom->comment = 'Received Materials.';
                $pcom->proj_task_id = $taskid;
                $pcom->user_id = Auth::User()->id;
                $pcom->save();


                foreach($request->data as $item) {
                    //$expired_at = ($item['expired_at'] === null) ? '' : $item['expired_at'];
                    //$documentType = ClientDocumentType::where('id', $item['client_document_type_id'])->first();
                    if($item['file_path'] == 'image.png'){
                        $item['file_path'] = time().'image.png';
                    }
                    $path = $request->type.'/' . $o['id'] . '/'.$item['file_path'];
                    $pfile = ProjectOrderFiles::create([
                        'order_id' => $o['id'],
                        'file_path' => $path,
                    ]);
                    //jeffrey
                    // DB::table('order_details')->where('order_id', $id)->update(array('paid' => 1));

                    $imgData = [
                        'imgBase64' => $item['imgBase64'],
                        'file_path' => $o['id'],
                        'img_name' => $item['file_path']
                    ];

                    $this->uploadDocuments($imgData, $request->type);


                    //pass uploaded receipt during receiving of material to comment
                    $com_ids = ProjectTaskComments::where('proj_task_id', $taskid)->get()->pluck('id');
                    $count = ProjectTaskFiles::whereIn('comment_id', $com_ids)->count();

                    $count +=1;

                    $path_parts = pathinfo($item['file_path']);
                    $filename = $count.'.'.strtolower($path_parts['extension']);

                    $path = 'project_task/' . $taskid . '/'.$filename;

                    $pfile = ProjectTaskFiles::create([
                        'comment_id' => $pcom->id,
                        'file_path' => $path,
                    ]);

                    $imgData = [
                        'imgBase64' => $item['imgBase64'],
                        'file_path' => $taskid,
                        'img_name' => $filename
                    ];

                    $this->uploadDocuments($imgData, 'project_task');
                }
            }
        }

        return json_encode([
            'success' => true,
            'message' => 'Successfully saved.'
        ]);
    }

    public function uploadFileDocuments(Request $request) {
        $arrayTest = [];
        $id = $request->id;

        // $pcom = ProjectTaskComments::create([
        //     'comment' => $request->comment,
        //     'proj_task_id' => $id,
        //     'user_id' => Auth::User()->id,
        // ]);

        $pcom = new ProjectTaskComments;
        $pcom->comment =$request->comment;
        $pcom->proj_task_id = $id;
        $pcom->user_id =Auth::User()->id;
        $pcom->save();

        foreach($request->data as $item) {
            //$expired_at = ($item['expired_at'] === null) ? '' : $item['expired_at'];
            //$documentType = ClientDocumentType::where('id', $item['client_document_type_id'])->first();
            $com_ids = ProjectTaskComments::where('proj_task_id', $id)->get()->pluck('id');
            $count = ProjectTaskFiles::whereIn('comment_id', $com_ids)->count();

            $count +=1;

            $path_parts = pathinfo($item['file_path']);
            $filename = $count.'.'.strtolower($path_parts['extension']);

            $path = $request->type. '/' . $id . '/'.$filename;

            // ProjectTaskFiles::create([
            //     'comment_id' => $pcom->id,
            //     'file_path' => $path
            // ]);

            // if($item['file_path'] == 'image.png'){
            //     $item['file_path'] = time().'image.png';
            // }
            //$path = $request->type.'/' . $id . '/'.$item['file_path'];
            $pfile = ProjectTaskFiles::create([
                'comment_id' => $pcom->id,
                'file_path' => $path,
            ]);

            // DB::table('order_details')->where('order_id', $id)->update(array('paid' => 1));
            $imgData = [
                'imgBase64' => $item['imgBase64'],
                'file_path' => $id,
                'img_name' => $filename
            ];

            $this->uploadDocuments($imgData, $request->type);
        }

        return json_encode([
            'success' => true,
            'message' => 'Successfully saved.'
        ]);
    }

    public function uploadPaymentFileDocuments(Request $request) {
        $arrayTest = [];
        $id = $request->id;

        // $pcom = ProjectTaskComments::create([
        //     'comment' => $request->comment,
        //     'proj_task_id' => $id,
        //     'user_id' => Auth::User()->id,
        // ]);

        $pcom = new PaymentComments;
        $pcom->comment =$request->comment;
        $pcom->payment_id = $id;
        $pcom->user_id =Auth::User()->id;
        $pcom->save();

        foreach($request->data as $item) {
            //$expired_at = ($item['expired_at'] === null) ? '' : $item['expired_at'];
            //$documentType = ClientDocumentType::where('id', $item['client_document_type_id'])->first();
            $com_ids = PaymentComments::where('payment_id', $id)->get()->pluck('id');
            $count = PaymentCommentFiles::whereIn('comment_id', $com_ids)->count();

            $count +=1;

            $path_parts = pathinfo($item['file_path']);
            $filename = $count.'.'.strtolower($path_parts['extension']);

            $path = $request->type. '/' . $id . '/'.$filename;

            // ProjectTaskFiles::create([
            //     'comment_id' => $pcom->id,
            //     'file_path' => $path
            // ]);

            // if($item['file_path'] == 'image.png'){
            //     $item['file_path'] = time().'image.png';
            // }
            //$path = $request->type.'/' . $id . '/'.$item['file_path'];
            $pfile = PaymentCommentFiles::create([
                'comment_id' => $pcom->id,
                'file_path' => $path,
            ]);

            // DB::table('order_details')->where('order_id', $id)->update(array('paid' => 1));
            $imgData = [
                'imgBase64' => $item['imgBase64'],
                'file_path' => $id,
                'img_name' => $filename
            ];

            $this->uploadDocuments($imgData, $request->type);
        }

        return json_encode([
            'success' => true,
            'message' => 'Successfully saved.'
        ]);
    }

    public function uploadDocuments($data, $doctype) {
        // \Log::info($doctype);
        $img64 = $data['imgBase64'];

        list($type, $img64) = explode(';', $img64);
        list(, $img64) = explode(',', $img64); 
        // if($data['img_name'] == 'image.png'){
        //     $data['img_name'] = time().'image.png';
        // }
        
        if($img64!=""){ // storing image in storage/app/public Folder 
                \Storage::disk('public')->put($doctype.'/' . $data['file_path'] . '/' . $data['img_name'], base64_decode($img64)); 
        } 
    }

    public function getAllDrivers(Request $request){
        // return 0;
        $list = Drivers::get();

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $list;

        return Response::json($response);
    }

    public function getAllCars(Request $request){
        // return 0;
        $list = Cars::all();

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $list;

        return Response::json($response);
    }

    public function getAllCategories(Request $request){
        // return 0;
        $list = Categories::get();

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $list;

        return Response::json($response);
    }

    public function changeCategory(Request $request) {

        if(!is_numeric($request->cat)){
            $cat = new Categories;
            $cat->label = $request->cat;
            $cat->save();
        }

        if($request->type == 'order'){
            $pt = Order::findOrFail($request->id);
            $pt->category_id = is_numeric($request->cat) ? $request->cat : $cat->id;
            $pt->save();
        }
        else if($request->type == 'inventory'){
            $pt = Inventory::findOrFail($request->id);
            $pt->category_id = is_numeric($request->cat) ? $request->cat : $cat->id;
            $pt->save();
        }
                

            $response['status'] = 'Success';
            $response['code'] = 200;
        

        return Response::json($response);
    }

    public function changeUser(Request $request) {

        if($request->type == 'prepared_by'){
            OrderDetails::where('order_id', $request->id)
                ->where('order_status', 'to prep')
                ->update(['prepared_by' => $request->value]);
        }

            
            $response['status'] = 'Success';
            $response['code'] = 200;
        

        return Response::json($response);
    }

    public function exportPoc(Request $request) 
    {
        // if($request->input('ids') != ''){
        //    $pay_ids = explode(',', $request->input('ids')); //350
        // }else{
        //    $pay_ids = [];
        // }

        // $orders = OrderDetails::whereIn('payment_id', $pay_ids)->get();

        $export = new OrdersExport($request->ids);

        return Excel::download($export, 'pocs.xls');
    }

    public function exportPoc2(Request $request) 
    {
        // if($request->input('ids') != ''){
        //    $pay_ids = explode(',', $request->input('ids')); //350
        // }else{
        //    $pay_ids = [];
        // }

        // $orders = OrderDetails::whereIn('payment_id', $pay_ids)->get();

        $export = new ContainersExport($request->ids);

        return Excel::download($export, 'pocs.xls');
    }
}

