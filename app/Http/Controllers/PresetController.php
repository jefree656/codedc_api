<?php

namespace App\Http\Controllers;
use App\Remark;
use Carbon\Carbon;

use App\Preset;
use App\Material;
use App\MaterialImages;
use App\PresetMaterial;
use App\Order;
use App\OrderDetails;
use App\Bundle;
use App\BundlePreset;

use App\User;

use Illuminate\Support\Facades\URL;

use Auth, DB, Response, Validator;
//Excel
use Illuminate\Http\Request;

use Maatwebsite\Excel\Facades\Excel;

use Status;
use PDF;
use DateTime;

use App\Helpers\MessageHelper;

class PresetController extends Controller
{

	public function managePresets() {
		$groups = DB::table('groups as g')
			->select(DB::raw('g.id, g.name, CONCAT(u.first_name, " ", u.last_name) as leader, g.balance, g.collectables, p.latest_package as latest_package, srv.latest_service as latest_service'))
            ->leftjoin(DB::raw('(select * from users) as u'),'u.id','=','g.leader_id')
            ->leftjoin(DB::raw('
                    (
                        Select date_format(max(x.dates),"%M %e, %Y, %l:%i %p") as latest_package, x.group_id
                        from( SELECT STR_TO_DATE(created_at, "%Y-%m-%d %H:%i:%s") as dates,
                            group_id, status
                            FROM packages
                            ORDER BY dates desc
                        ) as x
                        group by x.group_id) as p'),
                    'p.group_id', '=', 'g.id')
            ->leftjoin(DB::raw('
                    (
                        Select date_format(max(cs.servdates),"%M %e, %Y") as latest_service,cs.client_id,cs.group_id
                        from( SELECT STR_TO_DATE(created_at, "%Y-%m-%d") as servdates,
                            group_id, active,client_id
                            FROM client_services
                            ORDER BY servdates desc
                        ) as cs
                        where cs.active = 1
                        group by cs.group_id) as srv'),
                    'srv.group_id', '=', 'g.id')
            ->orderBy('g.id', 'desc')
            ->get();

		$response['status'] = 'Success';
		$response['data'] = [
		    'groups' => $groups
		];
		$response['code'] = 200;

		return Response::json($response);
	}


  public function managePresetsPaginate(Request $request, $perPage = 20) {
        $sort = $request->input('sort');
        $search = $request->input('search');
        $branch = $request->input('branch');

        $presets = DB::table('presets as p')
            ->select(DB::raw('p.*'))
            ->when($search != '', function ($q) use($search){
                return $q->where('p.id','LIKE', '%'.$search.'%')->orwhere('p.name','LIKE', '%'.$search.'%');
            })
            ->when($sort != '', function ($q) use($sort){
                $sort = explode('-' , $sort);
                return $q->orderBy($sort[0], $sort[1]);
            })
            ->paginate($request->input('perPage'));
            //->paginate(100);

        return Response::json($presets);
    }

    public function presetSearch(Request $request){
        $keyword = $request->input('search');
        $users = '';

        $presets = DB::connection()
            ->table('presets as a')
            ->select(DB::raw('
                a.id,a.name'))
                //->orwhere('a.id','LIKE', '%' . $keyword .'%')
                ->orwhere('name','LIKE', '%' . $keyword .'%')
                ->get();


        $json = [];
        foreach($presets as $p){
          $json[] = array(
              'id' => $p->id,
              'name' => $p->name,
          );
        }
        $response['status'] = 'Success';
        $response['data'] =  $json;
        $response['code'] = 200;

        return Response::json($response);
    }

    public function store(Request $request) {

            $group = Preset::create([
                'name' => $request->preset_name,
                'name_cn' => $request->preset_name_ch,
                'type' => $request->type,
                'unit' => $request->unit,
                'description' => $request->desc,
                'description_cn' => $request->desc_ch,
            ]);

            $response['status'] = 'Success';
            $response['code'] = 200;

        return Response::json($response);
    }

    public function manageBundlesPaginate(Request $request) {
        $sort = $request->input('sort');
        $search = $request->input('search');
        $branch = $request->input('branch');

        $presets = DB::table('bundles as p')
            ->select(DB::raw('p.*'))
            ->when($search != '', function ($q) use($search){
                return $q->where('p.id','LIKE', '%'.$search.'%')->orwhere('p.name','LIKE', '%'.$search.'%');
            })
            ->when($sort != '', function ($q) use($sort){
                $sort = explode('-' , $sort);
                return $q->orderBy($sort[0], $sort[1]);
            })
            ->paginate($request->input('perPage'));
            //->paginate(100);

        return Response::json($presets);
    }

    public function loadAllPresets(Request $request) {
        $req = $request->all();

        $presets = Preset::all(); 

        $bundle = [];
        if($request->bundle_id > 0){
            $bundle = Bundle::findOrFail($request->bundle_id);

            $bunpre = BundlePreset::where('bundle_id',$request->bundle_id)->first(); 

            if($bunpre){
                $preids = BundlePreset::where('bundle_id',$request->bundle_id)->pluck('preset_id');            
                $pres = Preset::whereIn('id',$preids)->get();
                    // foreach($presets as $pre){
                    //     $mat['ratio'] = PresetMaterial::where('preset_id', $request->preset_id)->where('material_id',$mat->id)->value('ratio');

                    // }
                    $bundle['presets'] = $pres;
                    $bundle['preset_ids'] = $preids;

            }
        }

        // $orders = Order::where('project_id',0)->where('task_id',0)->pluck('id');
        // $od = OrderDetails::whereIn('order_id', $orders)->where('order_status','request')->get();
        // $reqOrders = [];
        // foreach($od as $o){
        //     $m = Material::findOrFail($o->product_id);
        //     $m->qty = $o->qty;
        //     $m->unit_price = $o->unit_price;
        //     $m->od_id = $o->id;
        //     $reqOrders[] = $m;
        // }
        // // \Log::info($req);
        // $budget_approved = OrderDetails::whereIn('order_id', $orders)->where('order_status','budget_approved')->get();
        // $appOrders = [];
        // foreach($budget_approved as $o){
        //     $m = Material::findOrFail($o->product_id);
        //     $m->qty = $o->qty;
        //     $m->unit_price = $o->unit_price;
        //     $m->od_id = $o->id;
        //     $appOrders[] = $m;
        // }
        $response['status'] = 'Success';
        $response['data'] = [
            'bundle' => $bundle,
            'presets' => $presets,
            // 'appOrders' => $appOrders,
            // 'reqOrders' => $reqOrders,
        ];
        $response['code'] = 200;

        return Response::json($response);
    }

    public function loadPresetsSelected(Request $request) {
        $presets = Preset::whereIn('id',$request->presets)->get();
        $response['status'] = 'Success';
        $response['data'] = [
            'presets' => $presets
        ];
        $response['code'] = 200;

        return Response::json($response);
    }

    public function getMaterials(Request $request) {
        $req = $request->all();

        $materials = Material::all(); 

        $preset = [];
        if($request->preset_id > 0){
            $preset = Preset::findOrFail($request->preset_id);

            $premat = PresetMaterial::where('preset_id',$request->preset_id)->first(); 

            if($premat){
                $premat = PresetMaterial::where('preset_id',$request->preset_id)->pluck('material_id');            
                $mats = Material::whereIn('id',$premat)->get();
                    foreach($mats as $mat){
                        $mat['ratio'] = PresetMaterial::where('preset_id', $request->preset_id)->where('material_id',$mat->id)->value('ratio');

                    }
                    $preset['materials'] = $mats;
                    $preset['material_ids'] = $premat;

            }
        }

        $orders = Order::where('project_id',0)->where('task_id',0)->pluck('id');
        $od = OrderDetails::whereIn('order_id', $orders)->where('order_status','request')->get();
        $reqOrders = [];
        foreach($od as $o){
            $m = Material::findOrFail($o->product_id);
            $m->qty = $o->qty;
            $m->unit_price = $o->unit_price;
            $m->od_id = $o->id;
            $reqOrders[] = $m;
        }
        // \Log::info($req);
        $budget_approved = OrderDetails::whereIn('order_id', $orders)->where('order_status','budget_approved')->get();
        $appOrders = [];
        foreach($budget_approved as $o){
            $m = Material::findOrFail($o->product_id);
            $m->qty = $o->qty;
            $m->unit_price = $o->unit_price;
            $m->od_id = $o->id;
            $appOrders[] = $m;
        }
        $response['status'] = 'Success';
        $response['data'] = [
            'preset' => $preset,
            'materials' => $materials,
            'appOrders' => $appOrders,
            'reqOrders' => $reqOrders,
        ];
        $response['code'] = 200;

        return Response::json($response);
    }

    public function getSelectedMaterials(Request $request) {
        // $preset = Preset::findOrFail($request->preset_id);
        $temp = $request->tempmaterials;
        $selected = $request->materials;
        $old = $request->oldmaterials;

        if(count($selected) > count($temp)){
            $new = array_diff($selected,$temp);
        }else{
            $new = $selected;
        }
        $materials = Material::whereIn('id',$new)->get();
                foreach($materials as $mat){
                    $ratio = PresetMaterial::where('preset_id', $request->preset_id)->where('material_id',$mat->id)->first();
                    $mat['qty'] = 1;
                    $mat['retail_price'] = $mat->unit_price;
                    $mat['files'] = MaterialImages::where('material_id', $mat->id)->get();
                    if($request->order_id){
                        $od = OrderDetails::where('order_id',$request->order_id)->where('product_id',$mat->id)->where('preset_id',0)->first();
                        if($od){
                            $mat['qty'] = $od->qty;
                            $mat['retail_price'] = $od->unit_price;
                            $mat['note'] = $od->remarks;
                        }
                    }
                    if($ratio){
                        $mat['ratio'] =$ratio->ratio;
                    }
                    else{
                        $mat['ratio'] = 0;
                    }
                }
                // \Log::info(count($selected));
                // \Log::info(count($materials));
        if(count($selected) > count($temp)){
             $materials = array_merge($old, $materials->toArray());
        }else{
            $newold = [];
            foreach($old as $o){
                foreach($materials as $m){
                    if($o['id'] == $m->id){
                        $newold[] = $o;
                    }
                }
            }
             $materials = $newold;
        }
       
        $response['status'] = 'Success';
        $response['data'] = [
            'materials' => $materials
        ];
        $response['code'] = 200;

        return Response::json($response);
    }

public function getBundlePresets(Request $request) { //jeff
        $preset_ids = BundlePreset::where('bundle_id',$request->bundle)->pluck('preset_id');
        $presets = Preset::whereIn('id',$preset_ids)->get();

            $units = [];
            $press= [];
            $qty = [];
            $prem = [];
            $ctr = 0;
            $matCtr = 1;
            foreach($presets as $p){
                $press[$ctr] = $p->id;
                $list = '';
                $pm = PresetMaterial::where('preset_id', $p->id)->orderBy('section')->get();
                $m = '';
                $m = $pm->pluck('material_id');
                if($m != ''){
                    $list = "'".implode("', '", $m->toArray())."'";
                }

                $materials = Material::whereIn('id',$m)
                                ->when($m != '', function ($q) use($list) {
                                        return $q->orderByRaw(DB::raw("FIELD(id, $list)"));
                                })
                                ->get();
                // foreach($pm as $premat){
                //     if($premat->reminder != '' && $premat->reminder != null){

                //     }
                // }
                $sec = 0;                
                $rem = '';
                $lastid = 0;                
                foreach($materials as $mat){
                    $prst = PresetMaterial::where('preset_id', $p->id)->where('material_id', $mat->id)->first();
                    if($sec != $prst->section && $prst->reminder != '' && $prst->reminder != null){
                        $lastid = PresetMaterial::where('preset_id', $p->id)->where('section', $prst->section)
                                    ->orderBy('id','DESC')->first()->id;
                        $sec = $prst->section;
                        $rem = $prst->reminder;
                    }
                    // \Log::info($lastid);
                    if($prst->id == $lastid){
                        $mat['reminder'] = $rem;
                    }
                    else{
                        $mat['reminder'] = '';
                    }
                    $od = OrderDetails::where('order_id', $request->order_id)->where('preset_id', $p->id)
                            ->where('product_id',$mat->id)->where('order_status','mob')->first();
                    $units[$ctr] = ($od ? $od->preset_unit : '');
                    $qty[$ctr] = ($od ? $od->preset_qty : 0);
                    $mat['ratio'] = PresetMaterial::where('preset_id', $p->id)->where('material_id',$mat->id)->value('ratio');
                    $mat['qty'] = ($od ? $od->qty : 0);
                    // $mat['unit'] = ($od ? $od->preset_unit : 0);
                    $mat['subtotal'] = ($od ? $od->total_price : 0);
                    $mat['final_price'] = ($od ? $od->unit_price : ($mat->retail_price> 0 ? $mat->retail_price : 1));
                    $mat['files'] = MaterialImages::where('material_id', $mat->id)->get();
                    $mat['matCtr'] = $matCtr;
                    $mat['setion'] = $prst->section;
                    // $mat['reminder'] = $prst->reminder;
                    $mat['unit'] = $prst->unit;
                    $matCtr++;
                }
                $p['materials'] = $materials;
                $ctr++;
            }
        

        $response['status'] = 'Success';
        $response['data'] = [
            'presets' => $presets,
            'units' => $units,
            'qty' => $qty,
            'press' => $press
        ];
        $response['code'] = 200;

        return Response::json($response);
    }

    public function updatePresetMaterials(Request $request) {
        $preset = PresetMaterial::where('preset_id',$request->preset_id)->delete();

        $p = Preset::findOrFail($request->preset_id);
        if($p){
                $p->name = $request->preset_name;
                $p->name_cn = $request->preset_name_cn;
                $p->save();
        }
        
        foreach($request->materials as $mat){
            $pm = PresetMaterial::create([
                    'preset_id' => $request->preset_id,
                    'material_id' => $mat['id'],
                    'ratio' => $mat['ratio'],
                ]);
        }

        $response['status'] = 'Success';
        // $response['data'] = [
        //     'materials' => $materials
        // ];
        $response['code'] = 200;

        return Response::json($response);
    }

    public function updateBundlePresets(Request $request) {
        $bundle = BundlePreset::where('bundle_id',$request->bundle_id)->delete();
        
        foreach($request->presets as $pre){
            $pm = BundlePreset::create([
                    'bundle_id' => $request->bundle_id,
                    'preset_id' => $pre['id']
                ]);
        }

        $response['status'] = 'Success';
        $response['code'] = 200;

        return Response::json($response);
    }

    public function addBundle(Request $request) {

            $group = Bundle::create([
                'name' => $request->bundle_name
            ]);

            $response['status'] = 'Success';
            $response['code'] = 200;

        return Response::json($response);
    }

}
