<?php

namespace App\Http\Controllers;
use App\Remark;
use Carbon\Carbon;

use App\BankAccounts;
use App\BankTransfer;
use App\BankTransferFiles;
use App\Project;
use App\ProjectTask;
use App\ProjectReadComments;
use App\ProjectTaskComments;
use App\ProjectTaskFiles;
use App\ProjectTaskImages;
use App\ProjectInCharge;
use App\Preset;
use App\PresetMaterial;
use App\Bundle;
use App\BundlePreset;
use App\Material;
use App\MaterialImages;
use App\Order;
use App\OrderDetails;
use App\Inventory;
use App\QuotationNotes;
use App\TransactionLogs ;

use App\User;

use Illuminate\Support\Facades\URL;

use Auth, DB, Response, Validator;
//Excel
use Illuminate\Http\Request;

use Maatwebsite\Excel\Facades\Excel;

use Status;
use PDF;
use DateTime;

use App\Helpers\MessageHelper;
use App\Exports\ProjectQuotationExport;

class ProjectController extends Controller
{

	public function manageProjects() {
		$groups = DB::table('groups as g')
			->select(DB::raw('g.id, g.name, CONCAT(u.first_name, " ", u.last_name) as leader, g.balance, g.collectables, p.latest_package as latest_package, srv.latest_service as latest_service'))
            ->leftjoin(DB::raw('(select * from users) as u'),'u.id','=','g.leader_id')
            ->leftjoin(DB::raw('
                    (
                        Select date_format(max(x.dates),"%M %e, %Y, %l:%i %p") as latest_package, x.group_id
                        from( SELECT STR_TO_DATE(created_at, "%Y-%m-%d %H:%i:%s") as dates,
                            group_id, status
                            FROM packages
                            ORDER BY dates desc
                        ) as x
                        group by x.group_id) as p'),
                    'p.group_id', '=', 'g.id')
            ->leftjoin(DB::raw('
                    (
                        Select date_format(max(cs.servdates),"%M %e, %Y") as latest_service,cs.client_id,cs.group_id
                        from( SELECT STR_TO_DATE(created_at, "%Y-%m-%d") as servdates,
                            group_id, active,client_id
                            FROM client_services
                            ORDER BY servdates desc
                        ) as cs
                        where cs.active = 1
                        group by cs.group_id) as srv'),
                    'srv.group_id', '=', 'g.id')
            ->orderBy('g.id', 'desc')
            ->get();

		$response['status'] = 'Success';
		$response['data'] = [
		    'groups' => $groups
		];
		$response['code'] = 200;

		return Response::json($response);
	}


  public function manageProjectsPaginate(Request $request, $perPage = 20) {
        $sort = $request->input('sort');
        $search = $request->input('search');

        $projects = DB::table('projects as p')
            ->select(array('p.*'))
            ->when($search != '', function ($q) use($search){
                return $q->orwhere('p.nick_name','LIKE', '%'.$search.'%')->orwhere('p.name','LIKE', '%'.$search.'%');
            })
            ->when($sort != '', function ($q) use($sort){
                $sort = explode('-' , $sort);
                return $q->orderBy($sort[0], $sort[1]);
            })
            // ->where('pt.prio','>',0)
            ->paginate($request->input('perPage'));
            //->paginate(100);

        $pr = Project::get();
        foreach($pr as $p){
            $p->urgent = ProjectTask::where('project_id', $p->id)->where('prio', '!=' ,0)->count();
            $p->team = ProjectTask::where('project_id', $p->id)->where('assigned_to', '!=' ,'')->count();
            $incharge = ProjectInCharge::where('project_id', $p->id)->first();
            $p->incharge = '';
            if($incharge){
                $p->incharge = User::where('id',$incharge->user_id)->first()->first_name;
            }
        }

        // return Response::json($projects);
        $response['status'] = 'Success';
        $response['data'] = [
            'projects' => $projects,
            'tasks' => $pr
        ];
        $response['code'] = 200;

        return Response::json($response);
    }

    public function projectSearch(Request $request){
        $keyword = $request->input('search');
        $users = '';

        $projects = DB::connection()
            ->table('projects as a')
            ->select(DB::raw('
                a.id,a.name'))
                //->orwhere('a.id','LIKE', '%' . $keyword .'%')
                ->orwhere('name','LIKE', '%' . $keyword .'%')
                ->get();


        $json = [];
        foreach($projects as $p){
          $json[] = array(
              'id' => $p->id,
              'name' => $p->name,
          );
        }
        $response['status'] = 'Success';
        $response['data'] =  $json;
        $response['code'] = 200;

        return Response::json($response);
    }

    public function show($id){
        $users = '';

        $project = DB::connection()
            ->table('projects as a')
            ->where('id', $id)
            ->first();
        $project->incharge = ProjectInCharge::where('project_id',$id)->pluck('user_id');

        $ptasks = ProjectTask::where('parent_id', '0')->where('project_id', $id)
                ->orderBy('project_tasks.id', 'asc')
                ->get();
        $tasks = [];

        //\Log::info($ptasks);

        $ctr = 0;
        foreach($ptasks as $p){
            $tasks[$ctr]['id'] = $p->id;
            $tasks[$ctr]['label'] = $p->label;
            $tasks[$ctr]['user'] = $p->assigned_to;
            $tasks[$ctr]['start'] = $p->start;
            if($p->start == null && $p->end == null){
                $tasks[$ctr]['duration'] = null;
            }
            else{
                $tasks[$ctr]['duration'] = intval((Carbon::parse($p->end))->diffInDays(Carbon::parse($p->start)));
            }
            $tasks[$ctr]['progress'] = $p->progress;
            $tasks[$ctr]['type'] = $p->type;
            $tasks[$ctr]['parentId'] = $p->parent_id;
            $tasks[$ctr]['tasks'] = $p->tasks;
            $tasks[$ctr]['status'] = "<span style=\"color:red;\">". $p->status ."</span>";
            $tasks[$ctr]['style']['base']['fill'] = '#666';
            $tasks[$ctr]['style']['base']['stroke'] = '#666';

            $ctr++;
        }

        $users = User::select('id',DB::raw("CONCAT(first_name,' ',last_name) AS label"), DB::raw("CONCAT(substr(first_name,1,1),last_name,id,' ') AS value"))->where('id','!=',Auth::user()->id)->get();


        $response['status'] = 'Success';
        $response['data'] =  $project;
        $response['tasks'] =  $tasks;
        // $response['users'] =  $users;
        $response['code'] = 200;

        return Response::json($response);
    }

    public function getAllTasks(Request $request){

        // $filter = $request->filter;
        $filter = explode (",", $request->filter); 
        $flagNull = false;
        foreach($filter  as $key => $f){
            if($f == "EMPTY"){
                $filter[$key] = NULL;
                $flagNull = true;
            }
            if($f == "ONGOING"){
                 $filter[$key]= "ON-GOING";
            }
        }

        //\Log::info($filter);

        $sort = $request->input('sort');
        $search = $request->input('search');
        $project_id = $request->input('project_id');
        $t = ProjectTask::where('project_id',$project_id)->where('parent_id', 0)->get();
        $tc = $t->count();

        $tasks = ProjectTask::where('project_id',$project_id)
            ->where('parent_id', 0)
            // ->whereIn('progress_status', $filter)
            ->when($flagNull == false, function ($q) use($filter){
                return $q->whereIn('progress_status', $filter);
            })
            ->when($flagNull == true, function ($q) use($filter) {
                $q->where(function ($query) use($filter){
                           return  $query->orwhereIn('progress_status',$filter)->orWhereNull('progress_status');                  
                        });
            })
            ->when($search != '', function ($q) use($search){
                return $q->where('id','LIKE', '%'.$search.'%')->orwhere('label','LIKE', '%'.$search.'%');
            })
            ->when($sort != '', function ($q) use($sort){
                $sort = explode('-' , $sort);
                return $q->orderBy($sort[0], $sort[1]);
            })
            ->orderBy('id')
            ->paginate($tc);
            //->paginate(100);

        return Response::json($tasks);
    }

    public function getPerTeamTasks(Request $request){

        $team = $request->input('team');
        $sort = $request->input('sort');
        $search = $request->input('search');
        $project_id = $request->input('project_id');
        $users = [];
        $taskcount = 0;
        $user_id = Auth::User()->id;
        if($team != 'MENTIONED'){
            $t = ProjectTask::where('assigned_to',$team)->where('parent_id', 0)
                    ->where(function ($query)  {
                        $query->orwhere('progress_status','PENDING')
                            ->orwhere('progress_status','PREPARATION')
                            ->orwhere('progress_status','ON-GOING')
                            ->orwhere('progress_status','HOLD')
                            ->orwhere('progress_status',NULL);
                    })
                    ->get();

            $tc = $t->count();

            $tasks = [];
            if($tc > 0){
                $tasks = DB::table('project_tasks as pt')
                    ->select(DB::raw('pt.*,p.name as project_name'))
                    ->leftjoin(DB::raw('(select * from projects) as p'),'p.id','=','pt.project_id')
                    ->where('pt.assigned_to',$team)->where('pt.parent_id', 0)
                    ->where(function ($query)  {
                        $query->orwhere('pt.progress_status','PENDING')
                            ->orwhere('pt.progress_status','PREPARATION')
                            ->orwhere('pt.progress_status','ON-GOING')
                            ->orwhere('pt.progress_status','HOLD')
                            ->orwhere('pt.progress_status',NULL);
                    })
                    ->when($search != '', function ($q) use($search){
                        return $q->where('pt.id','LIKE', '%'.$search.'%')->orwhere('pt.label','LIKE', '%'.$search.'%');
                    })
                    ->when($sort != '', function ($q) use($sort){
                        $sort = explode('-' , $sort);
                        return $q->orderBy($sort[0], $sort[1]);
                    })
                    ->orderBy('pt.id')
                    ->paginate($tc);
            }
        }
        else{
            if($search == '' || $search == null){
                $fname = Auth::User()->first_name;
                $lname = Auth::User()->last_name;
                $uid = Auth::User()->id;
            }
            else{
                $user = User::where('id', $search)->first();
                $fname = $user->first_name;
                $lname = $user->last_name;
                $uid = $user->id;
            }
            $name = substr( $fname, 0, 1).$lname.$uid; 
            $tasks = [];
            $cc = 0;

            $read = ProjectReadComments::where('user_id',$uid)->pluck('comment_id');
            $mentioned = ProjectTask::where('label','LIKE', '%'.$name.'%')
                    //->where('is_read', 0)
                    ->orderBy('id','DESC')
                    ->get();
            $m1 = $mentioned->pluck('id');
            $c1 = count($m1->toArray());
            $mentioned2 = ProjectTaskComments::where('comment','LIKE', '%'.$name.'%')
                        ->whereNotIn('id', $read)
                    ->orderBy('id','DESC')
                    ->get();
            $m2 = $mentioned2->pluck('proj_task_id');
            $cc = count($m2->toArray());
            
            //$m = array_merge($mentioned->toArray(), $mentioned2->toArray());
            $tc = $c1 + $cc;

            $users = User::select('id','first_name','last_name')
                        // ->where('id','!=',Auth::user()->id)
                        ->get();

            $srt = '';
            if($sort != ''){
                $srt = explode('-' , $sort);
                $srt = $srt[0];
            }

            if($tc > 0){     
                $tasks =  DB::table('project_tasks as pt')
                        ->select(DB::raw('pt.*,p.name as project_name'))
                        ->leftjoin(DB::raw('(select * from projects) as p'),'p.id','=','pt.project_id')
                        //->where('pt.is_read', 0)
                        ->where('pt.label','LIKE', '%'.$name.'%')
                        ->where('pt.parent_id', 0)
                        ->when($cc > 0, function ($q) use($m2) {
                            //\Log::info($m2);
                                            return $q->orwhereIn('pt.id', $m2);
                                    })
                        ->groupBy('pt.id')
                        //->orderBy('pt.id','DESC')
                        ->when($srt == 'project' , function ($q) use($sort) {
                            $sort = explode('-' , $sort);
                            //if($sort[0] == 'qty') {
                                return $q->orderBy('p.name', $sort[1]);
                            //}
                        })
                        ->when($srt == 'progress_status' , function ($q) use($sort) {
                            $sort = explode('-' , $sort);
                            //if($sort[0] == 'qty') {
                                return $q->orderBy('pt.progress_status', $sort[1]);
                            //}
                        })
                        ->when($srt == 'label' || $srt == 'created_at' || $srt == 'status', function ($q) use($sort) {
                            $sort = explode('-' , $sort);
                            if($sort[0] == 'created_at') {
                                $sort[0] = 'pt.id';
                            }
                                return $q->orderBy($sort[0], $sort[1]);
                            //}
                        })
                        ->paginate($tc);

                $taskcount =  DB::table('project_tasks as pt')
                        ->select(DB::raw('pt.*,p.name as project_name'))
                        ->leftjoin(DB::raw('(select * from projects) as p'),'p.id','=','pt.project_id')
                        //->where('pt.is_read', 0)
                        ->where('pt.label','LIKE', '%'.$name.'%')
                        ->where('pt.parent_id', 0)
                        ->when($cc > 0, function ($q) use($m2) {
                            //\Log::info($m2);
                                            return $q->orwhereIn('pt.id', $m2);
                                    })
                        ->groupBy('pt.id')
                        ->orderBy('pt.id','DESC')
                        ->get()->count();
            }
        }
        $response['status'] = 'Success';
        $response['tasks'] =  $tasks;
        $response['taskcount'] =  $taskcount;
        $response['users'] =  $users;
        $response['user_id'] =  $user_id;
        return Response::json($response);
    }

    public function getPerTeamCount(Request $request){

        $teamSite = ProjectTask::where('assigned_to','SITE')->where('parent_id', 0)
                ->where(function ($query)  {
                    $query->orwhere('progress_status','PENDING')
                        ->orwhere('progress_status','PREPARATION')
                        ->orwhere('progress_status','ON-GOING')
                        ->orwhere('progress_status','HOLD')
                        ->orwhere('progress_status',NULL);
                })->orderBy('id','DESC')
                ->get()->count();

        $teamDesign = ProjectTask::where('assigned_to','DESIGN')->where('parent_id', 0)
                ->where(function ($query)  {
                    $query->orwhere('progress_status','PENDING')
                        ->orwhere('progress_status','PREPARATION')
                        ->orwhere('progress_status','ON-GOING')
                        ->orwhere('progress_status','HOLD')
                        ->orwhere('progress_status',NULL);
                })->orderBy('id','DESC')
                ->get()->count();

        $teamWarehouse = ProjectTask::where('assigned_to','WAREHOUSE')
                ->where(function ($query)  {
                    $query->orwhere('progress_status','PENDING')
                        ->orwhere('progress_status','PREPARATION')
                        ->orwhere('progress_status','ON-GOING')
                        ->orwhere('progress_status','HOLD')
                        ->orwhere('progress_status',NULL);
                })->orderBy('id','DESC')
                ->get()->count();

        $teamOffice = ProjectTask::where('assigned_to','OFFICE ARCH')->where('parent_id', 0)
                ->where(function ($query)  {
                    $query->orwhere('progress_status','PENDING')
                        ->orwhere('progress_status','PREPARATION')
                        ->orwhere('progress_status','ON-GOING')
                        ->orwhere('progress_status','HOLD')
                        ->orwhere('progress_status',NULL);
                })->orderBy('id','DESC')
                ->get()->count();

        $teamAdmin = ProjectTask::where('assigned_to','ADMIN')->where('parent_id', 0)
                ->where(function ($query)  {
                    $query->orwhere('progress_status','PENDING')
                        ->orwhere('progress_status','PREPARATION')
                        ->orwhere('progress_status','ON-GOING')
                        ->orwhere('progress_status','HOLD')
                        ->orwhere('progress_status',NULL);
                })->orderBy('id','DESC')
                ->get()->count();

        $teamClient = ProjectTask::where('assigned_to','CLIENT')->where('parent_id', 0)
                ->where(function ($query)  {
                    $query->orwhere('progress_status','PENDING')
                        ->orwhere('progress_status','PREPARATION')
                        ->orwhere('progress_status','ON-GOING')
                        ->orwhere('progress_status','HOLD')
                        ->orwhere('progress_status',NULL);
                })->orderBy('id','DESC')
                ->get()->count();

        $teamSubcon = ProjectTask::where('assigned_to','SUBCON')->where('parent_id', 0)
                ->where(function ($query)  {
                    $query->orwhere('progress_status','PENDING')
                        ->orwhere('progress_status','PREPARATION')
                        ->orwhere('progress_status','ON-GOING')
                        ->orwhere('progress_status','HOLD')
                        ->orwhere('progress_status',NULL);
                })->orderBy('id','DESC')
                ->get()->count();

        $teamPurchasing = ProjectTask::where('assigned_to','PURCHASING')->where('parent_id', 0)
                ->where(function ($query)  {
                    $query->orwhere('progress_status','PENDING')
                        ->orwhere('progress_status','PREPARATION')
                        ->orwhere('progress_status','ON-GOING')
                        ->orwhere('progress_status','HOLD')
                        ->orwhere('progress_status',NULL);
                })->orderBy('id','DESC')
                ->get()->count();

            $fname = Auth::User()->first_name;
            $lname = Auth::User()->last_name;
            $uid = Auth::User()->id;
            $name = substr( $fname, 0, 1).$lname.$uid; 
            $tasks = [];
            $cc = 0;
            $mentioned = ProjectTask::where('label','LIKE', '%'.$name.'%')
                    //->where('is_read', 0)
                    ->orderBy('id','DESC')
                    ->get()->count();
            $mentioned2 = ProjectTaskComments::where('comment','LIKE', '%'.$name.'%')
                    ->orderBy('id','DESC')
                    ->get()->count();
            


        $response['status'] = 'Success';
        $response['teamSite'] =  $teamSite;
        $response['teamDesign'] =  $teamDesign;
        $response['teamWarehouse'] = $teamWarehouse;
        $response['teamOffice'] = $teamOffice;
        $response['teamAdmin'] = $teamAdmin;
        $response['teamClient'] = $teamClient;
        $response['teamSubcon'] = $teamSubcon;
        $response['teamPurchasing'] = $teamPurchasing;
        $response['mentioned'] = $mentioned + $mentioned2;
        $response['code'] = 200;

        return Response::json($response);
    }

    public function getProfile($id){
        $users = '';

        $project = DB::connection()
            ->table('projects as a')
            ->where('id', $id)
            ->first();
        $project->incharge = ProjectInCharge::where('project_id',$id)->pluck('user_id');

        $ptasks = ProjectTask::where('parent_id', '0')->where('project_id', $id)
                ->orderBy('project_tasks.id', 'asc')
                ->get();
        $tasks = [];

        // \Log::info($ptasks);

        $ctr = 0;
        foreach($ptasks as $p){
            // \Log::info($p->tasks);
            $tasks[$ctr]['id'] = $p->id;
            $tasks[$ctr]['label'] = $p->label;
            $tasks[$ctr]['user'] = $p->assigned_to;
            $tasks[$ctr]['start'] = $p->start;
            if($p->start == null && $p->end == null){
                $tasks[$ctr]['duration'] = null;
            }
            else{
                $tasks[$ctr]['duration'] = intval((Carbon::parse($p->end))->diffInDays(Carbon::parse($p->start)));
            }
            $tasks[$ctr]['progress'] = $p->progress;
            $tasks[$ctr]['type'] = $p->type;
            $tasks[$ctr]['parentId'] = $p->parent_id;
            $tasks[$ctr]['tasks'] = $p->tasks;
            $tasks[$ctr]['status'] = "<span style=\"color:red;\">". $p->status ."</span>";
            $tasks[$ctr]['style']['base']['fill'] = '#666';
            $tasks[$ctr]['style']['base']['stroke'] = '#666';

            $ctr++;
        }

        $tsk = ProjectTask::where('parent_id', '0')->where('project_id', $id)
                ->orderBy('project_tasks.id', 'asc')
                ->get();
            foreach($tsk as $t){
                $t->files =  ProjectTaskImages::where('task_id',$t->id)->get();
            }

        $tskids = $tsk->pluck('id');

        $commentfiles = ProjectTaskComments::whereIn('proj_task_id', $tskids)
                ->get();
            foreach($commentfiles as $c){
                $c->files =  ProjectTaskFiles::where('comment_id',$c->id)->get();
            }

        $users = User::select('id',DB::raw("CONCAT(first_name,' ',last_name) AS label"), DB::raw("CONCAT(substr(first_name,1,1),last_name,id,' ') AS value"))
                // ->where('id','!=',Auth::user()->id)
                ->get();


        $response['status'] = 'Success';
        $response['data'] =  $project;
        $response['tasks'] =  $tasks;
        $response['files'] =  $tsk;
        $response['commentfiles'] =  $commentfiles;
        $response['users'] =  $users;
        $response['code'] = 200;

        return Response::json($response);
    }

    public function uploadTaskImages(Request $request) {
        foreach($request->uplPhotos as $item) {
                if($item['file_path'] == 'image.png'){
                    $item['file_path'] = time().'image.png';
                }
                $path = $request->type.'/' . $request->id . '/'.$item['file_path'];
                $pfile = ProjectTaskImages::create([
                    'task_id' => $request->id,
                    'file_path' => $path,
                ]);

                $imgData = [
                    'imgBase64' => $item['imgBase64'],
                    'file_path' => $request->id,
                    'img_name' => $item['file_path']
                ];

                $this->uploadDocuments($imgData, $request->type);
            }

        $response['status'] = 'Success';
        $response['code'] = 200;
        

        return Response::json($response);
    }

    public function bankTransfer(Request $request) {

        $b1 = BankAccounts::findorfail($request->transfer_from);
        $b1->balance = $b1->balance - $request->amount;
        $b1->save();

        $b2 = BankAccounts::findorfail($request->transfer_to);
        $b2->balance = $b2->balance + $request->amount_transferred;
        $b2->save();

        $transfer = new BankTransfer;
        $transfer->transfer_from = $request->transfer_from;
        $transfer->transfer_to = $request->transfer_to;
        $transfer->notes = $request->notes;
        $transfer->rate = $request->rate;
        $transfer->amount = $request->amount;
        $transfer->save();

        $trans = new TransactionLogs;
        $trans->bank_id = $b1->id;
        $trans->payment_id = 0;
        $trans->transfer_id = $transfer->id;
        $trans->user_id = Auth::User()->id;
        $trans->detail = '';
        $trans->rate = $request->rate;
        $trans->payment = $request->amount;
        $trans->balance = $b1->balance;
        $trans->save();

        $trans = new TransactionLogs;
        $trans->bank_id = $b2->id;
        $trans->payment_id = 0;
        $trans->transfer_id = $transfer->id;
        $trans->user_id = Auth::User()->id;
        $trans->detail = '';
        $trans->rate = $request->rate;
        $trans->deposit = $request->amount_transferred;
        $trans->balance = $b2->balance;
        $trans->save();

        foreach($request->uplPhotos as $item) {
                if($item['file_path'] == 'image.png'){
                    $item['file_path'] = time().'image.png';
                }
                $path = $request->type.'/' . $transfer->id . '/'.$item['file_path'];
                $pfile = BankTransferFiles::create([
                    'bank_transfer_id' => $transfer->id,
                    'file_path' => $path,
                ]);

                $imgData = [
                    'imgBase64' => $item['imgBase64'],
                    'file_path' => $transfer->id,
                    'img_name' => $item['file_path']
                ];

                $this->uploadDocuments($imgData, $request->type);
            }

        $response['status'] = 'Success';
        $response['code'] = 200;
        

        return Response::json($response);
    }

    public function uploadDocuments($data, $doctype) {
        // \Log::info($doctype);
        $img64 = $data['imgBase64'];

        list($type, $img64) = explode(';', $img64);
        list(, $img64) = explode(',', $img64); 
        // if($data['img_name'] == 'image.png'){
        //     $data['img_name'] = time().'image.png';
        // }
        
        if($img64!=""){ // storing image in storage/app/public Folder 
                \Storage::disk('public')->put($doctype.'/' . $data['file_path'] . '/' . $data['img_name'], base64_decode($img64)); 
        } 
    }

    public function addTask(Request $request) {
        $validator = Validator::make($request->all(), [
            'project_id' => 'required',
            //'label' => 'required'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;
        } else {

                // if($request->task_id != 0){
                //     $pt = ProjectTask::findOrFail($request->task_id);
                //     $request->parent_id = $pt->parent_id;
                // }

                ProjectTask::create([
                        'parent_id' => ($request->parent_id == 0) ? $request->task_id : $request->parent_id,
                        'project_id' => $request->project_id,
                        'label' => ($request->label == null ? "" : $request->label),
                        'type'  => ($request->parent_id == 0 ? "project" : "task"),
                        'progress' => 0,
                        'dependent' => null,
                        'status' => ($request->parent_id == 0 ? "" : "purchase order"),
                        'last_mod' => Auth::user()->first_name
                        //'parent_id' => $request->parent_id
                    ]);

            $response['status'] = 'Success';
            $response['code'] = 200;
        }

        return Response::json($response);
    }

    public function editTask(Request $request) {
        // \Log::info($request);
        $validator = Validator::make($request->all(), [
            'project_id' => 'required',
            'id' => 'required'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;
        } else {
            $ss = $request->site_started;

            if($request->progress_status != '' || $request->progress_status != null){
                $ss = 1;
            }

            $pt = ProjectTask::findOrFail($request->id);
            $pt->label = $request->label;
            $pt->assigned_to = $request->assigned_to;
            $pt->start = $request->start;
            $pt->end = $request->end;
            $pt->parent_id = $request->parent_id;
            $pt->task_desc = $request->task_desc;
            $pt->prio = $request->prio ? 1 : 0;
            $pt->progress = $request->progress > 100 ? 100 : $request->progress;
            $pt->progress_status = $request->progress_status;
            $pt->site_started = $ss;
            $pt->site_completion = $request->site_completion;
            $pt->last_mod = Auth::user()->first_name;
            $pt->save();

            if($request->parent_id > 0){
                $child = ProjectTask::where('parent_id', $request->parent_id)->get(); 
                $child = $child->sum('progress')/ $child->count();
                $pt2 = ProjectTask::findOrFail($request->parent_id);   
                $pt2->progress = $child;
                $pt2->save();
            }
                

            $response['status'] = 'Success';
            $response['code'] = 200;
            
        }

        return Response::json($response);
    }

    public function getTask($task_id, Request $request) {
            $task = ProjectTask::findorfail($task_id);
            $comments = ProjectTaskComments::where('proj_task_id',$task->id)->orderBy('id','desc')->get();
            foreach($comments as $c){
                $user = User::where('id', $c->user_id)->first();
                $c->user = $user->first_name.' '.$user->last_name;
                $c->files = ProjectTaskFiles::where('comment_id', $c->id)->get();
            }
            $is_parent = ProjectTask::where('parent_id', $task_id)->first();
            $task->is_parent = ($is_parent) ? 1 : 0;

            $users = User::select('id',DB::raw("CONCAT(first_name,' ',last_name) AS label"), DB::raw("CONCAT(substr(first_name,1,1),last_name,id,' ') AS value"))
            // ->where('id','!=',Auth::user()->id)
            ->get();

            $response['users'] = $users;
            $response['data'] = $task;
            $response['comments'] = $comments;
            $response['status'] = 'Success';
            $response['code'] = 200;
        
        return Response::json($response);
    }

    public function getTaskWithOrders($task_id, Request $request) {
            $task = ProjectTask::findorfail($task_id);

            $orders = Order::where('task_id', $task_id)->get();
            $ctr = 0;
            foreach($orders as $o){
                $order_id = $o->id;
                $preset_ids = OrderDetails::where('order_id',$order_id)->where('preset_id','!=',0)->orderBy('preset_id')
                ->pluck('preset_id');

                $pre = Preset::whereIn('id',$preset_ids)->get();
                $taskdetflag = 0;
                foreach($pre as $p){
                    $pm = PresetMaterial::where('preset_id', $p->id)->get();
                    $m = $pm->pluck('material_id');

                    $materials = Material::whereIn('id',$m)->get();
                    $ordet = OrderDetails::where('order_id',$order_id)->where('preset_id', $p->id)
                                    ->get();
                    $n = '';
                    if(!$task->details){
                        $orderdet = OrderDetails::where('order_id',$order_id)->where('preset_id', $p->id)
                                    ->first();
                        if($orderdet){
                            $taskdetflag = 1;
                            $task->details = $orderdet;
                        }
                    }
                    foreach($ordet as $mat){
                        $materials = Material::where('id',$mat->product_id)->first();
                        $mat->material_name = $materials->name;
                        $mat->unit = $materials->unit;
                    }
                    $p['materials'] = $ordet;
                }



                $add = OrderDetails::where('order_id',$order_id)->where('preset_id',0)->get();
                foreach($add as $a=>$b){
                    $mat = Material::findorfail($b->product_id);
                    $b->material_name = $mat->name;
                    $b->unit = $mat->unit;
                    if($taskdetflag == 0){
                        $orderdet = OrderDetails::where('order_id',$order_id)->where('preset_id', 0)
                                    ->first();
                        $task->details = $orderdet;
                    }
                }
                $o->pre = $pre;
                $o->add = $add;
                $ctr++;
            }

            $response['orders'] = $orders;
            $response['data'] = $task;
            $response['status'] = 'Success';
            $response['code'] = 200;
        
        return Response::json($response);
    }

    public function getOrderDetail($order_id, Request $request) {
        $order = Order::findorfail($order_id);
        $project = Project::findorfail($order->project_id);
        $task = ProjectTask::findorfail($order->task_id);

        $preset_ids = OrderDetails::where('order_id',$order_id)->where('preset_id','!=',0)->orderBy('preset_id')
                ->pluck('preset_id');

        $pre = Preset::whereIn('id',$preset_ids)->get();
        $taskdetflag = 0;
        foreach($pre as $p){
            $pm = PresetMaterial::where('preset_id', $p->id)->get();
            $m = $pm->pluck('material_id');

            $materials = Material::whereIn('id',$m)->get();
            $ordet = OrderDetails::where('order_id',$order_id)->where('preset_id', $p->id)
                            ->get();
            $n = '';
            if(!$task->details){
                $orderdet = OrderDetails::where('order_id',$order_id)->where('preset_id', $p->id)
                            ->first();
                if($orderdet){
                    $taskdetflag = 1;
                    $task->details = $orderdet;
                }
                // else{
                //     $orderdet = OrderDetails::where('order_id',$order_id)->where('preset_id', 0)
                //             ->first();
                //     $task->details = $orderdet;
                // }
            }
            foreach($ordet as $mat){
                $materials = Material::where('id',$mat->product_id)->first();
                //$ordet = OrderDetails::where('preset_id', $p->id)->where('product_id',$mat->id)->where('order_id', $order_id);
                //$mat['qty'] = $ordet->value('qty');
                //$mat['subtotal'] = $ordet->value('total_price');
                //$mat['final_price'] = $ordet->value('unit_price');
                $mat->material_name = $materials->name;
                $mat->unit = $materials->unit;
                //$n = $ordet->value('remarks');
            }
            $p['materials'] = $ordet;
            //$p['note'] = $n;
        }



        $add = OrderDetails::where('order_id',$order_id)->where('preset_id',0)->get();
        foreach($add as $a=>$b){
            $mat = Material::findorfail($b->product_id);
            $b->material_name = $mat->name;
            $b->unit = $mat->unit;
            if($taskdetflag == 0){
                $orderdet = OrderDetails::where('order_id',$order_id)->where('preset_id', 0)
                            ->first();
                $task->details = $orderdet;
            }
        }

        $response['order'] = $order;
        $response['project'] = $project;
        $response['task'] = $task;
        $response['materials'] = $pre;
        $response['addmaterials'] = $add;
        $response['status'] = 'Success';
        $response['code'] = 200;
        
        return Response::json($response);
    }    


    public function getOrderDetail2($order_id, Request $request) {
        $order = Order::findorfail($order_id);
        $project = Project::findorfail($order->project_id);
        $task = ProjectTask::findorfail($order->task_id);

        $order_details = OrderDetails::where('order_id',$order_id)->where('preset_id','!=',0)->orderBy('preset_id')->get();
        $preset_ids = $order_details->pluck('preset_id');
        $product_ids = $order_details->pluck('product_id');

        $pre = Preset::whereIn('id',$preset_ids)->get();
        $taskdetflag = 0;
        foreach($pre as $p){
            $pm = PresetMaterial::where('preset_id', $p->id)->whereIn('material_id', $product_ids)->get();
            $m = $pm->pluck('material_id');

            $materials = Material::whereIn('id',$m)->get();
            $ordet = OrderDetails::where('order_id',$order_id)->where('preset_id', $p->id)
                            ->get();
            $n = '';
            if(!$task->details){
                $orderdet = OrderDetails::where('order_id',$order_id)->where('preset_id', $p->id)
                            ->first();
                $orderdet->requested_at = $order->created_at;
                if($orderdet){
                    $taskdetflag = 1;
                    $task->details = $orderdet;
                }
            }
            foreach($ordet as $mat){
                $materials = Material::where('id',$mat->product_id)->first();
                $mat->material_name = $materials->name;
                $mat->unit = $materials->unit;
                $mat->created_at = $order->created_at;
                $mat->requested_at = $order->created_at;
                //$n = $ordet->value('remarks');
            }
            $p['materials'] = $ordet;
            //$p['note'] = $n;
        }



        $add = OrderDetails::where('order_id',$order_id)->where('preset_id',0)->get();
        foreach($add as $a=>$b){
            $mat = Material::findorfail($b->product_id);
            $b->material_name = $mat->name;
            $b->unit = $mat->unit;
            $b->created_at = $order->created_at;
            $b->requested_at = $order->created_at;
            if($taskdetflag == 0){
                $orderdet = OrderDetails::where('order_id',$order_id)->where('preset_id', 0)
                            ->first();
                $orderdet->requested_at = $order->created_at;
                
                $task->details = $orderdet;
            }
        }

        $response['order'] = $order;
        $response['project'] = $project;
        $response['task'] = $task;
        $response['materials'] = $pre;
        $response['addmaterials'] = $add;
        $response['status'] = 'Success';
        $response['code'] = 200;
        
        return Response::json($response);
    }   

    public function getOrderDetailMultiple(Request $request) {
        $projids = Order::whereIn('id',$request->ids)->groupBy('project_id')->pluck('project_id');
        // $taskids = Order::whereIn('id',$request->ids)->pluck('task_id');
        $projects = Project::whereIn('id',$projids)->get();
        $pushOrderIds = [];
        $totalkg = 0;

        foreach($projects as $p){
            $orders =  Order::whereIn('id',$request->ids)->where('project_id', $p->id)->get();
            $p['orders'] = $orders;
            foreach($orders as $o){
                $task = ProjectTask::findorfail($o->task_id);
                $o['tasks'] = $task; 
                $preset_ids = OrderDetails::where('order_id',$o->id)->where('preset_id','!=',0)
                                ->where('order_status',$request->type)
                                ->orderBy('preset_id')->pluck('preset_id');
                $pre = Preset::whereIn('id',$preset_ids)->get();
                $o['presets'] = $pre;
                foreach($pre as $p){
                    $o['task_name'] = $task->label;
                    $pm = PresetMaterial::where('preset_id', $p->id)->get();
                    $m = $pm->pluck('material_id');

                    $materials = Material::whereIn('id',$m)->get();
                    $n = '';
                    // if(!$task->details){
                    //     $task->details = OrderDetails::where('preset_id', $p->id)->where('order_id', $o->id)->first();
                    // }
                    foreach($materials as $mat){
                        $ordet = OrderDetails::where('preset_id', $p->id)->where('product_id',$mat->id)->where('order_id', $o->id)->where('order_status',$request->type);
                        $mmmm = Material::findOrFail($mat->id);
                        //$pushOrderIds[] = $ordet->pluck('id');
                        $mat['order_detail_id'] = $ordet->value('id');
                        $mat['qty'] = $ordet->value('qty');
                        $mat['qty2'] = $ordet->value('qty');
                        $mat['subtotal'] = $ordet->value('total_price');
                        $mat['final_price'] = $ordet->value('unit_price');
                        $totalkg += $ordet->value('qty') * $mmmm->weight;
                        //$mat['details'] = $ordet;
                        // $n = $ordet->value('remarks');
                    }
                    $p['materials'] = $materials;
                    //$p['note'] = $n;
                }

                $add = OrderDetails::where('order_id',$o->id)->where('order_status',$request->type)->where('preset_id',0)->get();
                foreach($add as $a=>$b){
                    $mat = Material::findorfail($b->product_id);
                    $b->material_name = $mat->name;
                    $b->qty2 = $b->qty;
                }
                $o['addmaterials'] = $add;
            }
        }

        $response['data'] = $projects;
        $response['totalkg'] = $totalkg;
        $response['status'] = 'Success';
        $response['code'] = 200;
        
        return Response::json($response);
    }    

    public function getOrderDetailMultiple2(Request $request) {
        $projids = Order::whereIn('id',$request->ids)->groupBy('project_id')->pluck('project_id');
        // $taskids = Order::whereIn('id',$request->ids)->pluck('task_id');
        $projects = Project::whereIn('id',$projids)->get();
        $pushOrderIds = [];
        $totalkg = 0;

        foreach($projects as $p){
            $orders =  Order::whereIn('id',$request->ids)->where('project_id', $p->id)->get();
            $p['orders'] = $orders;
            foreach($orders as $o){
                $task = ProjectTask::findorfail($o->task_id);
                $o['tasks'] = $task; 
                $ordets = OrderDetails::where('order_id',$o->id)->where('preset_id','!=',0)
                                ->where('order_status',$request->type)
                                ->get();
                $preset_ids = $ordets->pluck('preset_id');
                $mat_ids = $ordets->pluck('product_id');
                                \Log::info($mat_ids);
                $pre = Preset::whereIn('id',$preset_ids)->get();
                $o['presets'] = $pre;

                //foreach($ordets as $p){
                    $o['task_name'] = $task->label;

                    $materials = Material::whereIn('id',$mat_ids)->get();
                    $n = '';
                    foreach($ordets as $ordet){
                        //$ordet = OrderDetails::where('product_id',$mat->id)->where('order_id', $o->id)->where('order_status',$request->type);
                        $mmmm = Material::findOrFail($ordet->product_id);
                        //$pushOrderIds[] = $ordet->pluck('id');
                        $ordet['order_detail_id'] = $ordet->id;
                        $ordet['qty'] = $ordet->qty;
                        $ordet['qty2'] = $ordet->qty;
                        $ordet['subtotal'] = $ordet->total_price;
                        $ordet['final_price'] = $ordet->unit_price;
                        $ordet['name'] = $mmmm->name;
                        $totalkg += $ordet->qty * $mmmm->weight;
                        //$mat['details'] = $ordet;
                        // $n = $ordet->value('remarks');
                    }
                    $o['materials'] = $ordets;
                    //$p['note'] = $n;
                //}

                $add = OrderDetails::where('order_id',$o->id)->where('order_status',$request->type)->where('preset_id',0)->get();
                foreach($add as $a=>$b){
                    $mat = Material::findorfail($b->product_id);
                    $b->material_name = $mat->name;
                    $b->qty2 = $b->qty;
                }
                $o['addmaterials'] = $add;
            }
        }

        $response['data'] = $projects;
        $response['totalkg'] = $totalkg;
        $response['status'] = 'Success';
        $response['code'] = 200;
        
        return Response::json($response);
    }

	public function store(Request $request) {

        	$proj = Project::create([
        		'name' => $request->proj_name,
        		'nick_name' => $request->proj_nick,
        		'address' => $request->proj_address,
                'address_link' => $request->proj_address_link,
                'sqm' => $request->proj_sqm,
                'est_finished_date' => $request->estimated_date,
                'contact_number' => $request->client_num,
                'contact_person' => $request->client_ename,
                'contact_email' => $request->client_email,
                'admin_contact' => $request->admin_num,
                'admin_name' => $request->admin_name,
                'admin_email' => $request->admin_email,
                'remarks' => $request->remarks,
        	]);

            foreach($request->incharge as $incharge){
                $pic = ProjectInCharge::create([
                    'project_id' => $proj->id,
                    'user_id' => $incharge
                ]);
            }

        	$response['status'] = 'Success';
        	$response['code'] = 200;

        return Response::json($response);
	}

    public function update($id,Request $request) {

            $proj = Project::findOrFail($id);
            if($proj){
                $proj->name = $request->name;
                $proj->nick_name = $request->nick_name;
                $proj->address = $request->address;
                $proj->address_link = $request->address_link;
                $proj->sqm = $request->sqm;
                $proj->est_finished_date = $request->est_finished_date;
                $proj->contact_number = $request->contact_number;
                $proj->contact_person = $request->contact_person;
                $proj->contact_email = $request->contact_email;
                $proj->admin_contact = $request->admin_contact;
                $proj->admin_name = $request->admin_name;
                $proj->admin_email = $request->admin_email;
                $proj->remarks = $request->remarks;
                $proj->save();
            }

            $del_incharge = ProjectInCharge::where('project_id',$id)->delete();

            foreach($request->incharge as $incharge){
                $pic = ProjectInCharge::create([
                    'project_id' => $proj->id,
                    'user_id' => $incharge
                ]);
            }

            $response['status'] = 'Success';
            $response['code'] = 200;

        return Response::json($response);
    }

    public function getPresets(Request $request) {
        $presets = Preset::orderBy('id')->get();
        $bundles = Bundle::orderBy('id')->get();

        $req = $request->all();

        $pt = ProjectTask::findOrFail($request->task_id); 
        $pj = Project::findOrFail($request->project_id); 
        $allmat = Material::all(); 
        $addmaterials = [];
        $add = [];


        if($pt->status == 'mob'){
            $or_ids = Order::where('task_id',$request->task_id)->pluck('id'); 
            $od = OrderDetails::whereIn('order_id',$or_ids)->where('order_status','mob')->where('preset_id','!=',0)->get(); 
            $ords = OrderDetails::whereIn('order_id',$or_ids)->where('order_status','mob')->where('preset_id','!=',0)->pluck('order_id'); 
            $or = Order::whereIn('id',$ords)->first(); 
            $preset_ids = $od->unique('preset_id')->pluck('preset_id');
            $preset_units = $od->unique('preset_id')->pluck('preset_unit');
            $preset_qtys = $od->unique('preset_id')->pluck('preset_qty');
            $notes = $od->unique('remarks')->pluck('remarks');
            //$note = OrderDetails::where('order_id',$or->id)->where('order_status','mob')->where('preset_id','!=',0)->first();
            // \Log::info($note);
            if(!$or){
                $ords = OrderDetails::whereIn('order_id',$or_ids)->where('order_status','mob')->pluck('order_id'); 
                $or = Order::whereIn('id',$ords)->first();
            }

            $req['order_id'] = $or->id;
            $req['name'] = $preset_ids;
            $req['unit'] = $preset_units;
            $req['note'] = $notes;
            // $req['qty'] = $preset_qtys;
            $req['status'] = 'mob';

            $pre = Preset::whereIn('id',$preset_ids)->get();
            foreach($pre as $p){
                $oo = OrderDetails::where('order_id',$or->id)->where('order_status','mob')->where('preset_id',$p->id)->first(); 
                $req['qty'][$p->id] = 0;
                if($oo){
                    $req['qty'][$p->id] = $oo->preset_qty;
                }
                $pm = PresetMaterial::where('preset_id', $p->id)->get();
                $m = $pm->pluck('material_id');

                $materials = Material::whereIn('id',$m)->get();
                foreach($materials as $mat){
                    $mat['ratio'] = PresetMaterial::where('preset_id', $p->id)->where('material_id',$mat->id)->value('ratio');
                    $ordet = OrderDetails::where('preset_id', $p->id)->where('product_id',$mat->id)->where('order_status','mob')->where('order_id', $or->id);
                    $mat['qty'] = $ordet->value('qty');
                    $mat['subtotal'] = $ordet->value('total_price');
                    $mat['final_price'] = $ordet->value('unit_price');

                    $weight = $ordet->value('weight') > 0 ? $ordet->value('weight') : $mat->weight;
                    $length = $ordet->value('length') > 0 ? $ordet->value('length') : $mat->length;
                    $width = $ordet->value('width') > 0 ? $ordet->value('width') : $mat->width;
                    $height = $ordet->value('height') > 0 ? $ordet->value('height') : $mat->height;
                    $mat['weight'] = $weight;
                    $mat['length'] = $length;
                    $mat['width'] = $width;
                    $mat['height'] = $height;
                }
                $p['materials'] = $materials;
                $p['note'] = $notes;
            }
            $req['presets'] = $pre;
            $req['remarks'] = $or->remarks;
            $req['needed_date'] = null;
            if($or->needed_date != null && $or->needed_date != '0000-00-00'){
                $req['needed_date'] = $or->needed_date;
            }
            $req['needed_time'] = [];
            if($or->needed_time_from  != null && $or->needed_time_from != '00:00:00'){            
                $req['needed_time'][] = $or->needed_time_from;
                $req['needed_time'][] = $or->needed_time_to;
            }

            //additional materials
            $add = OrderDetails::where('order_id',$or->id)->where('order_status','mob')->where('preset_id',0)->pluck('product_id');
            //$add2 = OrderDetails::where('order_id',$or->id)->where('preset_id',0)->pluck('product_id');
            //\Log::info($or->id);
            $addmaterials = Material::whereIn('id',$add)->get();
            foreach($addmaterials as $addmat){
                    $addmat['ratio'] = 0;
                    $ordet2 = OrderDetails::where('preset_id', 0)->where('product_id',$addmat->id)->where('order_id', $or->id);
                    $addmat['qty'] = $ordet2->value('qty');
                    $addmat['subtotal'] = $ordet2->value('total_price');
                    $addmat['retail_price'] = $ordet2->value('unit_price');
                    $addmat['note'] = $ordet2->value('remarks');

                    $weight = $ordet2->value('weight') > 0 ? $ordet2->value('weight') : $addmat->weight;
                    $length = $ordet2->value('length') > 0 ? $ordet2->value('length') : $addmat->length;
                    $width = $ordet2->value('width') > 0 ? $ordet2->value('width') : $addmat->width;
                    $height = $ordet2->value('height') > 0 ? $ordet2->value('height') : $addmat->height;
                    $addmat['weight'] = $weight;
                    $addmat['length'] = $length;
                    $addmat['width'] = $width;
                    $addmat['height'] = $height;
                }
                //$p['addmaterials'] = $addmaterials;
        }
        //\Log::info($req);


        $response['status'] = 'Success';
        $response['data'] = [
            'presets' => $presets,
            'bundles' => $bundles,
            'materials' => $allmat,
            'addmaterials' => $addmaterials,
            'formaddmaterials' => $add,
            'form' => $req,
            'task' => $pt,
            'project' => $pj
        ];
        $response['code'] = 200;

        return Response::json($response);
    }

    public function getPresets2(Request $request) {
        $presets = Preset::orderBy('id')->get();
        $bundles = Bundle::orderBy('id')->get();

        $req = $request->all();

        $pt = ProjectTask::findOrFail($request->task_id); 
        $pj = Project::findOrFail($request->project_id); 
        $allmat = Material::all(); 
        $addmaterials = [];
        $add = [];


        if($pt->status == 'site order'){
            $or = Order::where('id',$request->order_id)->first(); 
            $od = OrderDetails::where('order_id',$or->id)->where('order_status','site order')->where('preset_id','!=',0)->get(); 
            $preset_ids = $od->unique('preset_id')->pluck('preset_id');
            $preset_units = $od->unique('preset_id')->pluck('preset_unit');
            $preset_qtys = $od->unique('preset_id')->pluck('preset_qty');
            $notes = $od->unique('remarks')->pluck('remarks');
            //$note = OrderDetails::where('order_id',$or->id)->where('order_status','mob')->where('preset_id','!=',0)->first();
            // \Log::info($note);

            $req['order_id'] = $or->id;
            $req['name'] = $preset_ids;
            $req['unit'] = $preset_units;
            $req['note'] = $notes;
            // $req['qty'] = $preset_qtys;
            $req['status'] = 'site order';
            $pre = Preset::whereIn('id',$preset_ids)->get();
            foreach($pre as $p){
                $oo = OrderDetails::where('order_id',$or->id)->where('order_status','site order')->where('preset_id',$p->id)->first(); 
                $req['qty'][$p->id] = 0;
                if($oo){
                    $req['qty'][$p->id] = $oo->preset_qty;
                }
                $pm = PresetMaterial::where('preset_id', $p->id)->get();
                $m = $pm->pluck('material_id');

                $materials = Material::whereIn('id',$m)->get();
                foreach($materials as $mat){
                    $mat['ratio'] = PresetMaterial::where('preset_id', $p->id)->where('material_id',$mat->id)->value('ratio');
                    $ordet = OrderDetails::where('preset_id', $p->id)->where('product_id',$mat->id)->where('order_status','site order')->where('order_id', $or->id);
                    $mat['qty'] = $ordet->value('qty');
                    $mat['subtotal'] = $ordet->value('total_price');
                    $mat['final_price'] = $ordet->value('unit_price');

                    $weight = $ordet->value('weight') > 0 ? $ordet->value('weight') : $mat->weight;
                    $length = $ordet->value('length') > 0 ? $ordet->value('length') : $mat->length;
                    $width = $ordet->value('width') > 0 ? $ordet->value('width') : $mat->width;
                    $height = $ordet->value('height') > 0 ? $ordet->value('height') : $mat->height;
                    $mat['weight'] = $weight;
                    $mat['length'] = $length;
                    $mat['width'] = $width;
                    $mat['height'] = $height;
                }
                $p['materials'] = $materials;
                $p['note'] = $notes;
            }
            $req['presets'] = $pre;
            $req['remarks'] = $or->remarks;
            $req['needed_date'] = null;
            if($or->needed_date != null && $or->needed_date != '0000-00-00'){
                $req['needed_date'] = $or->needed_date;
            }
            $req['needed_time'] = [];
            if($or->needed_time_from  != null && $or->needed_time_from != '00:00:00'){            
                $req['needed_time'][] = $or->needed_time_from;
                $req['needed_time'][] = $or->needed_time_to;
            }

            //additional materials
            $add = OrderDetails::where('order_id',$or->id)->where('order_status','site order')->where('preset_id',0)->pluck('product_id');
            //$add2 = OrderDetails::where('order_id',$or->id)->where('preset_id',0)->pluck('product_id');
            //\Log::info($or->id);
            $addmaterials = Material::whereIn('id',$add)->get();
            foreach($addmaterials as $addmat){
                    $addmat['ratio'] = 0;
                    $ordet2 = OrderDetails::where('preset_id', 0)->where('order_status', 'site order')->where('product_id',$addmat->id)->where('order_id', $or->id);
                    $addmat['qty'] = $ordet2->value('qty');
                    $addmat['subtotal'] = $ordet2->value('total_price');
                    $addmat['retail_price'] = $ordet2->value('unit_price');
                    $addmat['note'] = $ordet2->value('remarks');

                    $weight = $ordet2->value('weight') > 0 ? $ordet2->value('weight') : $addmat->weight;
                    $length = $ordet2->value('length') > 0 ? $ordet2->value('length') : $addmat->length;
                    $width = $ordet2->value('width') > 0 ? $ordet2->value('width') : $addmat->width;
                    $height = $ordet2->value('height') > 0 ? $ordet2->value('height') : $addmat->height;
                    $addmat['weight'] = $weight;
                    $addmat['length'] = $length;
                    $addmat['width'] = $width;
                    $addmat['height'] = $height;
                }
                //$p['addmaterials'] = $addmaterials;
        }
        //\Log::info($req);


        $response['status'] = 'Success';
        $response['data'] = [
            'presets' => $presets,
            'bundles' => $bundles,
            'materials' => $allmat,
            'addmaterials' => $addmaterials,
            'formaddmaterials' => $add,
            'form' => $req,
            'task' => $pt,
            'project' => $pj
        ];
        $response['code'] = 200;

        return Response::json($response);
    }

    public function getSelectedPresets(Request $request) { //jeff
        $temp = $request->tempmaterials;
        $selected = $request->materials;
        $old = $request->oldmaterials;

        if(count($selected) > count($temp)){
            $new = array_diff($selected,$temp);
        }else{
            $new = $selected;
        }

        $presets = Preset::whereIn('id',$new)->get();

            $units = [];
            $qty = [];
            $prem = [];
            $ctr = 0;
            $matCtr = 1;
            foreach($presets as $p){
                $list = '';
                $pm = PresetMaterial::where('preset_id', $p->id)->orderBy('section')->get();
                $m = '';
                $m = $pm->pluck('material_id');
                if($m != ''){
                    $list = "'".implode("', '", $m->toArray())."'";
                }

                $materials = Material::whereIn('id',$m)
                                ->when($m != '', function ($q) use($list) {
                                        return $q->orderByRaw(DB::raw("FIELD(id, $list)"));
                                })
                                ->get();
                // foreach($pm as $premat){
                //     if($premat->reminder != '' && $premat->reminder != null){

                //     }
                // }
                $sec = 0;                
                $rem = '';
                $lastid = 0;                
                foreach($materials as $mat){
                    $prst = PresetMaterial::where('preset_id', $p->id)->where('material_id', $mat->id)->first();
                    if($sec != $prst->section && $prst->reminder != '' && $prst->reminder != null){
                        $lastid = PresetMaterial::where('preset_id', $p->id)->where('section', $prst->section)
                                    ->orderBy('id','DESC')->first()->id;
                        $sec = $prst->section;
                        $rem = $prst->reminder;
                    }
                    // \Log::info($lastid);
                    if($prst->id == $lastid){
                        $mat['reminder'] = $rem;
                    }
                    else{
                        $mat['reminder'] = '';
                    }
                    $od = OrderDetails::where('order_id', $request->order_id)->where('preset_id', $p->id)
                            ->where('product_id',$mat->id)
                            ->where(function ($query) {
                                $query->orwhere('order_status','mob')
                                      ->orwhere('order_status','site order');
                                })
                            // ->where('order_status','mob')
                            ->first();
                    $units[$ctr] = ($od ? $od->preset_unit : '');
                    $qty[$ctr] = ($od ? $od->preset_qty : 0);
                    $mat['ratio'] = PresetMaterial::where('preset_id', $p->id)->where('material_id',$mat->id)->value('ratio');
                    $mat['qty'] = ($od ? $od->qty : 0);
                    // $mat['unit'] = ($od ? $od->preset_unit : 0);
                    $mat['subtotal'] = ($od ? $od->total_price : 0);
                    $mat['final_price'] = ($od ? $od->unit_price : ($mat->unit_price> 0 ? $mat->unit_price : 1));
                    $mat['files'] = MaterialImages::where('material_id', $mat->id)->get();
                    $mat['matCtr'] = $matCtr;
                    $mat['setion'] = $prst->section;
                    // $mat['reminder'] = $prst->reminder;
                    $mat['unit'] = $prst->unit;
                    $matCtr++;
                }
                $p['materials'] = $materials;
                $ctr++;
            }

        if(count($selected) > count($temp)){
             $presets = array_merge($old, $presets->toArray());
        }else{
            $newold = [];
            foreach($old as $o){
                foreach($presets as $m){
                    if($o['id'] == $m->id){
                        $newold[] = $o;
                    }
                }
            }
             $presets = $newold;
        }

        $units = [];
        $qty = [];
        $ctr = 0;
        foreach($presets as $p){
            foreach($materials as $mat){
                $od = OrderDetails::where('order_id', $request->order_id)->where('preset_id', $p['id'])
                            ->where('product_id',$mat->id)
                            ->where(function ($query) {
                                $query->orwhere('order_status','mob')
                                      ->orwhere('order_status','site order');
                                })
                            ->first();
                $units[$ctr] = ($od ? $od->preset_unit : '');
                $qty[$ctr] = ($od ? $od->preset_qty : 0);
            }
        }
        

        $response['status'] = 'Success';
        $response['data'] = [
            'presets' => $presets,
            'units' => $units,
            'qty' => $qty
        ];
        $response['code'] = 200;

        return Response::json($response);
    }

    public function clearMob(Request $request){
        $validator = Validator::make($request->all(), [
            'project_id' => 'required',
            'task_id' => 'required',
            // 'name' => 'required|array',
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;
        } else {
            

            $orders = Order::where('project_id',$request->project_id)->where('task_id',$request->task_id)->pluck('id');
            $res2=OrderDetails::whereIn('order_id', $orders)->where('order_status', 'mob')->delete();
            $order_id = Order::where('project_id',$request->project_id)->where('task_id',$request->task_id)->first();
            $status = $this->updateProjectTaskStatus($order_id->id);
            //

            $nick = Project::where('id',$request->project_id)->value('nick_name');

            $response['status'] = 'Success';
            $response['project_nickname'] = $nick;
            $response['code'] = 200;
            return Response::json($response);
        }
    }

    public static function updateProjectTaskStatus($id){
        $status = null; // empty

        $order = Order::findorfail($id);
        $order_ids = Order::where('project_id', $order->project_id)->where('task_id', $order->task_id)->pluck('id');

        $mob = DB::table('order_details')
            ->select('*')
            ->whereIn('order_id', $order_ids)
            ->where('order_status', 'mob')
            ->count();

        $siteOrders = DB::table('order_details')
            ->select('*')
            ->whereIn('order_id', $order_ids)
            ->where('order_status', 'site order')
            ->count();

        $toPrep = DB::table('order_details')
            ->select('*')
            ->whereIn('order_id', $order_ids)
            ->where('order_status', 'to prep')
            ->count();

        $toShip = DB::table('order_details')
            ->select('*')
            ->whereIn('order_id', $order_ids)
            ->where('order_status', 'to ship')
            ->count();

        $toRcv = DB::table('order_details')
            ->select('*')
            ->whereIn('order_id', $order_ids)
            ->where('order_status', 'to received')
            ->count();

        $completed = DB::table('order_details')
            ->select('*')
            ->whereIn('order_id', $order_ids)
            ->where('order_status', 'completed')
            ->count();

        if($completed > 0){
            $status = "completed";
        }
        if($toRcv > 0){
            $status = "to received";
        }
        if($toShip > 0){
            $status = "to ship";
        }
        if($toPrep > 0){
            $status = "to prep";
        }
        if($siteOrders > 0){
            $status = "site order";
        }
        if($mob > 0){
            $status = "mob";
        }

        // if($completed == 0 && $toRcv == 0 && $toShip ==0 && $toPrep ==0 && $siteOrders ==0){
        //     $status = 'purchase order';
        // }

        $data = array('status' => $status);

        DB::table('project_tasks')
            ->where('project_id', $order->project_id)
            ->where('id', $order->task_id)
            ->update($data);
    }

    public function addPurchaseOrder(Request $request){
        $validator = Validator::make($request->all(), [
            'project_id' => 'required',
            'task_id' => 'required',
            // 'name' => 'required|array',
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;
        } else {
            // \Log::info($request->order_id);
            $order_ids = Order::where('task_id', $request->task_id)->where('project_id', $request->project_id)->pluck('id');
            $checkOrderDetails = OrderDetails::whereIn('order_id', $order_ids)->where('order_status','mob')->pluck('order_id');
            $checkOrder = Order::whereIn('id', $checkOrderDetails)->first();

            if($request->order_id == null && !$checkOrder){
                $order = new Order;
                $order->user_id = Auth::user()->id;
                $order->project_id = $request->project_id;
                $order->task_id = $request->task_id;
                $order->remarks = $request->remarks;
                $order->needed_date = $request->needed_date;
                $order->needed_time_from = $request->needed_time[0];
                $order->needed_time_to = $request->needed_time[1];
                $order->save();
            }
            else{
                $order = Order::findOrFail($checkOrder->id);
                $order->remarks = $request->remarks;
                if($request->needed_date != null){  
                    $order->needed_date = $request->needed_date;
                }
                if($request->needed_time){                
                    $order->needed_time_from = $request->needed_time[0];
                    $order->needed_time_to = $request->needed_time[1];
                }
                $order->created_at =  date("Y-m-d H:i:s");
                $order->save();
                $res =OrderDetails::where('order_id',$order->id)->where('order_status','mob')->delete();
            }


                $ctr = 0;
                foreach($request->name as $pre){
                    foreach($request->materials[$ctr] as $m) {
                        if($m['qty'] > 0){                            
                            $mmm = Material::findOrFail($m['id']);
                            $orderdet = new OrderDetails;
                            $orderdet->order_id = $order->id;
                            $orderdet->preset_id = $pre;
                            $orderdet->product_id = $m['id'];
                            $orderdet->qty = $m['qty'];
                            $orderdet->requested_by = $request->requested_by;
                            $orderdet->requested_at = $request->requested_at;
                            $orderdet->preset_unit = $request->unit[$ctr];
                            $orderdet->preset_qty = $request->qty[$pre];
                            $orderdet->remarks = isset($request->note[$ctr]) ? $request->note[$ctr]  : null;
                            $orderdet->order_status = $request->status;
                            $orderdet->weight = $m['weight'];
                            $orderdet->length = $m['length'];
                            $orderdet->width = $m['width'];
                            $orderdet->height = $m['height'];
                            $orderdet->unit_price = $m['final_price'];
                            $orderdet->total_price = $m['final_price'] * $m['qty'];
                            $orderdet->supplier_id = ($mmm->supplier_id != null ? $mmm->supplier_id : 0 );
                            $orderdet->save();
                        }
                    }
                    $ctr++;
                }

                $ctr = 0;
                foreach($request->addmaterials as $m) {
                    $mmm = Material::findOrFail($m['id']);
                    $orderdet = new OrderDetails;
                    $orderdet->order_id = $order->id;
                    $orderdet->preset_id = 0;
                    $orderdet->product_id = $m['id'];
                    $orderdet->qty = $m['qty'];
                    $orderdet->requested_by = $request->requested_by;
                    $orderdet->requested_at = $request->requested_at;
                    $orderdet->preset_unit = null;
                    $orderdet->preset_qty = null;
                    $orderdet->remarks = isset($m['note']) ? $m['note']  : null;
                    $orderdet->order_status = $request->status;
                    $orderdet->weight = $m['weight'];
                    $orderdet->length = $m['length'];
                    $orderdet->width = $m['width'];
                    $orderdet->height = $m['height'];
                    $orderdet->unit_price = $m['retail_price'];
                    $orderdet->total_price = $m['retail_price'] * $m['qty'];
                    $orderdet->supplier_id = ($mmm->supplier_id != null ? $mmm->supplier_id : 0 );
                    $orderdet->save();
                }


                $pt = ProjectTask::findOrFail($request->task_id);
                $pt->status = $request->status;
                $pt->save();

                if($request->status == 'site order'){
                     $pr = Project::findOrFail($request->project_id);
                     if($pr->mob_first_date == null){                    
                         $pr->mob_first_date = date("Y-m-d");
                         $pr->save();
                     }
                }


            $nick = Project::where('id',$request->project_id)->value('nick_name');

            $response['status'] = 'Success';
            $response['project_nickname'] = $nick;
            $response['code'] = 200;
            return Response::json($response);
        }
    }


    public function addPurchaseOrder2(Request $request){
        $validator = Validator::make($request->all(), [
            'project_id' => 'required',
            'task_id' => 'required',
            // 'name' => 'required|array',
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;
        } else {
            // \Log::info($request->order_id);
            if($request->order_id == null){
                $order = new Order;
                $order->user_id = Auth::user()->id;
                $order->project_id = $request->project_id;
                $order->task_id = $request->task_id;
                $order->remarks = $request->remarks;
                $order->needed_date = $request->needed_date;
                $order->needed_time_from = $request->needed_time[0];
                $order->needed_time_to = $request->needed_time[1];
                $order->save();
            }
            else{
                $order = Order::findOrFail($request->order_id);
                $order->remarks = $request->remarks;
                if($request->needed_date != null && $request->needed_date != $order->needed_date){  
                    $order->needed_date = $request->needed_date;
                }
                if($request->needed_time){                
                    $order->needed_time_from = $request->needed_time[0];
                    $order->needed_time_to = $request->needed_time[1];
                }
                //$order->created_at =  date("Y-m-d H:i:s");
                $order->save();
                $res =OrderDetails::where('order_id',$order->id)->where('order_status','site order')->delete();
            }


                $ctr = 0;
                foreach($request->name as $pre){
                    foreach($request->materials[$ctr] as $m) {
                        //\Log::info($ctr);
                        //\Log::info($request->note);
                        // $m['final_price'] = ($m['final_price'] ? $m['final_price'] : $m['unit_price']);
                        if($m['qty'] > 0){                            
                            $mmm = Material::findOrFail($m['id']);
                            $orderdet = new OrderDetails;
                            $orderdet->order_id = $order->id;
                            $orderdet->preset_id = $pre;
                            $orderdet->product_id = $m['id'];
                            $orderdet->qty = $m['qty'];
                            $orderdet->requested_by = $request->requested_by;
                            $orderdet->requested_at = $request->requested_at;
                            $orderdet->preset_unit = $request->unit[$ctr];
                            $orderdet->preset_qty = $request->qty[$pre];
                            $orderdet->remarks = isset($request->note[$ctr]) ? $request->note[$ctr]  : null;
                            $orderdet->order_status = $request->status;
                            $orderdet->weight = $m['weight'];
                            $orderdet->length = $m['length'];
                            $orderdet->width = $m['width'];
                            $orderdet->height = $m['height'];
                            $orderdet->unit_price = $m['final_price'];
                            $orderdet->total_price = $m['final_price'] * $m['qty'];
                            $orderdet->supplier_id = ($mmm->supplier_id != null ? $mmm->supplier_id : 0 );
                            $orderdet->save();
                        }
                    }
                    $ctr++;
                }

                $ctr = 0;
                foreach($request->addmaterials as $m) {
                    $mmm = Material::findOrFail($m['id']);
                    $orderdet = new OrderDetails;
                    $orderdet->order_id = $order->id;
                    $orderdet->preset_id = 0;
                    $orderdet->product_id = $m['id'];
                    $orderdet->qty = $m['qty'];
                    $orderdet->requested_by = $request->requested_by;
                    $orderdet->requested_at = $request->requested_at;
                    $orderdet->preset_unit = null;
                    $orderdet->preset_qty = null;
                    $orderdet->remarks = isset($m['note']) ? $m['note']  : null;
                    $orderdet->order_status = $request->status;
                    $orderdet->weight = $m['weight'];
                    $orderdet->length = $m['length'];
                    $orderdet->width = $m['width'];
                    $orderdet->height = $m['height'];
                    $orderdet->unit_price = $m['retail_price'];
                    $orderdet->total_price = $m['retail_price'] * $m['qty'];
                    $orderdet->supplier_id = ($mmm->supplier_id != null ? $mmm->supplier_id : 0 );
                    $orderdet->save();
                }


                $pt = ProjectTask::findOrFail($request->task_id);
                $pt->status = $request->status;
                $pt->save();

                if($request->status == 'site order'){
                     $pr = Project::findOrFail($request->project_id);
                     if($pr->mob_first_date == null){                    
                         $pr->mob_first_date = date("Y-m-d");
                         $pr->save();
                     }
                }


            $nick = Project::where('id',$request->project_id)->value('nick_name');

            $response['status'] = 'Success';
            $response['project_nickname'] = $nick;
            $response['code'] = 200;
            return Response::json($response);
        }
    }

    public function requestMaterials(Request $request){
                $order = new Order;
                $order->user_id = Auth::user()->id;
                $order->project_id = 0;
                $order->task_id = 0;
                $order->remarks = null;
                $order->save();

                foreach($request->materials as $m) {
                    if($m['qty'] > 0){                    
                        $orderdet = new OrderDetails;
                        $orderdet->order_id = $order->id;
                        $orderdet->preset_id = 0;
                        $orderdet->product_id = $m['id'];
                        $orderdet->qty = $m['qty'];
                        $orderdet->preset_unit = null;
                        $orderdet->preset_qty = null;
                        $orderdet->order_status = 'request';
                        $orderdet->unit_price = $m['unit_price'];
                        $orderdet->total_price = $m['unit_price'] * $m['qty'];
                        $orderdet->save();
                    }
                }

            $response['status'] = 'Success';
            $response['code'] = 200;
            return Response::json($response);
    }

    public function requestLowQty(Request $request) {
            $reqOrders = Order::where('project_id',0)->where('task_id',0)->pluck('id');

            $inv = Inventory::where('status',1)->where('qty','<',10)->get();

            $lowinv = $inv->pluck('material_id');
            $od = OrderDetails::whereIn('order_id',$reqOrders)->whereIn('product_id',$lowinv)->get();

            $lowqty = [];
            $ids = [];

            foreach ($inv as $k=>$v){       
                foreach($od as $a=>$b){
                    if($b->product_id == $v->material_id){
                        $v->qty += $b->qty;
                    }

                }
                if($v->qty < 10){
                    $low = Material::findorfail($v->material_id);
                    $low->qty = 10 - $v->qty;
                    $lowqty[] = $low;
                    $ids[] = $v->material_id;
                }
            }

            $response['status'] = 'Success';
            $response['data'] = $lowqty;
            $response['ids'] = $ids;
            $response['code'] = 200;
        return Response::json($response);
    }

    public function deleteTask(Request $request){
        $id = $request->id;
        $res=ProjectTask::where('id',$id)->delete();
        $res2=ProjectTask::where('parent_id',$id)->delete();
        // $orders=Order::where('task_id',$id)->pluck('id');
        // $res3=Order::where('task_id',$id)->delete();
        // $res4=OrderDetails::whereIn('order_id',$orders)->delete();
        $response['status'] = 'Success';
        $response['code'] = 200;
        return Response::json($response);
    }

    public function deleteComment(Request $request){
        $id = $request->id;
        $res=ProjectTaskComments::where('id',$id)->delete();
        //$res2=ProjectTask::where('parent_id',$id)->delete();
        $response['status'] = 'Success';
        $response['code'] = 200;
        return Response::json($response);
    }

    public function getProjectDetails(Request $request) {
        $id = $request->project_id;
        $details = Project::findorfail($id);
        $q = QuotationNotes::orderBy('step')->get();

        //$ptasks = ProjectTask::where('project_id', $id)->where('parent_id',0)->get();

        $orders = Order::where('project_id', $id)->pluck('id');

        $od = OrderDetails::whereIn('order_id', $orders)->groupBy('preset_id')->get();

        $pt = ProjectTask::where('project_id', $id)->get();

        $scope = [];
        $additionals = [];
        $ctr = 0;
        $ctr2 = 0;
        $subtotal = 0;

        foreach($pt as $o){
            // if($o->preset_id > 0){
                //$preset = Preset::findOrFail($o->preset_id);
                $checkOr = Order::where('task_id', $o->id)->first();
                if($checkOr){
                $scope[$ctr]['scope_name'] = $o->label;
                // if($preset->name_cn != null || $preset->name_cn != ''){
                //     $scope[$ctr]['scope_name'] = $preset->name . '('.$preset->name_cn.')';
                // }
                    $or = Order::where('task_id', $o->id)->pluck('id');
                    $pm = OrderDetails::whereIn('order_id', $or)->get();
                    $materials = [];
                    foreach($pm as $p){
                        $mat = Material::findOrFail($p->product_id);
                        $od = OrderDetails::where('order_id', $p->order_id)
                                ->where('product_id', $mat->id)->get();
                        $mat->name_cn = $mat->name_cn == null ? ' ' : $mat->name_cn;
                        $mat->qty = $od->sum('qty');
                        $mat->unit = $mat->unit;
                        $mat->srp = $mat->retail_price;
                        $mat->sum = $mat->retail_price * $od->sum('qty');
                        $subtotal += $mat->sum;
                        $materials[] = $mat;
                    }
                    $scope[$ctr]['materials'] = $materials;

                    $ctr++;
                }
            // }
            // else if($o->preset_id == 0 && count($additionals)< 1){
            //     $additionals[$ctr2]['scope_name'] = 'Additional Materials';
            //     $additionals[$ctr2]['scope_name_cn'] = ' ';
            //     //$mat = Material::findOrFail($o->product_id);
            //     $od = OrderDetails::whereIn('order_id', $orders)->where('preset_id',0)->groupBy('product_id')->get();
            //     $addmaterials = [];
            //     foreach($od as $p){
            //         $mat = Material::findOrFail($p->product_id);
            //         $od = OrderDetails::whereIn('order_id', $orders)->where('preset_id',0)
            //                 ->where('product_id', $mat->id)->get();
            //         $mat->qty = $od->sum('qty');
            //         $mat->srp = $od->max('unit_price');
            //         $mat->sum = $od->max('unit_price') * $od->sum('qty');
            //         $subtotal += $mat->sum;
            //         $addmaterials[] = $mat;
            //     }
            //     $additionals[$ctr2]['materials'] = $addmaterials;
            //     $ctr2++;
            // }
        }
        $response['status'] = 'Success';
        $response['details'] = $details;
        $response['notes'] = $q;
        $response['scope'] = $scope;
        $response['additionals'] = $additionals;
        $response['subtotal'] = $subtotal;
        $response['code'] = 200;
        return Response::json($response);
    }

    public function exportProjectQuotation(Request $request) 
    {
        $export = new ProjectQuotationExport($request->id);

        return Excel::download($export, 'quotation.xls');
    }

    public function allUsers() {
        $users = User::select('id','nick_name')->get();

        $response['status'] = 'Success';
        $response['data'] =  $users;
        $response['code'] = 200;

        return Response::json($response);
    }

    public function toggleTaskPriority(Request $request) {


            $pt = ProjectTask::findOrFail($request->id);
            $pt->prio = $pt->prio == 0 ? 1 : 0;
            $pt->last_mod = Auth::user()->first_name;
            $pt->save();
                

            $response['status'] = 'Success';
            $response['code'] = 200;
        

        return Response::json($response);
    }

    public function markTaskComplete(Request $request) {
            $pt = ProjectTask::findOrFail($request->id);
            $pt->progress = 100;
            $pt->last_mod = Auth::user()->first_name;
            $pt->save();      

            $response['status'] = 'Success';
            $response['code'] = 200;

        return Response::json($response);
    }

    public function markTaskIncomplete(Request $request) {
            $pt = ProjectTask::findOrFail($request->id);
            $pt->progress = 0;
            $pt->last_mod = Auth::user()->first_name;
            $pt->save();      

            $response['status'] = 'Success';
            $response['code'] = 200;

        return Response::json($response);
    }

    public function deleteTaskImage(Request $request) {
            $res=ProjectTaskImages::where('id',$request->id)->delete();      

            $response['status'] = 'Success';
            $response['code'] = 200;

        return Response::json($response);
    }

    public function deleteMaterialImage(Request $request) {
            $res=MaterialImages::where('id',$request->id)->delete();      

            $response['status'] = 'Success';
            $response['code'] = 200;

        return Response::json($response);
    }

    public function markAsRead(Request $request) {
            // $res=ProjectTask::where('id',$request->id)->first();   
            // if($res){
            //     $res->is_read = 1;
            //     $res->save();

            //     $res2=ProjectTaskComments::where('proj_task_id', $res->id)->update(['is_read' => 1]);
            // }   

            $comments = ProjectTaskComments::where('proj_task_id', $request->id)->get();

            foreach($comments as $c){
                $is_read = ProjectReadComments::where('user_id',Auth::user()->id)
                                ->where('comment_id',$c->id)->first();
                if(!$is_read){                
                    $read = new ProjectReadComments;
                    $read->comment_id = $c->id;
                    $read->user_id = Auth::user()->id;
                    $read->is_read = 1;
                    $read->save();
                }
            }

            $response['status'] = 'Success';
            $response['code'] = 200;

        return Response::json($response);
    }

}
