<?php

namespace App\Http\Controllers;
use App\Remark;
use Carbon\Carbon;

use App\Project;
use App\Preset;
use App\Supplier;
use App\Material;
use App\PresetMaterial;
use App\Order;
use App\Biller;
use App\Cars;
use App\OrderDetails;

use App\User;

use Illuminate\Support\Facades\URL;

use Auth, DB, Response, Validator;
//Excel
use Illuminate\Http\Request;

use Maatwebsite\Excel\Facades\Excel;

use Status;
use PDF;
use DateTime;

use App\Helpers\MessageHelper;

class SupplierController extends Controller
{

	public function manageSuppliers() {
		$groups = DB::table('groups as g')
			->select(DB::raw('g.id, g.name, CONCAT(u.first_name, " ", u.last_name) as leader, g.balance, g.collectables, p.latest_package as latest_package, srv.latest_service as latest_service'))
            ->leftjoin(DB::raw('(select * from users) as u'),'u.id','=','g.leader_id')
            ->leftjoin(DB::raw('
                    (
                        Select date_format(max(x.dates),"%M %e, %Y, %l:%i %p") as latest_package, x.group_id
                        from( SELECT STR_TO_DATE(created_at, "%Y-%m-%d %H:%i:%s") as dates,
                            group_id, status
                            FROM packages
                            ORDER BY dates desc
                        ) as x
                        group by x.group_id) as p'),
                    'p.group_id', '=', 'g.id')
            ->leftjoin(DB::raw('
                    (
                        Select date_format(max(cs.servdates),"%M %e, %Y") as latest_service,cs.client_id,cs.group_id
                        from( SELECT STR_TO_DATE(created_at, "%Y-%m-%d") as servdates,
                            group_id, active,client_id
                            FROM client_services
                            ORDER BY servdates desc
                        ) as cs
                        where cs.active = 1
                        group by cs.group_id) as srv'),
                    'srv.group_id', '=', 'g.id')
            ->orderBy('g.id', 'desc')
            ->get();

		$response['status'] = 'Success';
		$response['data'] = [
		    'groups' => $groups
		];
		$response['code'] = 200;

		return Response::json($response);
	}


  public function manageSuppliersPaginate(Request $request, $perPage = 20) {
        $sort = $request->input('sort');
        $search = $request->input('search');
        $branch = $request->input('branch');

        $presets = DB::table('suppliers as p')
            ->select(DB::raw('p.*'))
            ->when($search != '', function ($q) use($search){
                return $q->where('p.id','LIKE', '%'.$search.'%')->orwhere('p.full_name','LIKE', '%'.$search.'%');
            })
            ->when($sort != '', function ($q) use($sort){
                $sort = explode('-' , $sort);
                return $q->orderBy($sort[0], $sort[1]);
            })
            ->paginate($request->input('perPage'));
            //->paginate(100);

        return Response::json($presets);
    }

    public function supplierSearch(Request $request){
        $keyword = $request->input('search');
        $users = '';

        $suppliers = DB::connection()
            ->table('suppliers as a')
            ->select(DB::raw('
                a.id,a.full_name,a.nick_name'))
                //->orwhere('a.id','LIKE', '%' . $keyword .'%')
                ->orwhere('name','LIKE', '%' . $keyword .'%')
                ->get();


        $json = [];
        foreach($suppliers as $p){
          $json[] = array(
              'id' => $p->id,
              'name' => $p->full_name." (".$p->nick_name.")",
          );
        }
        $response['status'] = 'Success';
        $response['data'] =  $json;
        $response['code'] = 200;

        return Response::json($response);
    }

    public function store(Request $request) {

            $supplier = Supplier::create([
                'full_name' => $request->full_name,
                'nick_name' => $request->nick_name,
                'gender' => $request->gender,
                'contact_person' => $request->contact_person,
                'contact_number' => $request->contact_number,
                'address' => $request->address,
                'email' => $request->email,
                'tin' => $request->tin,
                'notes' => $request->notes,
                'bank' => $request->bank,
                'bank_account_name' => $request->bank_account_name,
                'bank_account_number' => $request->bank_account_number,
            ]);

            $response['status'] = 'Success';
            $response['code'] = 200;

        return Response::json($response);
    }

    public function update(Request $request,$id){
        $supp = Supplier::findorfail($id);
        $supp->full_name = $request->full_name;
        $supp->nick_name = $request->nick_name;
        $supp->gender = $request->gender;
        $supp->contact_person = $request->contact_person;
        $supp->contact_number = $request->contact_number;
        $supp->address = $request->address;
        $supp->email = $request->email;
        $supp->tin = $request->tin;
        $supp->notes = $request->notes;
        $supp->bank = $request->bank;
        $supp->bank_account_name = $request->bank_account_name;
        $supp->bank_account_number = $request->bank_account_number;
        $supp->save();

        $response['status'] = 'Success';
        $response['code'] = 200;

        return Response::json($response);
    }

    public function getAllSuppliers(){
        $supplier = Supplier::get();
        $projects = Project::get();
        $materials = Material::get();
        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $supplier;
        $response['projects'] = $projects;
        $response['materials'] = $materials;

        return Response::json($response);
    }

    public function loadBillerData(Request $request, $perPage = 20) {
        $sort = $request->input('sort');
        $search = $request->input('search');
        $branch = $request->input('branch');

        $presets = DB::table('billers as p')
            ->select(DB::raw('p.*'))
            ->when($search != '', function ($q) use($search){
                return $q->where('p.id','LIKE', '%'.$search.'%')->orwhere('p.name','LIKE', '%'.$search.'%');
            })
            ->when($sort != '', function ($q) use($sort){
                $sort = explode('-' , $sort);
                return $q->orderBy($sort[0], $sort[1]);
            })
            ->paginate($request->input('perPage'));
            //->paginate(100);

        return Response::json($presets);
    }

    public function addBiller(Request $request) {

            $biller = Biller::create([
                'name' => $request->name
            ]);

            $response['status'] = 'Success';
            $response['code'] = 200;

        return Response::json($response);
    }

    public function editBiller(Request $request, $id) {

            $biller = Biller::findorfail($id);
            $biller->name = $request->name;
            $biller->save();

            $response['status'] = 'Success';
            $response['code'] = 200;

        return Response::json($response);
    }

    public function loadCarData(Request $request, $perPage = 20) {
        $sort = $request->input('sort');
        $search = $request->input('search');
        $branch = $request->input('branch');

        $presets = DB::table('cars as p')
            ->select(DB::raw('p.*'))
            ->when($search != '', function ($q) use($search){
                return $q->where('p.id','LIKE', '%'.$search.'%')->orwhere('p.name','LIKE', '%'.$search.'%');
            })
            ->when($sort != '', function ($q) use($sort){
                $sort = explode('-' , $sort);
                return $q->orderBy($sort[0], $sort[1]);
            })
            ->paginate($request->input('perPage'));
            //->paginate(100);

        return Response::json($presets);
    }

    public function addCar(Request $request) {

            $biller = Cars::create([
                'name' => $request->name,
                'weight_limit' => $request->weight_limit,

            ]);

            $response['status'] = 'Success';
            $response['code'] = 200;

        return Response::json($response);
    }

    public function editCar(Request $request, $id) {

            $biller = Cars::findorfail($id);
            $biller->name = $request->name;
            $biller->weight_limit = $request->weight_limit;
            $biller->save();

            $response['status'] = 'Success';
            $response['code'] = 200;

        return Response::json($response);
    }


}
