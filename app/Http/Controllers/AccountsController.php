<?php

namespace App\Http\Controllers;

use App\User;

use App\Attendance;
use App\Worker;
use App\ProjectWorker;

use App\Role;

use App\Branch;

use App\ContactNumber;

use App\RoleUser;

use DB, Auth, Response, Validator, Hash, DateTime;

use Illuminate\Http\Request;

class AccountsController extends Controller
{
	//get branch of current user
	public function getBranchAuth(){
		$branch = DB::table('branch_user')->where('user_id', Auth::User()->id)
                ->pluck('branch_id')[0];
        return $branch;
	}
    //get all Cpanel Users
    public function getCpanelUsers() {
	 	$role_id = Role::where(function ($query) {
                        $query->orwhere('name', 'master')->orwhere('name', 'cpanel-admin')->orwhere('name', 'employee');
                    })->pluck("id");
	 	// $both_branch = Branch::where('name', 'Both')->pluck("id")[0];
    	//$auth_branch =  $this->getBranchAuth();
	 	$users = User::select('id', 'email', 'first_name', 'last_name')
                    ->where('password','!=',null)
	 				->with(array('roles' => function($query){
	 					$query->select('roles.id', 'roles.label');
	 				}))->whereHas('roles', function ($query) use ($role_id) {
                		$query->whereIn('roles.id', $role_id);
	 	 	 		})
        //             ->whereHas('branches', function ($query) use ($auth_branch) {
        //         		$query->where('branches.id', '=', $auth_branch);
	 	 	 		// })
                    ->get();

	 	$response['status'] = 'Success';
	 	$response['data'] = $users;
        $response['code'] = 200;
	 	return Response::json($response);
	}

    //Save new cpanel user
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'first_name'                    => 'required',
            'middle_name'                   => 'nullable',
            'last_name'                     => 'required',
            'nick_name'                     => 'required',
            'birthday'                      => 'required|date',
            'gender'                        => 'required',
            'civil_status'                  => 'required',
            'position'                      => 'required',
            'hire_date'                     => 'required|date',
            'address'                       => 'required',
            'contact_numbers'               => 'required|array',
            'contact_numbers.*.number'      => 'nullable',
            'contact_numbers.*.is_primary'  => 'nullable',
            'contact_numbers.*.is_mobile'   => 'nullable',
            'email'                         => 'nullable|email|unique:users',
            // 'branch'                        => 'required',
            'password'                      => 'nullable|confirmed|min:6'
            ]);



        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;
        } else {
            //Check if password is set to default
            $is_default_password = $request->is_default_password;
            $password = "";

            if($is_default_password){
                $password = Hash::make('123admin');
            }else{
                $password = Hash::make($request->password);
            }    

            $user = new User;
            $user->first_name = $request->first_name;
            $user->middle_name = $request->middle_name;
            $user->last_name = $request->last_name;
            $user->nick_name = $request->nick_name;
            $user->birth_date = $request->birthday;
            $user->gender = $request->gender;
            $user->address = $request->address;
            $user->civil_status = $request->civil_status;
            $user->position = $request->position;
            $user->basic_rate = $request->basic_rate;
            $user->ot_rate = $request->ot_rate;
            $user->op_allowance = $request->op_allowance;
            $user->meal_allowance = $request->meal_allowance;
            $user->load_allowance = $request->load_allowance;
            $user->sss = $request->sss;
            $user->pagibig = $request->pagibig;
            $user->philhealth = $request->philhealth;
            $user->cash_advance = $request->cash_advance;
            $user->start_date = $request->hire_date;
            $user->end_date = $request->released_date;
            $user->email = $request->email;
            $user->password = $password;
            $user->save();

            foreach($request->contact_numbers as $contactNumber) {
                if(strlen($contactNumber['number']) !== 0 && $contactNumber['number'] !== null) {
                    ContactNumber::create([
                        'user_id' => $user->id,
                        'number' => $contactNumber['number'],
                        'is_primary' => $contactNumber['is_primary'],
                        'is_mobile' => $contactNumber['is_mobile']
                    ]);
                }
            }

            $user->roles()->detach();
            // $user->roles()->attach(4);
            $user->roles()->detach();
            foreach($request->roles as $role) {
                $user->roles()->attach($role);
            }

            //$user->givePermission($request->get('permissions'));

            //$user->branches()->detach();
            //$user->branches()->attach($request->branch);
            
            $response['status'] = 'Success';
            $response['code'] = 200;
        }

        return Response::json($response);
    }

    //get specific user
    public function show($id){
        $user = User::whereId($id)
                    ->with(array('roles' => function($query){
                        $query->select('roles.id');
                    }))->with('contactNumbers','roles')->first();

        if( $user ) {
            $response['status'] = 'Success';
            $response['data'] = [
                'user' => $user
            ];
            $response['code'] = 200;
        } else {
            $response['status'] = 'Failed';
            $response['errors'] = 'No query results.';
            $response['code'] = 404;
        }

        return Response::json($response);
    }

    //edit cpanel user
    public function update(Request $request, $id) {
        $user = User::find($request->id);
        $hashedPassword = $user->password;

        $validator = Validator::make($request->all(), [
            'first_name'                    => 'required',
            'middle_name'                   => 'nullable',
            'last_name'                     => 'required',
            'nick_name'                     => 'required',
            'birthday'                      => 'required|date',
            'gender'                        => 'required',
            'civil_status'                  => 'required',
            'address'                       => 'required',
            'contact_numbers'               => 'required|array',
            'contact_numbers.*.number'      => 'nullable',
            'contact_numbers.*.is_primary'  => 'nullable',
            'contact_numbers.*.is_mobile'   => 'nullable',
            'email'                         => 'nullable|email|unique:users,email,'.$request->id,
            //'password'                      => 'nullable|confirmed|min:6', //new password
            //'old_password'                  => 'required_with:password'

        ]);

        if($validator->fails()){
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;   
        } else {
            $ce_count = 0;

            foreach($request->contact_numbers as $key=>$contactNumber) {
                if(strlen($contactNumber['number']) !== 0 && $contactNumber['number'] !== null) {
                    if(strlen($contactNumber['number']) === 13) {
                        $number = substr($contactNumber['number'], 3);
                    } else if(strlen($contactNumber['number']) === 12) {
                        $number = substr($contactNumber['number'], 2);
                    } else {
                        $number = substr($contactNumber['number'], 1);
                    }
                    
                    $contact = ContactNumber::where('number','LIKE','%'.$number.'%')->get();
                    
                    if($contact) {
                        $num_duplicate = 0;
                        foreach($contact as $con) {
                            if(strval ($con['user_id']) === strval ($request->id)) {
                                $num_duplicate++;
                            }
                        }

                        if($num_duplicate === 0) {
                            $contact_error['contact_numbers.'.$key.'.number'] = ['The contact number has already been taken.'];
                            $ce_count++;
                        }
                        
                    }
                }
            }


            if($ce_count > 0){
                $response['status'] = 'Failed';
                $response['errors'] = $contact_error;
                $response['code'] = 422;
            }else{
                $pe_count = 0;

                if (($request->old_password != '') || (!is_null($request->old_password))) {
                    if(($request->password != '') || (!is_null($request->password))){
                        if (Hash::check($request->old_password, $hashedPassword)){
                            if($user){
                                $user->password = Hash::make($request->password);
                            }
                        }else{
                            $password_error['old_password'] = ['Old password is incorrect. '];
                            $pe_count = 1;
                        }   
                        
                    }else{
                        $password_error['password'] = ['New Password is required. '];
                        $pe_count = 1;
                    }

                }

                if($pe_count > 0){
                    $response['status'] = 'Failed';
                    $response['errors'] = $password_error;
                    $response['code'] = 422;
                }else{
                    if($user){
                        $user->first_name = $request->first_name;
                        $user->middle_name = $request->middle_name;
                        $user->last_name = $request->last_name;
                        $user->nick_name = $request->nick_name;
                        $user->birth_date = $request->birthday;
                        $user->gender = $request->gender;
                        $user->civil_status = $request->civil_status;
                        $user->address = $request->address;
                        $user->email = $request->email;
                        $user->position = $request->position;
                        $user->basic_rate = $request->basic_rate;
                        $user->ot_rate = $request->ot_rate;
                        $user->op_allowance = $request->op_allowance;
                        $user->meal_allowance = $request->meal_allowance;
                        $user->load_allowance = $request->load_allowance;
                        $user->sss = $request->sss;
                        $user->pagibig = $request->pagibig;
                        $user->philhealth = $request->philhealth;
                        $user->cash_advance = $request->cash_advance;
                        $user->wca = $request->wca;
                        $user->start_date = $request->hire_date;
                        $user->end_date = $request->released_date;

                        $user->save();
                        // $user->branches()->detach();
                        // $user->branches()->attach($request->branch);

                        $user->roles()->detach();
                        foreach($request->roles as $role) {
                            $user->roles()->attach($role);
                        }

                        //delete all contact numbers saved first before saving updates
                        $contact_numbers = ContactNumber::where('user_id', $request->id)->delete();
                        foreach($request->contact_numbers as $contactNumber) {
                            if(strlen($contactNumber['number']) !== 0 && $contactNumber['number'] !== null) {
                                ContactNumber::create([
                                    'user_id' => $user->id,
                                    'number' => $contactNumber['number'],
                                    'is_primary' => $contactNumber['is_primary'],
                                    'is_mobile' => $contactNumber['is_mobile']
                                ]);
                            }
                        }

                        $response['status'] = 'Success';
                        $response['code'] = 200;
                    } else {
                        $response['status'] = 'Failed';
                        $response['errors'] = 'No query results.';
                        $response['code'] = 404;
                    }
                }

            }

        }
        return Response::json($response);
    }

    public function destroy($id){
        $user = User::findOrFail($id);
        
        if( $user ) {
            $user->delete();
            $user->branches()->detach();
            $user->roles()->detach();
            $client->contactNumbers()->delete();
            $response['status'] = 'Success';
            $response['code'] = 200;
        } else {
            $response['status'] = 'Failed';
            $response['errors'] = 'No query results.';
            $response['code'] = 404;
        }

        return Response::json($response);
    }



    //get specific worker
    public function getWorker($id){
        $user = Worker::whereId($id)->first();
        $p = ProjectWorker::where('worker_id',$id)->orderBy('id','Desc')->first();

        if( $user ) {
            if($p){
                $user->project_id = $p->project_id;
            }
            $response['status'] = 'Success';
            $response['data'] = [
                'user' => $user
            ];
            $response['code'] = 200;
        } else {
            $response['status'] = 'Failed';
            $response['errors'] = 'No query results.';
            $response['code'] = 404;
        }

        return Response::json($response);
    }

    //edit cpanel user
    public function updateWorker(Request $request, $id) {
        $user = Worker::find($request->id);

        $validator = Validator::make($request->all(), [
            'first_name'                    => 'required',
            'middle_name'                   => 'nullable',
            'last_name'                     => 'required'
        ]);

        if($validator->fails()){
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;   
        } else {
            $user->first_name = $request->first_name;
            $user->middle_name = $request->middle_name;
            $user->last_name = $request->last_name;
            $user->position = $request->position;
            $user->basic_rate = $request->basic_rate;
            $user->ot_rate = $request->ot_rate;
            $user->op_allowance = $request->op_allowance;
            $user->meal_allowance = $request->meal_allowance;
            $user->load_allowance = $request->load_allowance;
            $user->sss = $request->sss;
            $user->pagibig = $request->pagibig;
            $user->philhealth = $request->philhealth;
            $user->cash_advance = $request->cash_advance;
            $user->wca = $request->wca;
            $user->start_date = $request->hire_date;
            $user->end_date = $request->released_date;
            $user->save();

            $pw = ProjectWorker::where('worker_id',$request->id)->delete();

            $newpw = new ProjectWorker;
            $newpw->project_id = $request->project_id;
            $newpw->worker_id = $request->id;
            $newpw->is_active = 1;
            $newpw->save();

            $response['status'] = 'Success';
            $response['code'] = 200;

        return Response::json($response);
        }
    }

        //edit cpanel user
    public function addNewWorker(Request $request) {
        //$user = Worker::find($request->id);

        $validator = Validator::make($request->all(), [
            'first_name'                    => 'required',
            'middle_name'                   => 'nullable',
            'last_name'                     => 'required'
        ]);

        if($validator->fails()){
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;   
        } else {
            $user = new Worker;
            $user->first_name = $request->first_name;
            $user->middle_name = $request->middle_name;
            $user->last_name = $request->last_name;
            $user->position = $request->position;
            $user->basic_rate = $request->basic_rate;
            $user->ot_rate = $request->ot_rate;
            $user->op_allowance = $request->op_allowance;
            $user->meal_allowance = $request->meal_allowance;
            $user->load_allowance = $request->load_allowance;
            $user->sss = $request->sss;
            $user->pagibig = $request->pagibig;
            $user->philhealth = $request->philhealth;
            $user->cash_advance = $request->cash_advance;
            $user->wca = $request->wca;
            $user->start_date = $request->hire_date;
            $user->end_date = $request->released_date;
            $user->save();

            if($request->project_id > 0){            
                $newpw = new ProjectWorker;
                $newpw->project_id = $request->project_id;
                $newpw->worker_id = $user->id;
                $newpw->is_active = 1;
                $newpw->save();
            }

            $response['status'] = 'Success';
            $response['code'] = 200;

        return Response::json($response);
        }
    }

    public function getAllWorkers(){
        //return 0;
        $workers = Worker::get();
        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['workers'] = $workers;

        return Response::json($response);
    }

     public function saveWorkerAttendance(Request $request) {

        $validator = Validator::make($request->all(), [
            'id'                    => 'required',
            'time'               => 'required|array',
        ]);

        if($validator->fails()){
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;   
        } else {
            $date = $request->date;
            $delete_status = 0;
            foreach($request->time as $t){  
                if( ($t['timein'][0] != null || $t['timein'][0] != '') && ($t['timein'][1] != null || $t['timein'][1] != '') ){    

                    $d1 = new DateTime($date.' '.$t['timein'][0]);
                    $d2 = new DateTime($date.' '.$t['timein'][1]);
                    $interval = $d1->diff($d2); 
                    //\Log::info($interval);   
                    //\Log::info($interval->i);
                    $min = sprintf("%02d", $interval->i);   
                    $hr = sprintf("%02d", $interval->h);  
                    $hr = $hr * 60; 

                    if($delete_status == 0){
                    $del = Attendance::where("day",$d1->format('d'))
                                ->where("month",$d1->format('m'))
                                ->where("year",$d1->format('Y'))
                                ->where("user_id", $request->id)
                                ->where("project_id", $request->project_id)
                                ->delete();

                        $delete_status = 1;
                    }
                                
                    $att = new Attendance;
                    $att->user_id = $request->id;
                    $att->project_id = $request->project_id;
                    $att->timein = $date.' '.$t['timein'][0];
                    $att->timeout = $date.' '.$t['timein'][1];
                    $att->duration = $hr + $min;
                    $att->day = $d1->format('d');
                    $att->month = $d1->format('m');
                    $att->year = $d1->format('Y');
                    $att->save();

                    $getlast = Attendance::where("user_id", $request->id)->orderBy('timeout','DESC')->first();
                    $pw = ProjectWorker::where('worker_id',$request->id)->delete();

                    $newpw = new ProjectWorker;
                    $newpw->project_id = $getlast->project_id;
                    $newpw->worker_id = $request->id;
                    $newpw->is_active = 1;
                    $newpw->save();
                }          
            }

            $response['status'] = 'Success';
            $response['code'] = 200;

        return Response::json($response);
        }
    }

    public function saveWorkerAttendanceMultiple(Request $request) {

        $validator = Validator::make($request->all(), [
            //'id'                    => 'required',
            'time'               => 'required|array',
        ]);

        if($validator->fails()){
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;   
        } else {
            //$date = $request->date;
            $delete_status = 0;
            foreach($request->time as $t){  
                if( ($t['project_id'] != null || $t['project_id'] != '') && ($t['worker_id'] != null || $t['worker_id'] != '') && ($t['timein'][0] != null || $t['timein'][0] != '') && ($t['timein'][1] != null || $t['timein'][1] != '') ){ 
                    date_default_timezone_set("Asia/Manila");   

                    $d1 = new DateTime($t['timein'][0]);
                    $d2 = new DateTime($t['timein'][1]);
                    $interval = $d1->diff($d2); 
                    //\Log::info($interval);   
                    //\Log::info($interval->i);
                    $min = sprintf("%02d", $interval->i);   
                    $hr = sprintf("%02d", $interval->h);  
                    $hr = $hr * 60; 

                    if($delete_status == 0){
                        $del = Attendance::where("day",$d1->format('d'))
                                ->where("month",$d1->format('m'))
                                ->where("year",$d1->format('Y'))
                                ->where("user_id", $t['worker_id'])
                                ->delete();

                        $delete_status = 1;
                    }
                                
                    $att = new Attendance;
                    $att->user_id = $t['worker_id'];
                    $att->project_id = $t['project_id'];
                    $att->timein = $t['timein'][0];
                    $att->timeout = $t['timein'][1];
                    $att->duration = $hr + $min;
                    $att->day = $d1->format('d');
                    $att->month = $d1->format('m');
                    $att->year = $d1->format('Y');
                    $att->save();

                    $getlast = Attendance::where("user_id", $t['worker_id'])->orderBy('timeout','DESC')->first();
                    $pw = ProjectWorker::where('worker_id',$t['worker_id'])->delete();

                    $newpw = new ProjectWorker;
                    $newpw->project_id = $getlast->project_id;
                    $newpw->worker_id = $t['worker_id'];
                    $newpw->is_active = 1;
                    $newpw->save();
                }          
            }

            $response['status'] = 'Success';
            $response['code'] = 200;

        return Response::json($response);
        }
    }

    public function getDayAttendance(Request $request) {

        $validator = Validator::make($request->all(), [
            'id'                    => 'required',
            'date'               => 'required',
        ]);

        if($validator->fails()){
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;   
        } else {
            $id = $request->id;
            $date = $request->date;
            $project_id = $request->project_id;

            //$d = new DateTime($date);
            $d2 = date('Y-m-d', strtotime("+1 day", strtotime($date)));
            $d1 = date('Y-m-d', strtotime("-1 day", strtotime($date)));

            $attendance = Attendance::where('user_id', $id)
                            ->whereDate('timein', '>', $d1)
                            ->whereDate('timein', '<', $d2)
                            ->where('project_id', $project_id)
                            ->get();

            $att = [];
            $ctr = 0;
            foreach($attendance as $a){
                $dt = new DateTime($a->timein);
                $time = $dt->format('H:i:s');
                $att[$ctr]['timein'][0] = $time;
                $dt = new DateTime($a->timeout);
                $time = $dt->format('H:i:s');
                $att[$ctr]['timein'][1] = $time;
                $ctr++;
            }
            
            $response['status'] = 'Success';
            $response['data'] = $att;
            $response['code'] = 200;

        return Response::json($response);
        }
    }

}
