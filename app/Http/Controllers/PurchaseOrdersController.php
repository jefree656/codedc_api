<?php

namespace App\Http\Controllers;

use App\BankAccounts;
use App\BankTransfer;
use App\BankTransferFiles;
use App\Biller;
use App\User;
use App\ContactNumber;
use App\Categories;
use App\PurchaseOrder;
use App\PurchaseOrderDetails;
use App\Order;
use App\OrderLog;
use App\OrderDetails;
use App\Material;
use App\Project;
use App\Containers;
use App\Preset;
use App\Payment;
use App\PaymentBanks;
use App\PaymentComments;
use App\PaymentCommentFiles;
use App\PaymentCategories;
use App\PaymentDetails;
use App\PaymentFiles;
use App\Product;
use App\ProductCategory;
use App\ProductParentCategory;
use App\FinancingDelivery;
use App\Supplier;
use App\Inventory;
use App\TransactionLogs;

use Illuminate\Http\Request;

use Auth;

use Response;
use Validator;
use Hash, DB;
use Storage;

use Carbon\Carbon;

class PurchaseOrdersController extends Controller
{

    public function store(Request $request){
        // \Log::info($request);
        $validator = Validator::make($request->all(), [
            'supplier_id' => 'required'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();
            $response['code'] = 422;
        } else {
            // \Log::info($request->purchase_order_id);
            if($request->purchase_order_id == null){
                $order = new PurchaseOrder;
                $order->needed_date = $request->needed_date;
                $order->prepared_by = $request->prepared_by;
                $order->prepared_at = date("Y-m-d H:i:s");
                $order->status = $request->status;
                $order->supplier_id = $request->supplier_id;
                $order->contact_person = $request->supplier['contact_person'];
                $order->contact_number = $request->supplier['contact_number'];
                $order->bank = $request->supplier['bank'];
                $order->bank_account_name = $request->supplier['bank_account_name'];
                $order->bank_account_number = $request->supplier['bank_account_number'];
                $order->notes = $request->supplier['notes'];
                $order->carrier = implode (",", $request->supplier['carrier']);
                $order->save();
            }
            else{
                $order = PurchaseOrder::findOrFail($request->purchase_order_id);
                $order->prepared_by = $request->prepared_by;
                $order->prepared_at = date("Y-m-d H:i:s");
                $order->status = $request->status;
                $order->supplier_id = $request->supplier_id;
                $order->contact_person = $request->supplier['contact_person'];
                $order->contact_number = $request->supplier['contact_number'];
                $order->bank = $request->supplier['bank'];
                $order->bank_account_name = $request->supplier['bank_account_name'];
                $order->bank_account_number = $request->supplier['bank_account_number'];
                $order->notes = $request->supplier['notes'];
                $order->carrier = implode (",", $request->supplier['carrier']);
                if($request->needed_date != null){  
                    $order->needed_date = $request->needed_date;
                }
                $order->save();
                $res =PurchaseOrderDetails::where('purchase_order_id',$order->id)->delete();
            }

                $ctr = 0;
                $total = 0;
                foreach($request->selectedmaterials as $mat){
                    foreach($mat['items'] as $itm) {
                        $cate = null;
                        if(!is_numeric($itm['category_id']) && $itm['category_id'] != null && $itm['category_id'] != ''){
                            $cat = new Categories;
                            $cat->label = $itm['category_id'];
                            $cat->save();
                        }
                        else if($itm['category_id'] == '' || $itm['category_id'] == null){
                            $cate = null;
                        }

                        $subtotal = ($itm['qty'] * $itm['unit_price']);
                        $total += $subtotal;
                        $orderdet = new PurchaseOrderDetails;
                        $orderdet->purchase_order_id = $order->id;
                        $orderdet->material_id = $mat['material_id'];
                        $orderdet->project_id = $itm['project_id'];
                        $orderdet->category_id = is_numeric($itm['category_id']) ? $itm['category_id'] : $cate;
                        $orderdet->qty = $itm['qty'];
                        $orderdet->weight = $itm['weight'];
                        $orderdet->unit = $itm['unit'];
                        $orderdet->unit_price = $itm['unit_price'];
                        $orderdet->notes = $itm['notes'];
                        $orderdet->order_status = ($request->status == 'draft' ? 'draft' : 'to received');
                        $orderdet->subtotal = $subtotal;
                        $orderdet->save();
                    }
                    //$ctr++;
                }

                if($request->status == 'for approval'){
                    $paytype = 'PO';
                    $ctrPay = Payment::whereDate('created_at', Carbon::today())->where('paytype',$paytype)->count();
                    $ctr = str_pad($ctrPay+1, 3, '0', STR_PAD_LEFT); 
                    $pay = new Payment;
                    $pay->ref = $paytype.date("mdy").$ctr;
                    $pay->paytype = $paytype;
                    $pay->supplier_id = $request->supplier_id;
                    $pay->purchase_order_id = $order->id;
                    $pay->total = $total;
                    $pay->biller_id = 0;
                    $pay->status = $request->status;
                    $pay->is_active = 1;
                    $pay->requested_by = Auth::User()->id;
                    $pay->type = 'Expense';
                    $pay->save();
                }

            $response['status'] = 'Success';
            // $response['project_nickname'] = $nick;
            $response['code'] = 200;
            return Response::json($response);
        }
    }

    public function show($id){
        
        $po = PurchaseOrder::findOrFail($id);

        if( $po ) {
            $pod = PurchaseOrderDetails::where('purchase_order_id', $id)->groupBy('material_id')->get();
            $addmat = $pod->pluck('material_id');

            $mat = [];
            $ctr = 0;
            foreach($pod as $p){
                $pur = PurchaseOrderDetails::where('purchase_order_id', $id)->where('material_id',$p->material_id)->get();
                foreach($pur as $pr){
                    if($pr->project_id == 0){
                        $pr->project_id = 'Warehouse';
                    }
                }
                $mat[$ctr]['material_id'] = $p->material_id;
                $mat[$ctr]['items'] = $pur;
                $ctr++;
            }
            $response['status'] = 'Success';
            $response['data'] = [
                'po' => $po,
                'addmat' => $addmat,
                'mat' => $mat,
            ];
            $response['code'] = 200;
        } else {
            $response['status'] = 'Failed';
            $response['errors'] = 'No query results.';
            $response['code'] = 404;
        }

        return Response::json($response);
    }

    public function poChina(Request $request){
        // \Log::info($request);
                $paytype = 'POC';
                $ctrPay = Payment::whereDate('created_at', Carbon::today())->where('paytype',$paytype)->count();
                $ctr = str_pad($ctrPay+1, 3, '0', STR_PAD_LEFT); 
                $pay = new Payment;
                $pay->ref = $paytype.date("mdy").$ctr;
                $pay->date = null;
                $pay->type = 'Expense';
                $pay->paytype = $paytype;
                $pay->supplier_id = 0;
                $pay->payment_category_id = null;
                $pay->purchase_order_id = null;
                $pay->biller_id = 0;
                $pay->purchaser = null;
                $pay->with_order = 1;
                $pay->status = 'for approval';
                $pay->is_active = 1;
                $pay->requested_by = Auth::User()->id;
                $pay->save();

                $ctr = 0;
                $total = 0;
                foreach($request->selectedmaterials as $mat){
                    foreach($mat['items'] as $itm) {


                        $subtotal = ($itm['qty'] * $itm['unit_price']);
                        $total += $subtotal;
                        $orderdet = new OrderDetails;
                        $orderdet->order_id = 0;
                        $orderdet->product_id = $mat['material_id'];
                        // $orderdet->project_id = 0;
                        $orderdet->payment_id = $pay->id;
                        $orderdet->qty = $itm['qty'];
                        // $orderdet->weight = $itm['weight'];
                        $orderdet->preset_unit = $itm['unit'];
                        $orderdet->unit_price = $itm['unit_price'];
                        // $orderdet->notes = $itm['notes'];
                        $orderdet->order_status = 'POC';
                        $orderdet->total_price = $subtotal;
                        $orderdet->save();
                    }
                    //$ctr++;
                }
                
                $pay->total = $total;
                $pay->save();

            $response['status'] = 'Success';
            // $response['project_nickname'] = $nick;
            $response['code'] = 200;
            return Response::json($response);
        
    }

    public function getAllDrafts(Request $request){
        // return 0;
        $pos = PurchaseOrder::where('status','draft')->get();

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $pos;

        return Response::json($response);
    }

    public function getAllPaidPoc(Request $request){
        // return 0;
        $pay_ids = Payment::where('paytype','POC')->where('with_order', 1)->where('status','completed')->pluck('id');

        $pocs = OrderDetails::whereIn('payment_id', $pay_ids)
                    ->where('is_receive',0)->get();

            foreach ($pocs as $a=>$b){
                $mat = Material::findorfail($b->product_id);
                $order = Order::findOrFail($b->order_id);
                $proj = Project::findOrFail($order->project_id);
                $con = Containers::where('id', $b->container_id)->first();

                $b->con = '';
                $b->container = '';
                if($con){
                    $b->con = $con->type.' '.$con->id;

                    $ordet = OrderDetails::where('container_id', $b->container_id)->get();

                    foreach ($ordet as $c=>$d){
                        $order = Order::findorfail($d->order_id);
                        $proj = Project::findorfail($order->project_id);
                        $mat = Material::findorfail($d->product_id);
                        $d->proj_name = $proj->name;
                        $d->material_name = $mat->name;
                        //$b->remarks = $order->name;
                        //$b->weight = $mat->weight;
                    }
                    $con->materials = $ordet;

                    $b->container = $con;
                }
                $b->needed_date = $order->needed_date;
                $b->proj_name = $proj->name;
                $b->project_id = $proj->id;
                $b->task_id = $order->task_id;
                $b->material_name = $mat->name;
                //$b->weight = $mat->weight;
                $b->cbm = $b->length * $b->width * $b->height;
            }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $pocs;

        return Response::json($response);
    }

    public function getAllContainers(Request $request){
        // return 0;
        $cons = Containers::where('status','!=',null)->get();
        foreach($cons as $c=>$d){
            $pocs = OrderDetails::where('container_id', $d->id)
                    // ->where('order_status', 'POC')
                    ->get();

            foreach ($pocs as $a=>$b){
                $order = Order::findorfail($b->order_id);
                $proj = Project::findorfail($order->project_id);
                $mat = Material::findorfail($b->product_id);
                $b->proj_name = $proj->name;
                $b->material_name = $mat->name;
                //$b->remarks = $order->name;
                //$b->weight = $mat->weight;
            }
            $d->materials = $pocs;
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $cons;

        return Response::json($response);
    }

    public function getAllContainers2(Request $request){
        // return 0;
        $cons = Containers::where('status', 'to_received_ph')->get();
        foreach($cons as $c=>$d){
            $pocs = OrderDetails::where('container_id', $d->id)
                    ->where('is_receive',0)->where('order_status', 'POC')->get();

            foreach ($pocs as $a=>$b){
                $order = Order::findorfail($b->order_id);
                $proj = Project::findorfail($order->project_id);
                $mat = Material::findorfail($b->product_id);
                $b->proj_name = $proj->name;
                $b->material_name = $mat->name;
                //$b->weight = $mat->weight;
            }
            $d->materials = $pocs;
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $cons;

        return Response::json($response);
    }

    public function updateBankTransfer(Request $request) {

            $id = $request->translogs_id;
            $amt = $request->amt;
            $rate = $request->rate;
            if($rate == null){
                $rate = 1;
            }
            //$bankfrom = $request->bank_id;
            //$bankto = $request->bank_id;
            $trans = TransactionLogs::where('id', $id)->first();
            //\Log::info($trans);
            if($trans){
                $olddepo = $trans->deposit;
                $oldpay = $trans->payment;
                $trans->rate = $request->rate;

                $bt = BankTransfer::where('id',$request->transfer_id)->first();
                // if($bt->transfer_to == $bankfrom){
                //     $trans->deposit = $request->amt;
                // }
                // else{
                //     $trans->payment = $request->amt;
                // }
                
               //$trans->save();
                //\Log::info($bt);
                if($bt){
                    $bankfrom = BankAccounts::where('id', $bt->transfer_from)->first();
                    $bankto = BankAccounts::where('id', $bt->transfer_to)->first();

                    $tlogsfrom = TransactionLogs::where('bank_id', $bt->transfer_from)->where('transfer_id', $trans->transfer_id)->first();
                    $tlogsto = TransactionLogs::where('bank_id', $bt->transfer_to)->where('transfer_id', $trans->transfer_id)->first();

                    $multi = 1;
                    if($bankfrom->currency == $bankto->currency){
                        $multi = 1;
                    }
                    else{
                        if($bankfrom->currency == 'CNY'){
                            $multi = 1;
                        }
                        else{
                            $multi = 0;
                        }
                    }

                    //if($trans->bank_id == $bankfrom){
                        $diff = $amt - $bt->amount;

                        //\Log::info($diff);
                        if($diff != 0 || $rate != $bt->rate){
                            $bankfrom->balance = $bankfrom->balance - $diff;
                            $bankfrom->save();
                            
                            if($multi == 1){
                                $diff = ($diff * $rate);
                                if($diff == 0){
                                    $diff =  ($bt->amount * $rate) - ($bt->amount * $bt->rate);
                                }
                            }
                            else{
                                $diff = ($diff / $rate);
                                if($diff == 0){
                                    $diff =  ($bt->amount/$rate) - ($bt->amount/$bt->rate);
                                }
                            }
                            $bankto->balance = $bankto->balance + $diff;
                            $bankto->save();   
                        }

                        $bt->amount = $amt;
                        $bt->rate = $rate;
                        $bt->save();


                        $tlogsfrom->rate = $rate;
                        $tlogsfrom->payment = $amt;
                        $tlogsfrom->balance = $bankfrom->balance;
                        $tlogsfrom->save();

                        $tlogsto->rate = $rate;
                        if($multi == 1){
                            $tlogsto->deposit = $amt * $rate;
                        }
                        else{
                            $tlogsto->deposit = $amt / $rate;
                        }
                        
                        $tlogsto->balance = $bankto->balance;
                        $tlogsto->save();

                    //}

                    // else{
                    //     $diff = $bt->amount - $amt;

                    //     if($diff != 0 || $rate != $bt->rate){
                    //         $from->balance = $from->balance - $diff;
                    //         $from->save();
                            
                    //         if($multi == 1){
                    //             $diff = ($diff * $rate) - ($bt->amount * $bt->rate);
                    //         }
                    //         else{
                    //             $diff = ($diff / $rate) - ($bt->amount / $bt->rate);
                    //         }
                    //         $to->balance = $to->balance + $diff;
                    //         $to->save();   
                    //     }

                    //     $bt->amount = $amt;
                    //     $bt->rate = $rate;
                    //     $bt->save();


                    //     $tlogsfrom->rate = $rate;
                    //     $tlogsfrom->payment = $amt;
                    //     $tlogsfrom->balance = $from->balance;
                    //     $tlogsfrom->save();

                    //     $tlogsto->rate = $rate;
                    //     if($multi == 1){
                    //         $tlogsto->deposit = $amt * $rate;
                    //     }
                    //     else{
                    //         $tlogsto->deposit = $amt / $rate;
                    //     }
                        
                    //     $tlogsto->balance = $to->balance;
                    //     $tlogsto->save();
                    // }

                }
            }


            $response['status'] = 'Success';
            $response['code'] = 200;
            
        return Response::json($response);
    }

    public function getBankHistory(Request $request){
        $bankId = $request->input('bankId');
        $bank = BankAccounts::findorfail($bankId);

        $trans = TransactionLogs::where('bank_id', $bankId)->orderBy('id','Desc')->get();

        foreach($trans as $t){
            // \Log::info($t->id);
            $t->details = null;
            // $t->files = [];
            if($t->payment_id > 0){            
                $p = Payment::findOrFail($t->payment_id);
                $t->single = $p->bank_id;
                // \Log::info($p);
                $t->payee = '';
                if($p->biller_id > 0){
                    $biller = Biller::findOrFail($p->biller_id); 
                    $t->payee = $biller->name;
                }
                else if($p->supplier_id > 0){
                    $supp = Supplier::findOrFail($p->supplier_id); 
                    $t->payee = $supp->full_name;
                }
                else if($p->purchaser != null){
                    $t->payee = $p->purchaser;
                }
                $payor = BankAccounts::findorfail($bankId);
                 $t->payor = $payor->name;

                $t->project = null;
                $t->ref = $p->ref;
                $t->purchase_order_id = $p->purchase_order_id;
                $t->with_order = $p->with_order;

                $paydet = PaymentDetails::where('payment_id', $t->payment_id)->groupBy('project_id')->get();
                //\Log::info($paydet);
                foreach($paydet as $pd){
                    //\Log::info('bankID : '.$pd->project_id);
                    $proj = Project::where('id',$pd->project_id)->first();
                    if($pd->project_id > 0){
                        $t->project .= $proj->name . ', ';
                    }
                    else if($pd->project_id == 0){
                        $t->project .= 'Warehouse, ';
                    }
                }
                $t->project = rtrim($t->project, ", ");
                $t->type = ($t->deposit > 0 ? 'Deposit' : 'Payment');
            }
            else if($t->transfer_id > 0){ 
                $bt = BankTransfer::findorfail($t->transfer_id);
                
                //$payee = ($bankId != $bt->transfer_from ? $bt->transfer_to : $bt->transfer_from);
                $payee = BankAccounts::findorfail($bt->transfer_to);
                $payor = BankAccounts::findorfail($bt->transfer_from);
                $t->project = '';
                $t->purchase_order_id = 0;
                $t->with_order = 0;
                $t->ref = 'TRANSFER'.$bt->id;
                $t->payee = $payee->name;
                $t->payor = $payor->name;
                $t->type = 'Bank Transfer';
                $bt->bfrom = BankAccounts::findorfail($bt->transfer_from)->name;
                $bt->bto = BankAccounts::findorfail($bt->transfer_to)->name;
                $bt->date = $t->created_at;
                // $bt->amt = ($t->deposit > 0 ? $t->deposit : $t->payment);
                $bt->amt = $bt->amount;
                $bt->rate = $t->rate;
                $bt->transfer_id = $t->transfer_id;
                $bt->translogs_id = $t->id;
                $bt->files = BankTransferFiles::where('bank_transfer_id', $t->transfer_id)->get();
                $t->details = $bt;

            }   
            // \Log::info($t);
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $trans;
        $response['bank'] = $bank;

        return Response::json($response);
    }

    public function getAllPayments(Request $request){
        $viewMode = $request->input('viewMode');
        $statusMode = $request->input('statusMode');
        $sort = $request->input('sort');
        $perPage  = $request->input('perPage');
        $search  = $request->input('search');

        $srt = '';
        $srt1 = '';
        if($sort != ''){
            $srts = explode('-' , $sort);
            $srt = $srts[0];
            $srt1 = $srts[1];
        }
        
        // $list = '';
        // $mids = '';

        // $item_ids =  Payment::orderBy('id','DESC')->pluck('id');

        // if($sort != ''){
        //     $sort = explode('-' , $sort);
        //     if($sort[0] == 'name' || $sort[0] == 'type' || $srt == 'id') {
        //             $list = Material::when($search != '', function ($q) use($search){
        //                         return $q->where('name','LIKE', '%'.$search.'%')->orwhere('type','LIKE', '%'.$search.'%');
        //                     })->whereIn('id', $item_ids)
        //                     ->orderBy($sort[0], $sort[1])->get()->pluck('id');
        //     }
        // }
        
        // if($list != ''){
        //     $mids = "'".implode("', '", $list->toArray())."'";
        // }

        $pos = Payment::where('is_active', 1)
                // ->when($search != '', function ($q) use($search){
                //     return $q->where('ref','LIKE', '%'.$search.'%')->orwhere('purchaser','LIKE', '%'.$search.'%');
                // })
                ->when($viewMode != 'All', function ($q) use($viewMode) {
                        return $q->where('paytype', $viewMode);
                })
                ->when($statusMode == 'Completed', function ($q) use($statusMode) {
                        return $q->where('status', 'completed');
                })
                ->when($statusMode == 'On Process', function ($q) use($statusMode) {
                        return $q->where('status', '!=', 'completed');
                })
                ->when($srt != '' && ($srt == 'id' || $srt == 'ref' || $srt == 'date' || $srt == 'purchaser' || $srt == 'total' || $srt == 'payout'), function ($q) use($srt, $srt1) {
                        return $q->orderBy($srt, $srt1);
                })
                ->get();

        foreach($pos as $p){
            //$p->payee = $p->purchaser;
            if($p->biller_id > 0){
                $biller = Biller::findOrFail($p->biller_id); 
                $p->payee = $biller->name;
            }
            if($p->supplier_id > 0){
                $supp = Supplier::findOrFail($p->supplier_id); 
                $p->payee = $supp->full_name;
            }
            $p->status2 = '';
            if($p->status == 'approved'){
                $ttl = ($p->payout - $p->payout_return);
                if($p->total == $ttl){
                    $p->status2 = 'Pending Receipt';
                }
                else{
                    $p->status2 = 'Partial';
                }
            }
            if($p->status == 'for approval'){
                $p->status2 = 'Request';
            }
            if($p->status == 'completed'){
                $p->status2 = 'Completed';
            }
            $p->project = '';
            $p->ordernotes = '';
            $p->order_id = '';
            $p->project_id = 0;
            $p->task_id = 0;
            $paydet = PaymentDetails::where('payment_id', $p->id)->groupBy('project_id')->get();
            // \Log::info($paydet);
                foreach($paydet as $pd){
                    if($pd->project_id > 0){
                        $proj = Project::findorfail($pd->project_id);
                        $p->project .= $proj->nick_name . ', ';
                    }
                    else if($pd->project_id == 0){
                        $p->project = 'Warehouse';
                    }
                }
            if($p->with_order){
                $pod = OrderDetails::where('payment_id', $p->id)->groupBy('order_id')->first();
                if($pod){
                    $po = Order::findorfail($pod->order_id);
                    $p->order_id = $pod->order_id;
                    if($po){
                        $p->ordernotes = $po->remarks;
                        $p->task_id = $po->task_id;
                        if($po->project_id > 0){
                            $proj = Project::findorfail($po->project_id);
                            $p->project .= $proj->nick_name;
                            $p->project_id = $proj->id;
                        }
                        else if($po->project_id == 0){
                            $p->project = 'Warehouse';
                        }
                    }
                }
            }

            $p->ref2 = strtolower($p->ref);

            $p->project = rtrim($p->project, ", ");
            if($p->project == ''){
                $p->project = "-";
            }
            $p->project2 = strtolower($p->project);
            if($p->project2 == null){
                $p->project2 = 'a';
            }

            $p->purchaser2 = strtolower($p->purchaser);
            if($p->purchaser2 == null){
                $p->purchaser2 = 'a';
            }

            $p->balance =  (
                                floatval($p->total) +
                                floatval($p->parking > 0 ? $p->parking : 0) +
                                floatval($p->gas > 0 ? $p->gas : 0) +
                                floatval($p->delivery_fee > 0 ? $p->delivery_fee : 0) +
                                floatval($p->toll > 0 ? $p->toll : 0) +
                                floatval($p->payout_return > 0 ? $p->payout_return : 0)
                            ) 
                            - floatval($p->payout);

        }

        // if($srt != '' && ($srt == 'project')){            
        //     if($srt1 == 'asc'){
        //         $pos = $pos->sortBy('id');
        //             //->all();
        //     }
        //     else{
        //         $pos = $pos->sortByDesc('id');
        //             //->all();
        //     }
        // }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $pos;

        return Response::json($response);
    }

    public function deletePaymentComment(Request $request){
        $id = $request->id;
        $res=PaymentComments::where('id',$id)->delete();
        //$res2=ProjectTask::where('parent_id',$id)->delete();
        $response['status'] = 'Success';
        $response['code'] = 200;
        return Response::json($response);
    }    

    public function deletePayment(Request $request){
        $id = $request->id;
        $res=Payment::where('id',$id)->delete();

        $log = TransactionLogs::where('payment_id', $id)->delete();

        $orders = OrderDetails::where('payment_id',$id)
                    ->update(['payment_id' => null, 'prepared_by' => null, 'order_status' => 'site order']);

        //$res2=ProjectTask::where('parent_id',$id)->delete();
        $response['status'] = 'Success';
        $response['code'] = 200;
        return Response::json($response);
    }

    public function getPaymentInfo($payment_id, Request $request) {
            $pay = Payment::findorfail($payment_id);
            $comments = PaymentComments::where('payment_id',$pay->id)->orderBy('id','desc')->get();
            foreach($comments as $c){
                $user = User::where('id', $c->user_id)->first();
                $c->user = $user->first_name.' '.$user->last_name;
                $c->files = PaymentCommentFiles::where('comment_id', $c->id)->get();
            }
            //$is_parent = ProjectTask::where('parent_id', $task_id)->first();
            //$task->is_parent = ($is_parent) ? 1 : 0;

            $users = User::select('id',DB::raw("CONCAT(first_name,' ',last_name) AS label"), DB::raw("CONCAT(substr(first_name,1,1),last_name,id,' ') AS value"))
            // ->where('id','!=',Auth::user()->id)
            ->get();

            $response['users'] = $users;
            $response['data'] = $pay;
            $response['comments'] = $comments;
            $response['status'] = 'Success';
            $response['code'] = 200;
        
        return Response::json($response);
    }

    public function getAllBanks(Request $request){
        // return 0;
        $pos = BankAccounts::get();

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $pos;

        return Response::json($response);
    }

    public function getAllBillers(Request $request){
        // return 0;
        $pos = Biller::get(); 
        foreach($pos as $p){
            $p->bid = 'b'.$p->id ;
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $pos;

        return Response::json($response);
    }

    public function getAllPaymentCategories(Request $request){
        // return 0;
        $list = PaymentCategories::get();

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $list;

        return Response::json($response);
    }

    public function changePaymentCategory(Request $request) {

        if(!is_numeric($request->cat)){
            $cat = new PaymentCategories;
            $cat->label = $request->cat;
            $cat->save();
        }

        if($request->type == 'payment'){
            $pt = Payment::findOrFail($request->id);
            $pt->payment_category_id = is_numeric($request->cat) ? $request->cat : $cat->id;
            $pt->save();
        }
                
            $response['status'] = 'Success';
            $response['code'] = 200;
        

        return Response::json($response);
    }

    public function getPaymentDetails($id){
        
        $po = Payment::findOrFail($id);
        $addmat = [];
        $mat = [];
        $paydet = [];
        $bankdet = [];
        if( $po ) {
            $paydet = PaymentDetails::where('payment_id',$id)->get();
            foreach($paydet as $p){
                if($p->project_id == 0){
                        $p->project_id = 'Warehouse';
                    }
            }

            $bankdet = PaymentBanks::where('payment_id',$id)->get();

            if($po->purchase_order_id > 0){
                $pod = PurchaseOrderDetails::where('purchase_order_id', $po->purchase_order_id)->groupBy('material_id')->get();
                $addmat = $pod->pluck('material_id');

                $mat = [];
                $ctr = 0;
                foreach($pod as $p){
                    $pur = PurchaseOrderDetails::where('purchase_order_id', $po->purchase_order_id)->where('material_id',$p->material_id)->get();
                    foreach($pur as $pr){
                        if($pr->project_id == 0){
                            $pr->project_id = 'Warehouse';
                        }
                    }
                    $mat[$ctr]['material_id'] = $p->material_id;
                    $mat[$ctr]['items'] = $pur;
                    $ctr++;
                }
            }

            if($po->with_order > 0){
                $pod = OrderDetails::where('payment_id', $po->id)->groupBy('product_id')->where('hidden',1)->get();
                $addmat = $pod->pluck('product_id');

                $mat = [];
                $ctr = 0;
                foreach($pod as $p){
                    $pur = OrderDetails::where('payment_id', $po->id)->where('hidden',1)->where('product_id',$p->product_id)->get();
                    foreach($pur as $pr){
                            $mmm = Material::findorfail($pr->product_id);
                            $pr->project_id = 'Warehouse';
                            $pr->project_name = 'Warehouse';
                            if($pr->order_id > 0){
                                $o = Order::findOrFail($pr->order_id);
                                $ppp = Project::findOrFail($o->project_id);
                                if($o->project_id == 0){
                                    $pr->project_id = $o->project_id;
                                    $pr->project_name = 'Warehouse';
                                }
                                else{
                                    $pr->project_id = $o->project_id;
                                    $pr->project_name = $ppp->name;
                                }
                            }
                            $pr->unit = $mmm->unit;
                            //$pr->weight = $mmm->weight;
                            //$pr->width = $mmm->width;
                            //$pr->length = $mmm->length;
                            //$pr->height = $mmm->height;
                            $pr->type = $mmm->type;
                    }
                    $mat[$ctr]['material_id'] = $p->product_id;
                    $mat[$ctr]['material_name'] = $mmm->name;
                    $mat[$ctr]['items'] = $pur;
                    $ctr++;
                }
            }

            if($po->requested_by > 0){
                $user = User::where('id', $po->requested_by)->first();
                if($user){
                    $po->requested_by = $user->first_name;
                }
            }  
            if($po->approved_by > 0){
                $user2 = User::where('id', $po->approved_by)->first();
                if($user2){
                    $po->approved_by = $user2->first_name;
                }
            } 

            $files = [];

            $files = PaymentFiles::where('payment_id', $id)->get();

            $response['status'] = 'Success';
            $response['data'] = [
                'po' => $po,
                'addmat' => $addmat,
                'mat' => $mat,
                'paydet' => $paydet,
                'bankdet' => $bankdet,
                'files' => $files
            ];
            $response['code'] = 200;
        } else {
            $response['status'] = 'Failed';
            $response['errors'] = 'No query results.';
            $response['code'] = 404;
        }

        return Response::json($response);
    }

    public function getPaymentDetails2($id){
        
        $po = Payment::findOrFail($id);
        $addmat = [];
        $mat = [];
        $paydet = [];
        if( $po ) {
            $paydet = PaymentDetails::where('payment_id',$id)->get();
            foreach($paydet as $p){
                if($p->project_id == 0){
                        $p->project_id = 'Warehouse';
                    }
            }

            $files = [];

            $files = PaymentFiles::where('payment_id', $id)->get();

            $response['status'] = 'Success';
            $response['data'] = [
                'po' => $po,
                'addmat' => $addmat,
                'mat' => $mat,
                'paydet' => $paydet,
                'files' => $files
            ];
            $response['code'] = 200;
        } else {
            $response['status'] = 'Failed';
            $response['errors'] = 'No query results.';
            $response['code'] = 404;
        }

        return Response::json($response);
    }

    public function addPayment(Request $request){
            $supplier_id = 0;
            $biller_id = 0;
            $purchaser = null;
            if(is_numeric($request->supplier_id)){
                $supplier_id = $request->supplier_id;
            }
            else if (preg_match('~[0-9]+~',  $request->supplier_id)) {
                $biller_id = preg_replace('/[^0-9]/', '', $request->supplier_id);
            }
            $con = false;
            // else{
            //     $purchaser = $request->supplier_id;
            // }

            if($request->payment_id == null){
                $ctrPay = Payment::whereDate('created_at', Carbon::today())->where('paytype',$request->paytype)->count();
                $ctr = str_pad($ctrPay+1, 3, '0', STR_PAD_LEFT); 
                $pay = new Payment;
                $pay->ref = $request->paytype.date("mdy").$ctr;
                $pay->date = $request->date;
                $pay->paytype = $request->paytype;
                $pay->type = 'Expense';
                $pay->description = $request->description;
                $pay->payout_type = $request->payout_type;
                $pay->checknum = $request->checknum;
                $pay->payout = $request->payout_total;
                $pay->payout_return = $request->payout_return;
                $pay->delivery_fee = $request->delivery_fee;
                $pay->gas = $request->gas;
                $pay->parking = $request->parking;
                $pay->toll = $request->toll;
                $pay->total = $request->totalsrp;
                $pay->bank_id = $request->bank_id;
                $pay->supplier_id = $supplier_id;
                $pay->biller_id = $biller_id;
                $pay->payment_category_id = null;
                $pay->purchase_order_id = $request->purchase_order_id;
                $pay->purchaser = $request->purchaser;
                $pay->is_active = 1;
                $pay->requested_by = Auth::User()->id;
                $pay->status = $request->status;
                if($request->completed_at == 1){
                    $pay->completed_at = date("Y-m-d H:i:s");
                }
                $pay->save();

                if($request->bank_id > 0 && $request->status == 'approved'){
                    $bank = BankAccounts::findorfail($request->bank_id);
                    $bank->balance = $bank->balance - ($request->payout_total - $request->payout_return);
                    $bank->save();

                    $trans = new TransactionLogs;
                    $trans->bank_id = $pay->bank_id;
                    $trans->payment_id = $pay->id;
                    $trans->user_id = Auth::User()->id;
                    $trans->detail = '';
                    $trans->payment = $request->payout_total- $request->payout_return;
                    $trans->balance = $bank->balance;
                    $trans->save();
                }
            }
            else{
                $pay = Payment::findOrFail($request->payment_id);
                $oldtotal = $pay->payout - $pay->payout_return;
                $pay->date = $request->date;
                $pay->type = 'Expense';
                $pay->description = $request->description;
                $pay->payout_type = $request->payout_type;
                $pay->checknum = $request->checknum;
                $pay->payout = $request->payout_total;
                $pay->payout_return = $request->payout_return;
                $pay->gas = $request->gas;
                $pay->toll = $request->toll;
                $pay->delivery_fee = $request->delivery_fee;
                $pay->parking = $request->parking;
                $pay->total = $request->totalsrp;
                $pay->bank_id = $request->bank_id;
                $pay->supplier_id = $supplier_id;
                $pay->biller_id = $biller_id;
                $pay->payment_category_id = null;
                $pay->purchase_order_id = $request->purchase_order_id;
                $pay->purchaser = $request->purchaser;
                $pay->is_active = 1;
                $pay->status = $request->status;

                if($request->status == 'approved'){
                    $pay->approved_by = Auth::User()->id;
                }

                if($request->completed_at == 1){
                    $pay->completed_at = date("Y-m-d H:i:s");
                }

                $pay->save();
                $newtotal = $request->payout_total - $request->payout_return;

                if($request->with_order > 0){
                    foreach($request->selectedmaterials as $selmat){
                        foreach($selmat['items'] as $itm){
                            $ordet = OrderDetails::findorfail($itm['id']);
                            $ordet->paynotes = $itm['paynotes'];
                            $ordet->qty = $itm['qty'];
                            $ordet->unit_price = $itm['unit_price'];
                            $ordet->total_price = $itm['unit_price'] * $itm['qty'];
                            $ordet->hidden = $itm['hidden'];
                            if($itm['hidden'] == 0){
                                $ordet->unit_price = 0;
                                $ordet->total_price = 0;
                            }
                            $ordet->save();
                        }
                    }
                }

                $getpaytrans = TransactionLogs::where('payment_id',$request->payment_id)->get();
                $transtotal = 0;
                foreach($getpaytrans as $g){
                    $transtotal += $g->payment;
                    $transtotal -= $g->deposit;
                }   

                if($request->bank_id > 0 && ($newtotal != $oldtotal || $transtotal == 0) && 
                    ($request->status == 'approved' || $request->status == 'completed')){
                    $totalsrp = 0;
                    if($newtotal != $oldtotal){
                        $totalsrp = $newtotal - $oldtotal;
                    }
                    if($transtotal == 0){
                        $totalsrp = $newtotal;
                    }
                    $bank = BankAccounts::findorfail($request->bank_id);
                    $bank->balance = $bank->balance - $totalsrp;
                    $bank->save();

                    $trans = new TransactionLogs;
                    $trans->bank_id = $pay->bank_id;
                    $trans->payment_id = $pay->id;
                    $trans->user_id = Auth::User()->id;
                    $trans->detail = '';
                    $trans->payment = ($totalsrp > 0 ? $totalsrp : 0);
                    $trans->deposit = ($totalsrp < 0 ? $totalsrp * -1 : 0);
                    $trans->balance = $bank->balance;
                    $trans->save();
                }

                $con = Containers::where('payment_id', $pay->id)->first();

                if(!$con){
                    $res =PaymentDetails::where('payment_id',$pay->id)->delete();
                }

                if($request->status == 'completed'){

                    if($con){

                        Containers::where('payment_id', $pay->id)
                              ->where('paid',0)->where('status','for loading')->where('type','Container')
                              ->update(['paid' => 1]);

                        Containers::where('payment_id', $pay->id)
                              ->where('paid2',0)->where('status','to_received_ph')
                              ->update(['paid2' => 1]);
                    }

                }

                if($request->status == 'approved'){

                    if($pay->purchase_order_id > 0){
                        $user = Auth::User()->first_name.' '.Auth::User()->last_name;
                        PurchaseOrderDetails::where('purchase_order_id', $pay->purchase_order_id)
                              ->where('order_status','for approval')
                              // ->where(
                              //  function($query) {
                              //    return $query
                              //           ->orwhere('order_status', 'SPC')
                              //           ->orWhere('order_status', 'POC');
                              //   })
                              ->update(['order_status' => 'to received', 'shipped_by' => $user]);
                    }
                }
                //$res =PurchaseOrderDetails::where('purchase_order_id',$pay->purchase_order_id)->delete();
            }

            if(!$con){
                foreach($request->details as $itm){
                        $cate = null;
                        if(!is_numeric($itm['category_id']) && $itm['category_id'] != null && $itm['category_id'] != ''){
                            $cat = new PaymentCategories;
                            $cat->label = $itm['category_id'];
                            $cat->save();
                            $cate = $cat->id;
                        }
                        else if($itm['category_id'] == '' || $itm['category_id'] == null){
                            $cate = null;
                        }

                        //$subtotal = ($itm['qty'] * $itm['unit_price']);
                        //$total += $subtotal;
                        $orderdet = new PaymentDetails;
                        $orderdet->payment_id = $pay->id;
                        $orderdet->project_id = $itm['project_id'];
                        $orderdet->category_id = is_numeric($itm['category_id']) ? $itm['category_id'] : $cate;
                        $orderdet->amount = $itm['amount'];
                        $orderdet->description = $itm['description'];
                        $orderdet->save();
                }
            }

            $response['status'] = 'Success';
            $response['payment_id'] = $pay->id;
            $response['code'] = 200;
            return Response::json($response);
        // }
    }    

    public function addPaymentMultipleBank(Request $request){
            $supplier_id = 0;
            $biller_id = 0;
            $purchaser = null;
            if(is_numeric($request->supplier_id)){
                $supplier_id = $request->supplier_id;
            }
            else if (preg_match('~[0-9]+~',  $request->supplier_id)) {
                $biller_id = preg_replace('/[^0-9]/', '', $request->supplier_id);
            }
            $con = false;
            // else{
            //     $purchaser = $request->supplier_id;
            // }

            if($request->payment_id == null){ 
                $ctrPay = Payment::whereDate('created_at', Carbon::today())->where('paytype',$request->paytype)->count();
                $ctr = str_pad($ctrPay+1, 3, '0', STR_PAD_LEFT); 
                $pay = new Payment;
                $pay->ref = $request->paytype.date("mdy").$ctr;
                $pay->date = $request->date;
                $pay->paytype = $request->paytype;
                $pay->type = 'Expense';
                $pay->description = $request->description;
                $pay->payout_type = $request->payout_type;
                $pay->checknum = $request->checknum;
                $pay->payout = $request->payout_total;
                $pay->payout_return = $request->payout_return;
                $pay->delivery_fee = $request->delivery_fee;
                $pay->gas = $request->gas;
                $pay->parking = $request->parking;
                $pay->toll = $request->toll;
                $pay->total = $request->totalsrp;
                $pay->bank_id = $request->bank_id;
                $pay->supplier_id = $supplier_id;
                $pay->biller_id = $biller_id;
                $pay->payment_category_id = null;
                $pay->purchase_order_id = $request->purchase_order_id;
                $pay->purchaser = $request->purchaser;
                $pay->is_active = 1;
                $pay->requested_by = Auth::User()->id;
                $pay->status = $request->status;
                if($request->completed_at == 1){
                    $pay->completed_at = date("Y-m-d H:i:s");
                }
                $pay->save();

                // $delba = PaymentBanks::where('payment_id', $pay->id)->delete();
                foreach($request->bankaccts as $ba){
                    $paybank = new PaymentBanks;
                    $paybank->payment_id = $pay->id;
                    $paybank->bank_id = $ba['bank_id'];
                    $paybank->amount = $ba['amount'];
                    $paybank->save();

                    if($request->status == 'approved'){
                        $bank = BankAccounts::findorfail($ba['bank_id']);
                        $bank->balance = $bank->balance - ($ba['amount']);
                        $bank->save();

                        $trans = new TransactionLogs;
                        $trans->bank_id = $ba['bank_id'];
                        $trans->payment_id = $pay->id;
                        $trans->user_id = Auth::User()->id;
                        $trans->detail = '';
                        $trans->payment = $ba['amount'];
                        $trans->balance = $bank->balance;
                        $trans->save();
                    }
                }

            }
            else{
                $pay = Payment::findOrFail($request->payment_id);
                $oldtotal = $pay->payout - $pay->payout_return;
                $pay->date = $request->date;
                $pay->type = 'Expense';
                $pay->description = $request->description;
                $pay->payout_type = $request->payout_type;
                $pay->checknum = $request->checknum;
                $pay->payout = $request->payout_total;
                $pay->payout_return = $request->payout_return;
                $pay->gas = $request->gas;
                $pay->toll = $request->toll;
                $pay->delivery_fee = $request->delivery_fee;
                $pay->parking = $request->parking;
                $pay->total = $request->totalsrp;
                $pay->bank_id = $request->bank_id;
                $pay->supplier_id = $supplier_id;
                $pay->biller_id = $biller_id;
                $pay->payment_category_id = null;
                $pay->purchase_order_id = $request->purchase_order_id;
                $pay->purchaser = $request->purchaser;
                $pay->is_active = 1;
                $pay->status = $request->status;

                if($request->status == 'approved'){
                    $pay->approved_by = Auth::User()->id;
                }

                if($request->completed_at == 1){
                    $pay->completed_at = date("Y-m-d H:i:s");
                }

                $pay->save();


                $newtotal = $request->payout_total - $request->payout_return;

                if($request->with_order > 0){
                    foreach($request->selectedmaterials as $selmat){
                        foreach($selmat['items'] as $itm){
                            $ordet = OrderDetails::findorfail($itm['id']);
                            $ordet->paynotes = $itm['paynotes'];
                            $ordet->qty = $itm['qty'];
                            $ordet->unit_price = $itm['unit_price'];
                            $ordet->total_price = $itm['unit_price'] * $itm['qty'];
                            $ordet->hidden = $itm['hidden'];
                            if($itm['hidden'] == 0){
                                $ordet->unit_price = 0;
                                $ordet->total_price = 0;
                            }
                            $ordet->save();
                        }
                    }
                }
                $bank_ids = [];
                $bankctr = 0;
                $allbanks = PaymentBanks::where('payment_id', $pay->id)->pluck('bank_id');
                $delba = PaymentBanks::where('payment_id', $pay->id)->delete();
                foreach($request->bankaccts as $ba){
                    $bank_ids[] = $ba['bank_id'];
                    $bankctr++;
                    $paybank = new PaymentBanks;
                    $paybank->payment_id = $pay->id;
                    $paybank->bank_id = $ba['bank_id'];
                    $paybank->amount = $ba['amount'];
                    $paybank->save();
                    $getpaytrans = TransactionLogs::where('payment_id',$request->payment_id)->where('bank_id', $ba['bank_id'])->get();
                    $transtotal = 0;
                    foreach($getpaytrans as $g){
                        $transtotal += $g->payment;
                        $transtotal -= $g->deposit;
                    }   

                    $oldtotal = $transtotal;
                    $newtotal = $ba['amount'];

                    if($ba['bank_id'] > 0 && ($newtotal != $oldtotal || $transtotal == 0) && 
                        ($request->status == 'approved' || $request->status == 'completed')){
                        $totalsrp = 0;
                        if($newtotal != $oldtotal){
                            $totalsrp = $newtotal - $oldtotal;
                        }
                        if($transtotal == 0){
                            $totalsrp = $newtotal;
                        }
                        $bank = BankAccounts::findorfail($ba['bank_id']);
                        $bank->balance = $bank->balance - $totalsrp;
                        $bank->save();

                        $trans = new TransactionLogs;
                        $trans->bank_id = $ba['bank_id'];
                        $trans->payment_id = $pay->id;
                        $trans->user_id = Auth::User()->id;
                        $trans->detail = '';
                        $trans->payment = ($totalsrp > 0 ? $totalsrp : 0);
                        $trans->deposit = ($totalsrp < 0 ? $totalsrp * -1 : 0);
                        $trans->balance = $bank->balance;
                        $trans->save();
                    }
                }

                $banksNotIncluded = TransactionLogs::where('payment_id',$request->payment_id)->whereIn('bank_id', $allbanks)->whereNotIn('bank_id', $bank_ids)->get();

                    $transtotal = 0;
                    foreach($banksNotIncluded as $g){
                        $transtotal += $g->payment;
                        $transtotal -= $g->deposit;
                    
                        $oldtotal = $transtotal;
                        $newtotal = 0;

                        if($g->bank_id > 0 && ($newtotal != $oldtotal || $transtotal == 0) && 
                            ($request->status == 'approved' || $request->status == 'completed')){
                            $totalsrp = 0;
                            if($newtotal != $oldtotal){
                                $totalsrp = $newtotal - $oldtotal;
                            }
                            if($transtotal == 0){
                                $totalsrp = $newtotal;
                            }
                            $bank = BankAccounts::findorfail($g->bank_id);
                            $bank->balance = $bank->balance - $totalsrp;
                            $bank->save();

                            $trans = new TransactionLogs;
                            $trans->bank_id = $g->bank_id;
                            $trans->payment_id = $pay->id;
                            $trans->user_id = Auth::User()->id;
                            $trans->detail = '';
                            $trans->payment = ($totalsrp > 0 ? $totalsrp : 0);
                            $trans->deposit = ($totalsrp < 0 ? $totalsrp * -1 : 0);
                            $trans->balance = $bank->balance;
                            $trans->save();
                        }
                    }

                $con = Containers::where('payment_id', $pay->id)->first();

                if(!$con){
                    $res =PaymentDetails::where('payment_id',$pay->id)->delete();
                }

                if($request->status == 'completed'){

                    if($con){

                        Containers::where('payment_id', $pay->id)
                              ->where('paid',0)->where('status','for loading')->where('type','Container')
                              ->update(['paid' => 1]);

                        Containers::where('payment_id', $pay->id)
                              ->where('paid2',0)->where('status','to_received_ph')
                              ->update(['paid2' => 1]);
                    }

                }

                if($request->status == 'approved'){

                    if($pay->purchase_order_id > 0){
                        $user = Auth::User()->first_name.' '.Auth::User()->last_name;
                        PurchaseOrderDetails::where('purchase_order_id', $pay->purchase_order_id)
                              ->where('order_status','for approval')
                              // ->where(
                              //  function($query) {
                              //    return $query
                              //           ->orwhere('order_status', 'SPC')
                              //           ->orWhere('order_status', 'POC');
                              //   })
                              ->update(['order_status' => 'to received', 'shipped_by' => $user]);
                    }
                }
                //$res =PurchaseOrderDetails::where('purchase_order_id',$pay->purchase_order_id)->delete();
            }

            if(!$con){
                foreach($request->details as $itm){
                        $cate = null;
                        if(!is_numeric($itm['category_id']) && $itm['category_id'] != null && $itm['category_id'] != ''){
                            $cat = new PaymentCategories;
                            $cat->label = $itm['category_id'];
                            $cat->save();
                            $cate = $cat->id;
                        }
                        else if($itm['category_id'] == '' || $itm['category_id'] == null){
                            $cate = null;
                        }

                        //$subtotal = ($itm['qty'] * $itm['unit_price']);
                        //$total += $subtotal;
                        $orderdet = new PaymentDetails;
                        $orderdet->payment_id = $pay->id;
                        $orderdet->project_id = $itm['project_id'];
                        $orderdet->category_id = is_numeric($itm['category_id']) ? $itm['category_id'] : $cate;
                        $orderdet->amount = $itm['amount'];
                        $orderdet->description = $itm['description'];
                        $orderdet->save();
                }
            }

            $response['status'] = 'Success';
            $response['payment_id'] = $pay->id;
            $response['code'] = 200;
            return Response::json($response);
        // }
    }

    public function addPayment2(Request $request){
            $supplier_id = 0;
            $biller_id = 0;
            $purchaser = null;
            if(is_numeric($request->supplier_id)){
                $supplier_id = $request->supplier_id;
            }
            else if (preg_match('~[0-9]+~',  $request->supplier_id)) {
                $biller_id = preg_replace('/[^0-9]/', '', $request->supplier_id);
            }
            $con = false;
            // else{
            //     $purchaser = $request->supplier_id;
            // }

            if($request->payment_id == null){
                $ctrPay = Payment::whereDate('created_at', Carbon::today())->where('paytype',$request->paytype)->count();
                $ctr = str_pad($ctrPay+1, 3, '0', STR_PAD_LEFT); 
                $pay = new Payment;
                $pay->ref = $request->paytype.date("mdy").$ctr;
                $pay->date = $request->date;
                $pay->paytype = $request->paytype;
                $pay->type = 'Expense';
                $pay->description = $request->description;
                $pay->payout_type = $request->payout_type;
                $pay->checknum = $request->checknum;
                $pay->payout = $request->payout_total;
                $pay->payout_return = $request->payout_return;
                $pay->delivery_fee = $request->delivery_fee;
                $pay->gas = $request->gas;
                $pay->parking = $request->parking;
                $pay->toll = $request->toll;
                $pay->total = $request->totalsrp;
                $pay->bank_id = $request->bank_id;
                $pay->supplier_id = $supplier_id;
                $pay->biller_id = $biller_id;
                $pay->payment_category_id = null;
                $pay->purchase_order_id = $request->purchase_order_id;
                $pay->purchaser = $request->purchaser;
                $pay->is_active = 1;
                $pay->requested_by = Auth::User()->id;
                $pay->status = $request->status;
                if($request->completed_at == 1){
                    $pay->completed_at = date("Y-m-d H:i:s");
                }
                $pay->save();

                if($request->bank_id > 0 && $request->status == 'approved'){
                    $bank = BankAccounts::findorfail($request->bank_id);
                    $bank->balance = $bank->balance - ($request->payout_total - $request->payout_return);
                    $bank->save();

                    $trans = new TransactionLogs;
                    $trans->bank_id = $pay->bank_id;
                    $trans->payment_id = $pay->id;
                    $trans->user_id = Auth::User()->id;
                    $trans->detail = '';
                    $trans->payment = $request->payout_total- $request->payout_return;
                    $trans->balance = $bank->balance;
                    $trans->save();
                }
            }
            else{
                $pay = Payment::findOrFail($request->payment_id);
                $oldtotal = $pay->payout - $pay->payout_return;
                $pay->date = $request->date;
                $pay->type = 'Expense';
                $pay->description = $request->description;
                $pay->payout_type = $request->payout_type;
                $pay->checknum = $request->checknum;
                $pay->payout = $request->payout_total;
                $pay->payout_return = $request->payout_return;
                $pay->gas = $request->gas;
                $pay->toll = $request->toll;
                $pay->delivery_fee = $request->delivery_fee;
                $pay->parking = $request->parking;
                $pay->total = $request->totalsrp;
                $pay->bank_id = $request->bank_id;
                $pay->supplier_id = $supplier_id;
                $pay->biller_id = $biller_id;
                $pay->payment_category_id = null;
                $pay->purchase_order_id = $request->purchase_order_id;
                $pay->purchaser = $request->purchaser;
                $pay->is_active = 1;
                $pay->status = $request->status;

                if($request->status == 'approved'){
                    $pay->approved_by = Auth::User()->id;
                }

                if($request->completed_at == 1){
                    $pay->completed_at = date("Y-m-d H:i:s");
                }

                $pay->save();
                $newtotal = $request->payout_total - $request->payout_return;

                if($request->with_order > 0){
                    foreach($request->selectedmaterials as $selmat){
                        foreach($selmat['items'] as $itm){
                            $ordet = OrderDetails::findorfail($itm['id']);
                            $ordet->paynotes = $itm['paynotes'];
                            $ordet->qty = $itm['qty'];
                            $ordet->weight = $itm['weight'];
                            $ordet->length = $itm['length'];
                            $ordet->width = $itm['width'];
                            $ordet->height = $itm['height'];
                            $ordet->unit_price = $itm['unit_price'];
                            $ordet->total_price = $itm['unit_price'] * $itm['qty'];
                            $ordet->hidden = $itm['hidden'];
                            if($itm['hidden'] == 0){
                                $ordet->unit_price = 0;
                                $ordet->total_price = 0;
                            }
                            $ordet->save();
                        }
                    }
                }

                $getpaytrans = TransactionLogs::where('payment_id',$request->payment_id)->get();
                $transtotal = 0;
                foreach($getpaytrans as $g){
                    $transtotal += $g->payment;
                    $transtotal -= $g->deposit;
                }   

                if($request->bank_id > 0 && ($newtotal != $oldtotal || $transtotal == 0) && 
                    ($request->status == 'approved' || $request->status == 'completed')){
                    $totalsrp = 0;
                    if($newtotal != $oldtotal){
                        $totalsrp = $newtotal - $oldtotal;
                    }
                    if($transtotal == 0){
                        $totalsrp = $newtotal;
                    }
                    $bank = BankAccounts::findorfail($request->bank_id);
                    $bank->balance = $bank->balance - $totalsrp;
                    $bank->save();

                    $trans = new TransactionLogs;
                    $trans->bank_id = $pay->bank_id;
                    $trans->payment_id = $pay->id;
                    $trans->user_id = Auth::User()->id;
                    $trans->detail = '';
                    $trans->payment = ($totalsrp > 0 ? $totalsrp : 0);
                    $trans->deposit = ($totalsrp < 0 ? $totalsrp * -1 : 0);
                    $trans->balance = $bank->balance;
                    $trans->save();
                }

                $con = Containers::where('payment_id', $pay->id)->first();

                if(!$con){
                    $res =PaymentDetails::where('payment_id',$pay->id)->delete();
                }

                if($request->status == 'approved'){

                    if($con){
                        Containers::where('payment_id', $pay->id)
                              ->where('paid',0)
                              ->update(['paid' => 1]);
                    }

                    if($pay->purchase_order_id > 0){
                        $user = Auth::User()->first_name.' '.Auth::User()->last_name;
                        PurchaseOrderDetails::where('purchase_order_id', $pay->purchase_order_id)
                              ->where('order_status','for approval')
                              // ->where(
                              //  function($query) {
                              //    return $query
                              //           ->orwhere('order_status', 'SPC')
                              //           ->orWhere('order_status', 'POC');
                              //   })
                              ->update(['order_status' => 'to received', 'shipped_by' => $user]);
                    }
                }
                //$res =PurchaseOrderDetails::where('purchase_order_id',$pay->purchase_order_id)->delete();
            }

            if(!$con){
                foreach($request->details as $itm){
                        $cate = null;
                        if(!is_numeric($itm['category_id']) && $itm['category_id'] != null && $itm['category_id'] != ''){
                            $cat = new PaymentCategories;
                            $cat->label = $itm['category_id'];
                            $cat->save();
                            $cate = $cat->id;
                        }
                        else if($itm['category_id'] == '' || $itm['category_id'] == null){
                            $cate = null;
                        }

                        //$subtotal = ($itm['qty'] * $itm['unit_price']);
                        //$total += $subtotal;
                        $orderdet = new PaymentDetails;
                        $orderdet->payment_id = $pay->id;
                        $orderdet->project_id = $itm['project_id'];
                        $orderdet->category_id = is_numeric($itm['category_id']) ? $itm['category_id'] : $cate;
                        $orderdet->amount = $itm['amount'];
                        $orderdet->description = $itm['description'];
                        $orderdet->save();
                }
            }

            $response['status'] = 'Success';
            $response['payment_id'] = $pay->id;
            $response['code'] = 200;
            return Response::json($response);
        // }
    }

    public function getAllPurchaseOrders(Request $request){
        $status = $request->input('status');
        // return 0;
        $od = PurchaseOrderDetails::where('order_status',$status)->pluck('purchase_order_id');
        $orders = PurchaseOrder::whereIn('id',$od)->get();
        foreach ($orders as $k=>$v){
            if($v->project_id > 0){            
                $proj = Project::findorfail($v->project_id);
                $v->name = $proj->nick_name;
            }
            else{
                $v->name = 'Warehouse';
            }

            if($v->supplier_id > 0){            
                $proj = Supplier::findorfail($v->supplier_id);
                $v->supplier_name = $proj->full_name;
            }
            else{
                $v->supplier_name = '';
            }
            $od = PurchaseOrderDetails::where('purchase_order_id',$v->id)->where('order_status',$status)->get();
            $fordel = null;
            $deldate = null;
            $delby = null;
            $delat = null;
            $cardet = null;
            foreach ($od as $a=>$b){
                $fordel = $b->for_delivery;
                $deldate = $b->delivery_date;
                $delby = $b->delivered_by;
                $delat = $b->delivered_at;
                $cardet = $b->car_details;
                $v->updated_at = $b->updated_at;
                $mat = Material::findorfail($b->material_id);
                $b->material_name = $mat->name;
                // if($b->preset_id > 0){
                //$pre = Preset::findorfail($b->preset_id);
                    //$b->preset_name = $pre->name;
                //}
                //else{
                    $b->preset_name = "--";
                //}
            }
            $v->for_delivery = $fordel;
            $v->delivery_date = $deldate;
            $v->delivered_by = $delby;
            $v->delivery_by = $delat;
            $v->car_details = $cardet;
            $v->orders = $od;
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $orders;

        return Response::json($response);
    }

    public function getAllSpcOrders(Request $request){
        //jepandan
        $mode = 'SPC';

        $sort = $request->input('sort');
        $perPage  = $request->input('perPage');
        $search  = $request->input('search');


        $srt = '';
        if($sort != ''){
            $srt = explode('-' , $sort);
            $srt = $srt[0];
        }
        $list = '';
        $mids = '';

        if($search != ''){
            $list = Project::when($search != '', function ($q) use($search){
                                return $q->where('name','LIKE', '%'.$search.'%')->orwhere('nick_name','LIKE', '%'.$search.'%');
                            })->get()->pluck('id');
        }

        if($sort != ''){
            $sort = explode('-' , $sort);
            if($sort[0] == 'name') {
                    $list = Project::when($search != '', function ($q) use($search){
                                return $q->where('name','LIKE', '%'.$search.'%');
                            })->orderBy($sort[0], $sort[1])->get()->pluck('id');
            }
        }


        if($list != ''){
            $mids = "'".implode("', '", $list->toArray())."'";
        }

        // if($inv_id == 0){
    
        $od = OrderDetails::where('order_status',$mode)->where('is_receive',0)->pluck('order_id');
        $orders = Order::whereIn('id',$od)
                    ->when($list != '', function ($q) use($list) {
                                return $q->whereIn('project_id', $list);
                        })
                    ->when($list != '', function ($q) use($mids) {
                                return $q->orderByRaw(DB::raw("FIELD(project_id, $mids)"));
                        })
                    ->when($srt == 'id' || $srt == 'needed_date' || $srt == 'remarks', function ($q) use($sort) {
                            // $sort = explode('-' , $sort);
                            //if($sort[0] == 'id') {
                                return $q->orderBy($sort[0], $sort[1]);
                            //}
                        })
                    ->get();
        foreach ($orders as $k=>$v){
            $proj = Project::findorfail($v->project_id);
            $v->name = $proj->nick_name;
            $od = OrderDetails::where('order_id',$v->id)->where('order_status', $mode)->where('is_receive',0)->get();
            // \Log::info($od);
            $reqat = null;
            $reqby = null;
            $prepat = null;
            $prepby = null;
            $fordel = null;
            $deldate = null;
            $delby = null;
            $delat = null;
            $cardet = null;
            $payment_id = null;
            $ctr = 0;
            $isRcv = 0;
            foreach ($od as $a=>$b){
                $reqat = ($reqat == null ? $b->requested_at : $reqat);
                $reqby = ($reqby == null ? $b->requested_by : $reqby);
                $prepat = ($prepat == null ? $b->prepared_at : $prepat);
                $prepby = ($prepby == null ? $b->prepared_by : $prepby);
                $fordel = ($fordel == null ? $b->for_delivery : $fordel);
                $deldate = ($deldate == null ? $b->delivery_date : $deldate);
                $delby = ($delby == null ? $b->delivered_by : $delby);
                $delat = ($delat == null ? $b->delivered_at : $delat);
                $cardet = ($cardet == null ? $b->car_details : $cardet);
                $payment_id = ($payment_id == null ? $b->payment_id : $payment_id);
                $mat = Material::findorfail($b->product_id);
                $b->material_name = $mat->name;
                if($b->preset_id > 0){
                $pre = Preset::findorfail($b->preset_id);
                    $b->preset_name = $pre->name;
                }
                else{
                    $b->preset_name = "--";
                }

                if($b->is_receive == 1){
                    $isRcv++;
                }
                $ctr++;
            }
            $v->isRcvFlag = 0;
            if($ctr == $isRcv){
                $v->isRcvFlag = 1;
            }
            $v->requested_at = $reqat;
            $v->requested_by = $reqby;
            $v->prepared_at = $prepat;
            $v->prepared_by = $prepby;
            $v->for_delivery = $fordel;
            $v->delivery_date = $deldate;
            $v->delivered_by = $delby;
            $v->delivered_at = $delat;
            $v->car_details = $cardet;
            $v->payment_id = $payment_id;
            $v->orders = $od;
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $orders;

        return Response::json($response);
    }

    public function getAllSpcOrders2(Request $request){
        // // return 0;
        // $pay_ids = OrderDetails::where('order_status','SPC')->where('is_receive',0)->pluck('payment_id');
        // // $spcs = Payment::where('paytype','SPC')->where('with_order', 1)->get();
        // $spcs = Payment::whereIn('id',$pay_ids)->where('status','!=','for approval')->get();
        // foreach ($spcs as $k=>$v){
        //     $od = OrderDetails::where('payment_id',$v->id)->where('order_status','SPC')->where('is_receive',0)->get();
        //     foreach ($od as $a=>$b){
        //         //$v->updated_at = $b->updated_at;
        //         $mat = Material::findorfail($b->product_id);
        //         $b->material_name = $mat->name;
        //     }
        //     $v->orders = $od;
        // }
        // $response['status'] = 'Success';
        // $response['code'] = 200;
        // $response['data'] = $spcs;
        // return Response::json($response);
        $od = OrderDetails::where('order_status','SPC')->where('is_receive',0)->pluck('order_id');
        $orders = Order::whereIn('id',$od)->get();
        foreach ($orders as $k=>$v){
            $proj = Project::findorfail($v->project_id);
            $v->name = $proj->nick_name;
            $od = OrderDetails::where('order_id',$v->id)->where('order_status','SPC')->where('is_receive',0)->get();
            $fordel = null;
            $deldate = null;
            $delby = null;
            $delat = null;
            $cardet = null;
            $reqat = null;
            $reqby = null;
            $prepat = null;
            $prepby = null;
            $payment_id = null;
            foreach ($od as $a=>$b){
                $reqat = ($reqat == null ? $b->requested_at : $reqat);
                $reqby = ($reqby == null ? $b->requested_by : $reqby);
                $prepat = $b->prepared_at;
                $prepby = $b->prepared_by;
                $fordel = $b->for_delivery;
                $deldate = $b->delivery_date;
                $delby = $b->delivered_by;
                $delat = $b->delivered_at;
                $cardet = $b->car_details;
                $payment_id = $b->payment_id;
                $v->updated_at = $b->updated_at;
                $mat = Material::findorfail($b->product_id);
                $b->material_name = $mat->name;
                if($b->preset_id > 0){
                $pre = Preset::findorfail($b->preset_id);
                    $b->preset_name = $pre->name;
                }
                else{
                    $b->preset_name = "--";
                }
            }
            $v->for_delivery = $fordel;
            $v->delivery_date = $deldate;
            $v->delivered_by = $delby;
            $v->delivery_by = $delat;
            $v->car_details = $cardet;
            $v->orders = $od;
            $v->requested_at = $reqat;
            $v->requested_by = $reqby;
            $v->prepared_at = $prepat;
            $v->prepared_by = $prepby;
            $v->payment_id = $payment_id;
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $orders;

        return Response::json($response);
    }

    public function rcvPurchaseOrder($id) {
        // return true;
            $mat = PurchaseOrderDetails::where('purchase_order_id',$id)->get();
            foreach($mat as $m){ 
                $getMat = PurchaseOrderDetails::findorfail($m->id);  
                $getMat->order_status = 'completed';
                $getMat->received_at = date("Y-m-d H:i:s");
                $getMat->received_by = Auth::user()->first_name. ' ' .Auth::user()->last_name;
                $getMat->save();       
                $inv = Inventory::where('material_id',$m->material_id)->where('address_id', 0)->first();
                if($inv){            
                    $inv->qty = $inv->qty + $m->qty;
                    $inv->save();
                }
            }

                

            $response['status'] = 'Success';
            $response['code'] = 200;
        

        return Response::json($response);
    }

    public function rcvSpc($id) {
        // return true;
            $mat = OrderDetails::where('payment_id',$id)->get();
            foreach($mat as $m){       
                $inv = Inventory::where('material_id',$m->product_id)->where('address_id', 0)->first();
                $getMat = OrderDetails::findorfail($m->id);  
                $getMat->is_receive = 1;
                $getMat->save();       
                if($inv){            
                    $inv->qty = $inv->qty + $m->qty;
                    $inv->save();
                }
            }

            $response['status'] = 'Success';
            $response['code'] = 200;
        

        return Response::json($response);
    }

    public function rcvPoc($id) {
        // return true;
            $con = Containers::findorfail($id);
            $con->status = 'received';
            $con->save();

            $mat = OrderDetails::where('container_id',$id)->where('order_status', 'POC')->get();
            foreach($mat as $m){       
                $inv = Inventory::where('material_id',$m->product_id)->where('address_id', 0)->first();
                $getMat = OrderDetails::findorfail($m->id);  
                $getMat->is_receive = 1;
                $getMat->save();       
                if($inv){            
                    $inv->qty = $inv->qty + $m->qty;
                    $inv->save();
                }
            }

            $response['status'] = 'Success';
            $response['code'] = 200;
        

        return Response::json($response);
    }
    
}
