<?php

namespace App\Http\Controllers;

use App\Attendance;

use App\CalendarEvents;

use App\Department;

use App\Project;
use App\ProjectWorker;
use App\Worker;

use App\User;

use DB, Response, Validator;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Storage, DateTime;

class AttendanceController extends Controller
{

    public function index(Request $request) {
        $month = $request->input('month');
        $year = $request->input('year');
        $project_id = $request->input('project_id');

        $dt = $year.'-'.$month.'-01';
        $startDate = date("Y-m-01", strtotime($dt));
        $endDate = date("Y-m-t", strtotime($dt)); 
        
        $attendance = Worker::with(array('attendance' => function($query) use ($startDate, $endDate, $project_id){ 
                            $query->select('*',DB::raw("(sum(duration)) as dura"))
                                                ->where('project_id', $project_id)
                                                ->whereDate('timein', '>=', $startDate)
                                                ->whereDate('timein', '<=', $endDate)
                                                ->groupBy('user_id')
                                                ->groupBy(DB::raw("DATE_FORMAT(timein, '%d-%m-%Y')"));
                        }))
                        //->leftjoin('workers', 'project_workers.worker_id', '=', 'workers.id')
                        //->leftjoin('projects', 'project_workers.project_id', '=', 'projects.id')
                        ->select('workers.*', 'workers.id as worker_id', 'workers.first_name', 'workers.last_name', DB::raw('CONCAT(workers.last_name," ",workers.first_name) AS full_name'))
                        //->where('workers.attendance.project_id', $project_id)
                        //->orderBy('projects.id', 'asc')
                        ->orderBy('workers.last_name', 'asc')
                        ->get();

        $response['status'] = 'Success';
		$response['code'] = 200;
        $response['data'] = $attendance;

		return Response::json($response);
    }

    public function generatePayroll(Request $request) {

    }

    public function computePayroll(Request $request) {
        $month = $request->input('month');
        $year = $request->input('year');
        $project_id = $request->input('project_id');

        $dnow = date("Y-m-d");
        $target = "Thursday";
        $thurs = DateTime::createFromFormat("Y-m-d", $dnow)
        ->modify("-4 days")->modify("next $target");
        $thurs = $thurs->format("Y-m-d");
        //\Log::info(strval($date));
        //return 0;
        //$lastfri = strtotime('tuesday last week');
        $lastfri = date("Y-m-d", strtotime("friday last week"));
        $lastfri = (date('W', strtotime($lastfri)) == date('W')) ? (strtotime($lastfri)-7*86400+7200) : strtotime($lastfri);
        $lastfri = date("Y-m-d", $lastfri);
        $startDate = $lastfri;
        $endDate = $thurs; 
        
        $attendance = ProjectWorker::with(array('attendance' => function($query) use ($startDate, $endDate){ 
                            $query->select('*',DB::raw("(sum(duration)) as dura"))
                                                ->whereDate('timein', '>=', $startDate)
                                                ->whereDate('timein', '<=', $endDate)
                                                //->groupBy('day')
                                                ->groupBy('user_id')
                                                ->groupBy(DB::raw("DATE_FORMAT(timein, '%d-%m-%Y')"));
                        }))
                        ->leftjoin('workers', 'project_workers.worker_id', '=', 'workers.id')
                        ->leftjoin('projects', 'project_workers.project_id', '=', 'projects.id')
                        ->select('project_workers.*', 'workers.id as worker_id', 'workers.first_name', 'workers.last_name', 'workers.basic_rate', 'workers.load_allowance', 'workers.meal_allowance', 'workers.op_allowance', 'workers.wca', 'workers.cash_advance', DB::raw('CONCAT(workers.last_name," ",workers.first_name) AS full_name'), 'projects.nick_name as project')
                        ->where('project_workers.project_id', $project_id)
                        ->orderBy('projects.id', 'asc')
                        ->orderBy('workers.last_name', 'asc')
                        ->get();

            $minutes = 0;
        foreach($attendance as $a){
            $salary = 0;
            foreach($a->attendance as $b){
                $perhr = floatval($a->basic_rate)/8;
                //$salary = $salary +  ((floatval($b->duration)/60) * $perhr);
                $minutes = floatval($b->dura);
                //if($a->worker_id == 257){
                    // $salary = floor($minutes / 60).':'.($minutes -   floor($minutes / 60) * 60);
                    //\Log::info($b->dura);
                    $hrs = (floor($minutes / 60)) + (floor($minutes % 60)/60);
                    //\Log::info($hrs);
                    $salary +=  ($hrs * $perhr);
                    //\Log::info(floor($minutes % 60)/60);
                //}
            }
            $op = is_null($a->op_allowance) ? 0 : $a->op_allowance; 
            $meal = is_null($a->meal_allowance) ? 0 : $a->meal_allowance; 
            $load = is_null($a->load_allowance) ? 0 : $a->load_allowance; 
            $ca = is_null($a->cash_advance) ? 0 : $a->cash_advance; 
            // if($ca > 500){
            //     $ca = 500;
            // }
            $wca = is_null($a->wca) ? 0 : $a->wca; 
            // if($wca > 500){
            //     $wca = 500;
            // }
            // \Log::info($ca);
            // \Log::info($wca);
            // \Log::info($salary);
            // \Log::info($op);
            // \Log::info($meal);
            // \Log::info($load);
            $ss = $salary;
            $allowance = $op + $meal + $load;
            $deductions = $ca + $wca;
            $salary = ($salary + $op + $meal + $load) - ($ca + $wca);
            $a->amount = round($salary, 2);
            $a->category_id = 7;
            $a->description = $a->full_name.' // SALARY = '.$ss.' // allowance = '.$allowance.' // deductions = '.$deductions ;
        }

        $response['status'] = 'Success';
        $response['code'] = 200;
        $response['data'] = $attendance;
        $response['from'] = $lastfri;
        $response['to'] = $thurs;

        return Response::json($response);
    }
    
    public function testKernel () {
        $d = Carbon::now('Asia/Manila');
        $date = $d->toDateString();
        $time = $d->toTimeString();
        $dayname = $d->format('l');

        $dt = explode('-',$date);
        $day = $dt[2];
        $month = $dt[1];
        $year = $dt[0];

        $newArr = [];

        $query = Attendance::with('user')
                ->where(function($query) {
                    return $query->where('day', '=', '13')->where('month', '=', '06')->where('year', '=', '2019');
                })
                ->get();

        $user = User::with('department')->whereHas('department', function ($user) {
                    return $user->where('user_id', '!=', 'null');
                })
                ->with('department.departments')
                ->get();

        foreach($user as $u) {
            $user_id = $u->id;

            $checkAttendance = Attendance::where('day',$day)
                                ->where('month',$month)
                                ->where('year',$year)
                                ->where('user_id',$user_id)
                                ->first();

            $getSchedule = DB::table('schedules')->where('user_id', $user_id)->first();
            array_push($newArr, $getSchedule->schedule_type_id);

            if(!$checkAttendance && ($dayname=='Saturday' || $dayname=='Sunday')) { // If today is weekend
                $timein = new Attendance();
                $timein->user_id = $user_id;
                $timein->day = $day;
                $timein->month = $month;
                $timein->year = $year;
                $timein->timein_status = 'WEEKENDS';
                $timein->timeout_status = 'WEEKENDS';
                $timein->save();
            }

            $cdate = $year.'-'.$month.'-'.$day;

            // Check if there 
            $checkHoliday = CalendarEvents::where(function($query) use ($cdate) {
                                return $query->whereDate('start_date', '<=', $cdate)
                                        ->whereDate('end_date', '>=', $cdate)
                                        ->where('type', 'LIKE', '%Holiday%')
                                        ->where('approval', 'Approved');
                            })
                            ->first();

            $checkLeave = CalendarEvents::where(function($query) use ($user_id, $cdate) {
                            return $query->where('user_id', $user_id)
                                        ->whereDate('start_date', '<=', $cdate)
                                        ->whereDate('end_date', '>=', $cdate)
                                        ->where('approval', 'Approved');
                            })
                            ->first();
            
            if(!$checkAttendance && ($dayname!='Saturday' && $dayname!='Sunday')) {
                

                if($checkHoliday) { // Today is a holiday
                    $timein = new Attendance();
                    $timein->user_id = $user_id;
                    $timein->day = $day;
                    $timein->month = $month;
                    $timein->year = $year;
                    $timein->timein_status = 'HOLIDAY';
                    $timein->timeout_status = 'HOLIDAY';
                    $timein->save();
                }

                if($checkLeave && !$checkHoliday) {
                    $eventType = strtoupper(str_replace(' ', '', $checkLeave->type));
                    $timein = new Attendance();
                    $timein->user_id = $user_id;
                    $timein->day = $day;
                    $timein->month = $month;
                    $timein->year = $year;
                    $timein->timein_status = $eventType;
                    $timein->timeout_status = $eventType;
                    $timein->event_id = $checkLeave->id;
                    $timein->save();
                }

                if(!$checkHoliday && !$checkLeave) {
                    if($getSchedule->schedule_type_id == 3) {
                        $timein = new Attendance();
                        $timein->user_id = $user_id;
                        $timein->day = $day;
                        $timein->month = $month;
                        $timein->year = $year;
                        $timein->time_in = '08:00:00';
                        $timein->time_out = '17:00:00';
                        $timein->save();
                    } else {
                        $timein = new Attendance();
                        $timein->user_id = $user_id;
                        $timein->day = $day;
                        $timein->month = $month;
                        $timein->year = $year;
                        $timein->timein_status = 'ABSENT';
                        $timein->timeout_status = 'ABSENT';
                        $timein->save();
                    }
                }
            } else if($checkAttendance && ($dayname!='Saturday' && $dayname!='Sunday')) {
                if($checkHoliday) {
                    $eventType = strtoupper(str_replace(' ', '', $checkHoliday->type));
                    Attendance::where('day',$day)
                                ->where('month',$month)
                                ->where('year',$year)
                                ->where('user_id',$user_id)
                                ->update(['timein_status'=>$eventType,'timeout_status'=>$eventType,'event_id'=>$checkHoliday->id]);
                }

                if($checkLeave && !$checkHoliday) {
                    $eventType = strtoupper(str_replace(' ', '', $checkLeave->type));
                    Attendance::where('day',$day)
                                ->where('month',$month)
                                ->where('year',$year)
                                ->where('user_id',$user_id)
                                ->update(['timein_status'=>$eventType,'timeout_status'=>$eventType,'event_id'=>$checkLeave->id]);
                }

                if($checkAttendance->time_out==NULL && $getSchedule->schedule_type_id==3){
                    Attendance::where('day',$day)
                                ->where('month',$month)
                                ->where('year',$year)
                                ->where('user_id',$user_id)
                                ->update(['time_out'=>'18:00']);
                }
            }

        }
    }
}