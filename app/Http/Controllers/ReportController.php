<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ClientController;

use App\LogsAppNotification;
use App\LogsNotification;
use Edujugon\PushNotification\PushNotification;

use Edujugon\PushNotification\Messages\PushMessage;

use App\ClientService;

use App\Document;

use App\GroupUser;

use App\DocumentLog;

use App\Log;

use App\Report;

use App\ClientReport;

use App\ClientReportDocument;

use App\ClientServicePoints;

use App\Service;

use App\ServiceProcedure;

use App\OnHandDocument;

use App\SuggestedDocument;

use App\TransferredFile;

use App\User;

use App\Project;
use App\ProjectTask;
use App\Order;
use App\OrderDetails;
use App\Material;

use App\Payment;

use App\PaymentCategories;

use App\PaymentDetails;

use App\ClientTransaction;

use Auth, Carbon\Carbon, DB, Response, Validator;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;

use App\Jobs\LogsPushNotification;


use Maatwebsite\Excel\Facades\Excel;

use App\Exports\PLReportExport;
use App\Exports\MaterialReportExport;
use DateTime;


class ReportController extends Controller
{
    protected $logsAppNotification;
    protected $cModel;
    public function __construct(LogsAppNotification $logsAppNotification, LogsNotification $logsNotification)
    {
        $this->logsAppNotification = $logsAppNotification;
        $this->cModel = $logsNotification;
    }

    public function getPLReports(Request $request, $perPage = 20) {
    	$project_ids = $request->project;
    	$cat_ids = $request->category;
    	$date = $request->date;
    	$mode = $request->mode;
    	$matview = $request->matview;
    	$pricing = $request->pricing;
    	$period = $request->period;
    	$project_selected = [];
    	$cat_selected = [];
		$superoverall = 0;
    	$PEoverall = 0;
    	$OEoverall = 0;
    	// \Log::info($date);
    	if($mode == 'Project'){
	    	$projs = Project::where('id','>=',2)->get();
	    	if($project_ids != NULL){
	    		$project_ids = (explode(",",$project_ids));
	    		$projs = Project::where('id','>=',2)->whereIn('id', $project_ids)->get();
	    	}
	    	$proj_ids = $projs->pluck('id');

	    	$project_selected = [];
	    	$project_selected[0]['id'] = 0;
			$project_selected[0]['label'] = '';
			$project_selected[0]['prop'] = 'cat';
			$project_selected[0]['width'] = '280';
	    	$ctr = 1;
	    	foreach($projs as $p){
	    		$project_selected[$ctr]['id'] = $p->id;
	    		$project_selected[$ctr]['label'] = $p->nick_name;
	    		$project_selected[$ctr]['prop'] = 'p'.$p->id;
	    		$project_selected[$ctr]['width'] = '180';
	    		$ctr++;
	    	}
	    	$project_selected[$ctr]['id'] = 0;
			$project_selected[$ctr]['label'] = 'Total';
			$project_selected[$ctr]['prop'] = 'total';
			$project_selected[$ctr]['width'] = '180';

	    	$cat_selected = [];
	    	$ctr = 0;

	    	$categs = PaymentCategories::where('type','!=',null)->groupBy('type')->orderBy('type','Desc')->get();
	    	if($cat_ids != NULL){
	    		$catss = (explode(",",$cat_ids));
	    		$categs = PaymentCategories::where('type','!=',null)->whereIn('id', $catss)->groupBy('type')->orderBy('type','Desc')->get();
	    	}
	    	$overall = [];
	    	foreach($categs as $c){
	    		$cat_selected[$ctr]['label'] = $c->type;
	    		$cat_selected[$ctr]['amount'] = [];
	    		//$ctr2 = 0;
	    		$totals = [];
	    		foreach($projs as $p){
	    			$cat_selected[$ctr]['amount'][$p->id] = '';
	    			$totals[$c->id.''.$p->id] = 0;
	    		}

	    		$paycats = PaymentCategories::where('type',$c->type)->get();
		    	if($cat_ids != NULL){
		    		$cat_idss = (explode(",",$cat_ids));
		    		$paycats = PaymentCategories::where('type',$c->type)->whereIn('id', $cat_idss)->get();
		    	}

		    	$ctr++;
		    	$percat_total = 0;
		    	foreach($paycats as $pc){
		    		$cat_selected[$ctr]['label'] = $pc->label;
		    		$cat_selected[$ctr]['cat_id'] = $pc->id;
		    		$cat_selected[$ctr]['amount'] = [];
		    		$cat_selected[$ctr]['proj_id'] = [];
		    		$overall = 0;
		    		foreach($projs as $p){
		    			//$totals[$c->id.''.$p->id] = 0;
		    			$paymentIds = Payment::where('deleted_at', '!=', null)->pluck('id');
		    			$det2 = 0;	
	    				if($pc->id == 8){
	    					$payids = PaymentDetails::whereIn('payment_id', $paymentIds)->where('category_id', $pc->id)->where('project_id', $p->id)
	    								// ->where('amount',0)
	    								->pluck('payment_id');
	    					//$payttl = PaymentDetails::whereIn('payment_id', $paymentIds)->where('category_id', $pc->id)->where('project_id', $p->id)->sum('amount');
		    				//$det2 += $payttl;
	    					$det2 += Payment::whereIn('id',$payids)->sum('gas');
	    					$det2 += Payment::whereIn('id',$payids)->sum('parking');
	    					$det2 += Payment::whereIn('id',$payids)->sum('toll');
	    					$det2 += Payment::whereIn('id',$payids)->sum('delivery_fee');
	    				}

	    				if($pc->id == 4){
		    					$det2 = 0;
		    					$orderids = Order::where('project_id', $p->id)->pluck('id');
		    					if($pricing == "Unit"){
	    							$det2 += OrderDetails::whereIn('order_id', $orderids)
	    										->where('payment_id',null)
	    										->when($matview != 'All', function ($query)  {
										                return $query->where('order_status','!=', 'mob');
										        })
	    										->sum('total_price');
		    					}
		    					else{
		    						$ordet = OrderDetails::whereIn('order_id', $orderids)->where('payment_id',null)->when($matview != 'All', function ($query)  {
										                return $query->where('order_status','!=', 'mob');
										            })->get();
		    						foreach($ordet as $od){
		    							$mat = Material::findorfail($od->product_id);
		    							$det2 += ($mat->retail_price * $od->qty);
		    						}
		    					}

		    				}

	    				if($pc->id == 5){
	    					$det2 = 0;
	    					$payids = Payment::where('deleted_at', '!=', null)->where('paytype', 'SPC')->pluck('id');
	    					$orderids = Order::where('project_id', $p->id)->pluck('id');
	    					$det2 += OrderDetails::whereIn('payment_id', $payids)->whereIn('order_id', $orderids)->sum('total_price');
	    				}

	    				if($pc->id == 6){
	    					$det2 = 0;
	    					$payids = Payment::where('deleted_at', '!=', null)->where('paytype', 'POC')->withTrashed()->pluck('id');
	    					$orderids = Order::where('project_id', $p->id)->pluck('id');
	    					$det2 += OrderDetails::whereIn('payment_id', $payids)->whereIn('order_id', $orderids)->sum('total_price');
	    				}

		    			$det = PaymentDetails::whereNotIn('payment_id', $paymentIds)->where('category_id', $pc->id)->where('project_id', $p->id)->get();
		    			$amt = $det->sum('amount');
		    			// \Log::info($date);
		    			if($date != null && $date != ''){
		    				$amt = 0;
		    				$date = explode(',', $request->date);
				    		$startDate = $date[0];
				    		$endDate = isset($date[1]) ? $date[1] : null;
		    				// $paymentIds = Payment::whereBetween('created_at', [$startDate, $endDate])->pluck('id');
		    				$paymentIds = Payment::where(function($query) use($startDate, $endDate) {
									    			if( $startDate && $endDate ) {
									    				$query->whereDate('created_at', '>=', $startDate)
									    					->whereDate('created_at', '<=', $endDate);
									    			}
									    		})->where('deleted_at', null)->pluck('id');

		    				$det2 = 0;	

		    				if($pc->id == 4){
		    					$det2 = 0;
		    					$orderids = Order::where(function($query) use($startDate, $endDate) {
									    			if( $startDate && $endDate ) {
									    				$query->whereDate('created_at', '>=', $startDate)
									    					->whereDate('created_at', '<=', $endDate);
									    			}
									    		})->where('project_id', $p->id)->pluck('id');
	    						// $det2 += OrderDetails::whereIn('order_id', $orderids)->where('payment_id',null)->sum('total_price');
	    						if($pricing == "Unit"){
	    							$det2 += OrderDetails::whereIn('order_id', $orderids)->where('payment_id',null)->when($matview != 'All', function ($query)  {
										                return $query->where('order_status','!=', 'mob');
										            })->sum('total_price');
		    					}
		    					else{
		    						$ordet = OrderDetails::whereIn('order_id', $orderids)->where('payment_id',null)->when($matview != 'All', function ($query)  {
										                return $query->where('order_status','!=', 'mob');
										            })->get();
		    						foreach($ordet as $od){
		    							$mat = Material::findorfail($od->product_id);
		    							$det2 += ($mat->retail_price * $od->qty);
		    						}
		    					}

		    				}
		    				

		    				if($pc->id == 5){
		    					$det2 = 0;
		    					$payids = Payment::where(function($query) use($startDate, $endDate) {
									    			if( $startDate && $endDate ) {
									    				$query->whereDate('created_at', '>=', $startDate)
									    					->whereDate('created_at', '<=', $endDate);
									    			}
									    		})->where('paytype', 'SPC')->where('deleted_at', null)->pluck('id');
		    					$orderids = Order::where('project_id', $p->id)->pluck('id');
	    						$det2 += OrderDetails::whereIn('payment_id', $payids)->whereIn('order_id', $orderids)->sum('total_price');

		    				}

		    				if($pc->id == 6){
		    					$det2 = 0;
		    					$payids = Payment::where(function($query) use($startDate, $endDate) {
									    			if( $startDate && $endDate ) {
									    				$query->whereDate('created_at', '>=', $startDate)
									    					->whereDate('created_at', '<=', $endDate);
									    			}
									    		})->where('paytype', 'POC')->where('deleted_at', null)->pluck('id');
		    					$orderids = Order::where('project_id', $p->id)->pluck('id');
	    						$det2 += OrderDetails::whereIn('payment_id', $payids)->whereIn('order_id', $orderids)->sum('total_price');

		    				}

		    				if($pc->id == 8){
		    					$det2 =0;
		    					$payids = PaymentDetails::whereIn('payment_id', $paymentIds)->where('category_id', $pc->id)->where('project_id', $p->id)
		    							// ->where('amount',0)
		    							->pluck('payment_id');

		    					$det2 += Payment::whereIn('id',$payids)->sum('gas');
		    					$det2 += Payment::whereIn('id',$payids)->sum('parking');
		    					$det2 += Payment::whereIn('id',$payids)->sum('toll');
		    					$det2 += Payment::whereIn('id',$payids)->sum('delivery_fee');
		    					// jeff
		    					//\Log::info("Proj: ". $p->id );
		    					//\Log::info($payids);
		    				}
		    				//else{
			    			    $det = PaymentDetails::whereIn('payment_id', $paymentIds)->where('category_id', $pc->id)->where('project_id', $p->id)->get();
			    			    $amt = $det->sum('amount');
			    			    if($pc->id == 8){
			    			    	\Log::info($amt);
			    			    	$g = $amt + $det2;
			    			    	\Log::info("total :" . $g);
			    			    }

		    				//}
		    			    $amt += $det2;

		    			    if($pc->id == 8){
		    			    	$g = $amt + $overall;
			    			    	\Log::info("overall :" . $g);
			    			    }

		    			}

		    			$totals[$c->id.''.$p->id] += $amt;
		    			$overall += $amt;
		    			//$overall[$c->id] += $amt;
		    			$cat_selected[$ctr]['amount'][$p->id] = $amt;
		    			$cat_selected[$ctr]['proj_id'][$p->id] = $p->id;
		    		}
		    		$cat_selected[$ctr]['rowtotal'] = $overall;
		    		$superoverall += $overall;
		    		if($c->type == "Project Direct Expenses"){
		    			$PEoverall += $overall;
		    		}
		    		if($c->type == "Other Expenses"){
		    			$OEoverall += $overall;
		    		}
		    		$ctr++;
		    	}

		    	$cat_selected[$ctr]['label'] = 'Total '.$c->type;
	    		$cat_selected[$ctr]['amount'] = [];
	    		foreach($paycats as $pc){
	    			$overall2 = 0;
		    		foreach($projs as $p){
		    			$cat_selected[$ctr]['amount'][$p->id] = $totals[$c->id.''.$p->id];
		    			$overall2 +=  $totals[$c->id.''.$p->id];
		    		}
		    		$cat_selected[$ctr]['rowtotal'] = $overall2;
		    	}

		    	$ctr++;
		    	if($c->type == 'Other Expenses'){	    		
			    	$cat_selected[$ctr]['label'] = 'OVERALL TOTAL';
			    	$cat_selected[$ctr]['amount'][$p->id] = null;
			    	$cat_selected[$ctr]['rowtotal'] = $superoverall;
			    	$ctr++;
		    	}
	    	}
	    	
    	}

    	else{
    		$proj_ids = [];
    		$projs = Project::where('id','>=',2)->get();
	    	if($project_ids != NULL){
	    		$project_ids = (explode(",",$project_ids));
	    		$projs = Project::where('id','>=',2)->whereIn('id', $project_ids)->get();
	    	}
	    	$proj_ids = $projs->pluck('id');

	    	//\Log::info($proj_ids);

	    	$project_selected = [];
	    	$project_selected[0]['id'] = 0;
			$project_selected[0]['label'] = '';
			$project_selected[0]['prop'] = 'cat';
			$project_selected[0]['width'] = '280';
	    	$ctr = 1;
	    	$colval = [];
	    	
	    	if($date != null && $date != ''){
	    		$date = explode(',', $request->date);
	    		$StartDate = @strtotime($date[0]);
        		$StopDate = @strtotime($date[1]);
		    	if($period == "All Dates" || $period == "This Year" || $period == "This Quarter"){
		    		$cols = $this->getMonthsBetweenTwoDates($StartDate, $StopDate);
		    		$ctr = 1;
		    		foreach($cols as $c){
			    		$project_selected[$ctr]['id'] = $ctr;
			    		$project_selected[$ctr]['label'] = date("M Y", $c);
			    		$project_selected[$ctr]['prop'] = 'p'.$ctr;
			    		$project_selected[$ctr]['width'] = '180';
			    		$colval[$ctr-1]['id'] = $ctr; 
			    		$colval[$ctr-1]['type'] = 'monthly'; 
			    		$colval[$ctr-1]['date'] = date("Y-m-d", $c); 
			    		$ctr++;
		    		}
		    	}
		    	else if($period == "This Month"){
		    		$cols = $this->getWeeksBetweenTwoDates($date[0], $date[1]);
		    		$ctr = 1;
		    		foreach($cols as $c){
		    			$d1 = date('M d', strtotime($c[0]));
		    			$d2 = date('M d', strtotime($c[1]));
			    		$project_selected[$ctr]['id'] = $ctr;
			    		$project_selected[$ctr]['label'] = $d1 . ' - ' . $d2;
			    		$project_selected[$ctr]['prop'] = 'p'.$ctr;
			    		$project_selected[$ctr]['width'] = '180';
			    		$colval[$ctr-1]['id'] = $ctr; 
			    		$colval[$ctr-1]['type'] = 'weekly'; 
			    		$colval[$ctr-1]['date'] = $c; 
			    		$ctr++;
		    		}
		    	}
		    	else if($period == "This Week" || $period == "This Week-to-date"){
		    		
		    		$cols = CarbonPeriod::create($date[0], $date[1]);
		    		$ctr = 1;
		    		foreach($cols as $c){
		    			$d1 = $c->format('M d, Y');
			    		$project_selected[$ctr]['id'] = $ctr;
			    		$project_selected[$ctr]['label'] = $d1 ;
			    		$project_selected[$ctr]['prop'] = 'p'.$ctr;
			    		$project_selected[$ctr]['width'] = '180';
			    		$colval[$ctr-1]['id'] = $ctr; 
			    		$colval[$ctr-1]['type'] = 'daily'; 
			    		$colval[$ctr-1]['date'] = $c; 
			    		$ctr++;
		    		}
		    	}
		    	else if($period == "Today"){
		    	    //$cols = [];
		    		$cols = array("Today");
		    		$ctr = 1;
		    		$date = explode(',', $request->date);
		    		//\Log::info('test');
		    		//\Log::info($date[0]);
		    		//foreach($cols as $c){
		    			$d1 = date('M d, Y', strtotime($date[0]));
			    		$project_selected[$ctr]['id'] = $ctr;
			    		$project_selected[$ctr]['label'] = $d1;
			    		$project_selected[$ctr]['prop'] = 'p'.$ctr;
			    		$project_selected[$ctr]['width'] = '180';
			    		$colval[$ctr-1]['id'] = $ctr; 
			    		$colval[$ctr-1]['type'] = 'daily'; 
			    		$colval[$ctr-1]['date'] = $date[0]; 
			    		$ctr++;
		    		//}
		    	}
		    	else if($period == "This Month-to-date"){
		    		$cols = $this->getWeeksBetweenTwoDates($date[0], $date[1]);
		    		$ctr = 1;
		    		foreach($cols as $c){
		    			$d1 = date('M d', strtotime($c[0]));
		    			$d2 = date('M d', strtotime($c[1]));
			    		$project_selected[$ctr]['id'] = $ctr;
			    		$project_selected[$ctr]['label'] = $d1 . ' - ' . $d2;
			    		$project_selected[$ctr]['prop'] = 'p'.$ctr;
			    		$project_selected[$ctr]['width'] = '180';
			    		$colval[$ctr-1]['id'] = $ctr; 
			    		$colval[$ctr-1]['type'] = 'weekly'; 
			    		$colval[$ctr-1]['date'] = $c; 
			    		$ctr++;
		    		}
		    	}
		    	else if($period == "This Quarter-to-date" || $period == "This Year-to-date"){
		    		$cols = $this->getMonthsBetweenTwoDates($StartDate, $StopDate);
		    		$ctr = 1;
		    		$arrayCount = count($cols);
		    		foreach($cols as $c){
		    			$label = date("M Y", $c);
		    			if($ctr == $arrayCount){
		    				$label = date("M Y", $c) . ' - today';
		    			}
			    		$project_selected[$ctr]['id'] = $ctr;
			    		$project_selected[$ctr]['label'] = $label;
			    		$project_selected[$ctr]['prop'] = 'p'.$ctr;
			    		$project_selected[$ctr]['width'] = '180';
			    		$colval[$ctr-1]['id'] = $ctr; 
			    		$colval[$ctr-1]['type'] = 'monthly'; 
			    		$colval[$ctr-1]['date'] = date("Y-m-d", $c); 
			    		$ctr++;
		    		}
		    	}
		    }

	    	$project_selected[$ctr]['id'] = 0;
			$project_selected[$ctr]['label'] = 'Total';
			$project_selected[$ctr]['prop'] = 'total';
			$project_selected[$ctr]['width'] = '180';

			$cat_selected = [];
	    	$ctr = 0;
	    	$categs = PaymentCategories::where('type','!=',null)->groupBy('type')->orderBy('type','Desc')->get();
	    	if($cat_ids != NULL){
	    		$catss = (explode(",",$cat_ids));
	    		$categs = PaymentCategories::where('type','!=',null)->whereIn('id', $catss)->groupBy('type')->orderBy('type','Desc')->get();
	    	}
	    	$overall = [];
	    	foreach($categs as $c){
	    		$cat_selected[$ctr]['label'] = $c->type;
	    		$cat_selected[$ctr]['amount'] = [];
	    		//$ctr2 = 0;
	    		$totals = [];
	    		foreach($colval as $p){
	    			$cat_selected[$ctr]['amount'][$p['id']] = '';
	    			$totals[$c->id.''.$p['id']] = 0;
	    		}

	    		$paycats = PaymentCategories::where('type',$c->type)->get();
		    	if($cat_ids != NULL){
		    		$cat_idss = (explode(",",$cat_ids));
		    		$paycats = PaymentCategories::where('type',$c->type)->whereIn('id', $cat_idss)->get();
		    	}

		    	$ctr++;
		    	$percat_total = 0;
		    	foreach($paycats as $pc){
		    		$cat_selected[$ctr]['label'] = $pc->label;
		    		$cat_selected[$ctr]['cat_id'] = $pc->id;
		    		$cat_selected[$ctr]['amount'] = [];
		    		$cat_selected[$ctr]['proj_id'] = [];
		    		$overall = 0;
		    		foreach($colval as $p){

		    			if($p['type'] == 'weekly'){
		    				$date = $p['date'];
				    		$startDate = $date[0];
				    		$endDate = $date[1]; 

		    			}
		    			else if($p['type'] == 'monthly'){
		    				$date = $p['date'];
		    				$startDate = date("Y-m-01", strtotime($date));
		    				$endDate = date("Y-m-t", strtotime($date));

		    			}
		    			else if($p['type'] == 'daily'){
		    				$date = $p['date'];
				    		$startDate = $p['date'];
				    		$endDate = $p['date']; 

		    			}

		    			$paymentIds = Payment::where(function($query) use($startDate, $endDate) {
									    			if( $startDate && $endDate ) {
									    				$query->whereDate('created_at', '>=', $startDate)
									    					->whereDate('created_at', '<=', $endDate);
									    			}
									    		})->where('deleted_at', null)->pluck('id');
		    				 // \Log::info($paymentIds);
		    			$det = PaymentDetails::whereIn('payment_id', $paymentIds)->where('category_id', $pc->id)
		    					->whereIn('project_id', $proj_ids)
		    			    	// ->when($project_ids != NULL, function ($query) use($proj_ids) {
					         //            return $query->whereIn('project_id', $proj_ids);
					         //    })
		    			    	->get();
		    			    $amt = $det->sum('amount');
		    			$det3 = 0;
		    			if($pc->id == 4){
							$det3 = 0;
							$orderids = Order::where(function($query) use($startDate, $endDate) {
									    			if( $startDate && $endDate ) {
									    				$query->whereDate('created_at', '>=', $startDate)
									    					->whereDate('created_at', '<=', $endDate);
									    			}

									    		})->whereIn('project_id', $proj_ids)->pluck('id');
	    						// $det3 += OrderDetails::where('payment_id', null)
	    						// 			->whereIn('order_id', $orderids)
	    						// 			->sum('total_price');
	    					if($pricing == "Unit"){
    							$det3 += OrderDetails::whereIn('order_id', $orderids)
    										->when($matview != 'All', function ($query)  {
										                return $query->where('order_status','!=', 'mob');
										            })->where('payment_id',null)->sum('total_price');
	    					}
	    					else{
	    						$ordet = OrderDetails::whereIn('order_id', $orderids)->where('payment_id',null)			->when($matview != 'All', function ($query)  {
										                return $query->where('order_status','!=', 'mob');
										            })			
	    									->get();
	    						foreach($ordet as $od){
	    							$mat = Material::findorfail($od->product_id);
	    							$det3 += ($mat->retail_price * $od->qty);
	    						}
	    					}
		    			}

		    			if($pc->id == 5){
		    					$det3 = 0;
		    					$payids = Payment::where(function($query) use($startDate, $endDate) {
									    			if( $startDate && $endDate ) {
									    				$query->whereDate('created_at', '>=', $startDate)
									    					->whereDate('created_at', '<=', $endDate);
									    			}

									    		})->where('paytype', 'SPC')->where('deleted_at', null)->pluck('id');
		    					$orderids = Order::whereIn('project_id', $proj_ids)->pluck('id');
	    						$det3 += OrderDetails::whereIn('payment_id', $payids)
	    									->whereIn('order_id', $orderids)
	    									->sum('total_price');

		    				}

	    				if($pc->id == 6){
	    					$det3 = 0;
	    					$payids = Payment::where(function($query) use($startDate, $endDate) {
								    			if( $startDate && $endDate ) {
								    				$query->whereDate('created_at', '>=', $startDate)
								    					->whereDate('created_at', '<=', $endDate);
								    			}
								    		})->where('paytype', 'POC')->where('deleted_at', null)->pluck('id');
	    					$orderids = Order::whereIn('project_id', $proj_ids)->pluck('id');
    						$det3 += OrderDetails::whereIn('payment_id', $payids)
    										->whereIn('order_id', $orderids)
    										->sum('total_price');

	    				}

		    				if($pc->id == 8){
		    					$det3 = 0;
		    					$paymentIds = Payment::where(function($query) use($startDate, $endDate) {
								    			if( $startDate && $endDate ) {
								    				$query->whereDate('created_at', '>=', $startDate)
								    					->whereDate('created_at', '<=', $endDate);
								    			}
								    		})->where('deleted_at', null)->pluck('id');
		    					//\Log::info($endDate);
		    					$payids = PaymentDetails::whereIn('payment_id', $paymentIds)->where('category_id', $pc->id)
		    								->whereIn('project_id', $proj_ids)
		    								->pluck('payment_id');
		    					$paydets = PaymentDetails::whereIn('payment_id', $paymentIds)->where('category_id', $pc->id)
		    								->whereIn('project_id', $proj_ids)
		    								->get();
		    					//$amttl = PaymentDetails::whereIn('payment_id', $paymentIds)->where('category_id', $pc->id)
		    								//->whereIn('project_id', $proj_ids)
		    								//->sum('amount');
		    					//$det3 += $amttl;
		    								// jeff
		    					//\Log::info($payids);
		    					$current = [];
		    					foreach($paydets as $pd){
		    					// 	// \Log::info($pd->payment_id);
		    					// 	$ppay = Payment::where('id', $pd->payment_id)->first();
		    					// 	$det3 = floatval($det3) + floatval($ppay->gas) + floatval($ppay->toll) + floatval($ppay->parking) + floatval($ppay->delivery_fee);
		    					// }
		    					
		    					// \Log::info($paymentIds);
		    					//\Log::info($payids);
		    						$cur = 	 $pd->payment_id.$pd->project_id.$pd->category_id;
									if(!in_array($cur, $current)){	
			    						$current[] = $cur;  		
				    					$det3 += Payment::where('id',$pd->payment_id)->sum('gas');
				    					$det3 += Payment::where('id',$pd->payment_id)->sum('parking');
				    					$det3 += Payment::where('id',$pd->payment_id)->sum('toll');
				    					$det3 += Payment::where('id',$pd->payment_id)->sum('delivery_fee');
			    					}
			    				}
		    				}

		    			$totals[$c->id.''.$p['id']] += ($amt + $det3);
		    			$overall += ($amt + $det3);
		    			//$overall[$c->id] += $amt;
		    			$cat_selected[$ctr]['amount'][$p['id']] = $amt + $det3;
		    			$cat_selected[$ctr]['proj_id'][$p['id']] = $p['id'];
		    		}
		    		$cat_selected[$ctr]['rowtotal'] = $overall;

		    		$superoverall += $overall;
		    		if($c->type == "Project Direct Expenses"){
		    			$PEoverall += $overall;
		    		}
		    		if($c->type == "Other Expenses"){
		    			$OEoverall += $overall;
		    		}

		    		$ctr++;
		    	}

		    	$cat_selected[$ctr]['label'] = 'Total '.$c->type;
	    		$cat_selected[$ctr]['amount'] = [];
	    		foreach($paycats as $pc){
	    			$overall2 = 0;
		    		foreach($colval as $p){
		    			$cat_selected[$ctr]['amount'][$p['id']] = $totals[$c->id.''.$p['id']];
		    			$overall2 +=  $totals[$c->id.''.$p['id']];
		    		}
		    		$cat_selected[$ctr]['rowtotal'] = $overall2;
		    	}

		    	$ctr++;
		    	if($c->type == 'Other Expenses'){	    		
			    	$cat_selected[$ctr]['label'] = 'OVERALL TOTAL';
			    	$cat_selected[$ctr]['amount'][$p['id']] = null;
			    	$cat_selected[$ctr]['rowtotal'] = $superoverall;
			    	$ctr++;
		    	}
		    	// $cat_selected[$ctr]['amount'][$p->id] = $totals[$c->id.''.$p->id];
		    	// $ctr++;
	    	}
    	}



    	$response['status'] = 'Success';
		$response['data'] = [
		    'data' => $cat_selected,
		    'projects' => $project_selected,
		    'PEoverall' => $PEoverall,
		    'OEoverall' => $OEoverall,
		    'superoverall' => $superoverall,
		];
		$response['code'] = 200;

		return Response::json($response);

    }

    public function getMonthsBetweenTwoDates( $start, $end ){

        $current = $start;
        $ret = array($start);

        $my     = date('mY', $end);
        // \Log::info($current);

        while( $current<$end ){
            
            $next = @date('Y-M-01', $current) . "+1 month";
            $current = @strtotime($next);
            if(date('mY', $current) <= $my && ($current<$end))
            $ret[] = $current;
        }

        return $ret;
        // return array_reverse($ret);
    }

    public static function getWeeksBetweenTwoDates($startDate, $endDate)
	{
		$weeks = [];
		while (strtotime($startDate) <= strtotime($endDate)) {
			$oldStartDate = $startDate;
			$startDate = date('Y-m-d', strtotime('+7 day', strtotime($startDate)));
			if (strtotime($startDate) > strtotime($endDate)) {
				$week = [$oldStartDate, $endDate];
			}
			else {
				$week = [$oldStartDate, date('Y-m-d', strtotime('-1 day', strtotime($startDate))) ];
			}

			$weeks[] = $week;
		}

		return $weeks;
	}

    public function getMaterialReports(Request $request, $perPage = 20) {

    	// return 1;
    	$project = $request->project;
    	// \Log::info($date);
    	$data = [];

    	if($project != null && $project != ''){
    		$tasks = ProjectTask::where('project_id', $project)->pluck('id');
	    	$orders = Order::where('project_id', $project)->whereIn('task_id', $tasks)->pluck('id');

	    	$data = OrderDetails::whereIn('order_id', $orders)->orderBy('order_id','Desc')->get();

	    	foreach($data as $d){
	    		$mat = Material::where('id', $d->product_id)->first();
	    		$or = Order::where('id', $d->order_id)->first();
	    		$pt = ProjectTask::where('id', $or->task_id)->first();
	    		$d->category = $mat->type;
	    		$d->unit = $mat->unit;
	    		$d->unit_price = $mat->unit_price;
	    		$d->srp = $mat->retail_price;
	    		$d->matname = $mat->name.' ('.$mat->name_cn.')';
	    		$d->taskname = $pt->label;
	    	}
    	}


    	$response['status'] = 'Success';
		$response['data'] = [
		    'data' => $data
		];
		$response['code'] = 200;

		return Response::json($response);

    }

    public function getPLDetails(Request $request){
    	$date = $request->date;
    	$cat_id = $request->cat_id;
    	$proj_id = $request->proj_id;
    	$project = $request->project;
    	$mode = $request->mode;
    	$project_ids = null;
    	if($project != null){
    		$project_ids = (explode(",",$project));
    	}
    	$paymentIds = Payment::where('deleted_at', '!=', null)->withTrashed()->pluck('id');
    	$details = PaymentDetails::where('category_id', $cat_id)->whereNotIn('payment_id', $paymentIds)->where('project_id', $proj_id)->get();
		$total = $details->sum('amount');

		if($date != null && $date != ''){
			//$date = explode(',', $request->date);
    		$startDate = $date[0];
    		$endDate = $date[1];
			// $paymentIds = Payment::whereBetween('created_at', [$startDate, $endDate])->pluck('id');
			$paymentIds = Payment::where(function($query) use($startDate, $endDate) {
					    			if( $startDate && $endDate ) {
					    				$query->whereDate('created_at', '>=', $startDate)
					    					->whereDate('created_at', '<=', $endDate);
					    			}
					    		})->where('deleted_at', null)->withTrashed()->pluck('id');
			if($mode == 'Project'){
		    	$details = PaymentDetails::whereIn('payment_id', $paymentIds)->where('category_id', $cat_id)->where('project_id', $proj_id)->groupBy('payment_id')->get();
			}
			else{
				//\Log::info($project_ids != null);
				$details = PaymentDetails::whereIn('payment_id', $paymentIds)
								->when($project_ids != null, function ($query) use($project_ids) {
					                return $query->whereIn('project_id', $project_ids);
					            })
								->where('category_id', $cat_id)
								->groupBy('payment_id')->get();
				//\Log::info($details);
			}
		    $total = $details->sum('amount');
		}

		foreach($details as $det){
			$payment = Payment::where('id', $det->payment_id)->first();
			if($det->category_id == 8){
				$det->amount = $det->amount + $payment->gas + $payment->toll + $payment->delivery_fee + $payment->parking;

			}
			$det->ref = '';
			$det->created_at = '';
			if($payment){			
				$det->ref = $payment->ref;
				$det->created_at = $payment->created_at;
			}
		}

    	$response['status'] = 'Success';
		$response['data'] = [
		    'details' => $details,
		    'total' => $total
		];
		$response['code'] = 200;

		return Response::json($response);
    }

    public function getPLDetails2(Request $request){
    	$date = $request->date;
    	$cat_id = $request->cat_id;
    	$proj_id = $request->proj_id;
    	$project_ids = $request->project;
    	$mode = $request->mode;
    	$matview = $request->matview;
    	$pricing = $request->pricing;

    	$projs = Project::where('id','>=',2)->get();
    	if($project_ids != NULL && $project_ids != ''){
    		$project_ids = (explode(",",$project_ids));
    		$projs = Project::where('id','>=',2)->whereIn('id', $project_ids)->get();
    	}
    	$proj_ids = $projs->pluck('id');

    	if($cat_id == 4){
	    	if($mode == 'Project'){
		    	$orderids = Order::where('project_id', $proj_id)->pluck('id');
		    	if($pricing == 'Unit'){
				    $details = OrderDetails::whereIn('order_id', $orderids)
				    				->when($matview != 'All', function ($query)  {
						                return $query->where('order_status','!=', 'mob');
						            })
						            ->where('payment_id',null)
				    				->get();
				    $total = $details->sum('total_price');
		    	}
		    	else{
		    		$details = OrderDetails::whereIn('order_id', $orderids)
				    				->when($matview != 'All', function ($query)  {
						                return $query->where('order_status','!=', 'mob');
						            })
						            ->where('payment_id',null)
				    				->get();
		    		foreach($details as $od){
						$mat = Material::findorfail($od->product_id);
						$total += ($mat->retail_price * $od->qty);
					}
		    	}
			}
			else{
				if($pricing == 'Unit'){
					$details = OrderDetails::when($matview != 'All', function ($query)  {
					                return $query->where('order_status','!=', 'mob');
					            })
								->where('payment_id',null)
								->get();
					$total = $details->sum('total_price');
				}
				else{
					$details = OrderDetails::when($matview != 'All', function ($query)  {
					                return $query->where('order_status','!=', 'mob');
					            })
								->where('payment_id',null)
								->get();
					foreach($details as $od){
						$mat = Material::findorfail($od->product_id);
						$total += ($mat->retail_price * $od->qty);
					}
				}
			}
			// \Log::info($date);
			if($date != null && $date != ''){
	    		$startDate = $date[0];
	    		$endDate = $date[1];
				if($mode == 'Project'){
			    	$orderids = Order::whereIn('project_id', $proj_ids)
			    					->where(function($query) use($startDate, $endDate) {
						    			if( $startDate && $endDate ) {
						    				$query->whereDate('created_at', '>=', $startDate)
						    					->whereDate('created_at', '<=', $endDate);
						    			}
						    		})->pluck('id');
				    $details = OrderDetails::whereIn('order_id', $orderids)
				    				->when($matview != 'All', function ($query)  {
						                return $query->where('order_status','!=', 'mob');
						            })
						            ->where('payment_id',null)
				    				->get();
				}
				else{
					$orderids = Order::whereIn('project_id', $proj_ids)
			    					->where(function($query) use($startDate, $endDate) {
						    			if( $startDate && $endDate ) {
						    				$query->whereDate('created_at', '>=', $startDate)
						    					->whereDate('created_at', '<=', $endDate);
						    			}
						    		})->pluck('id');
					$details = OrderDetails::whereIn('order_id', $orderids)
									->when($matview != 'All', function ($query)  {
						                return $query->where('order_status','!=', 'mob');
						            })
						    		->where('payment_id',null)
									->get();
				}
				if($pricing == 'Unit'){
			    	$total = $details->sum('total_price');
				}
				else{
					foreach($details as $od){
						$mat = Material::findorfail($od->product_id);
						$total += ($mat->retail_price * $od->qty);
					}
				}
			}

			foreach($details as $det){
		
					$mat = Material::findorfail($det->product_id);
					$det->ref = "";
					$det->description = "PO#".$det->id." - ".$det->order_status;
					$det->amount = $det->total_price;
					if($pricing != 'Unit'){
						$det->amount = ($mat->retail_price * $det->qty);
					}
				
			}
    	}
    	else{
	    	$paytype= $cat_id == 5 ? 'SPC' : 'POC';
	    	$paymentIds = Payment::where('paytype', $paytype)->where('deleted_at', '!=', null)->withTrashed()->pluck('id');
	    	if($mode == 'Project'){
		    	$orderids = Order::where('project_id', $proj_id)->pluck('id');
			    $details = OrderDetails::whereIn('payment_id', $paymentIds)->whereIn('order_id', $orderids)
			    				->when($matview != 'All', function ($query)  {
					                return $query->where('order_status','!=', 'mob');
					            })
			    				->get();
			    $total = $details->sum('total_price');
			}
			else{
				$details = OrderDetails::whereIn('payment_id', $paymentIds)
								->when($matview != 'All', function ($query)  {
					                return $query->where('order_status','!=', 'mob');
					            })
								->get();
				$total = $details->sum('total_price');
			}

			if($date != null && $date != ''){
				//$date = explode(',', $request->date);
	    		$startDate = $date[0];
	    		$endDate = $date[1];
				// $paymentIds = Payment::whereBetween('created_at', [$startDate, $endDate])->pluck('id');
				$paymentIds = Payment::where(function($query) use($startDate, $endDate) {
						    			if( $startDate && $endDate ) {
						    				$query->whereDate('created_at', '>=', $startDate)
						    					->whereDate('created_at', '<=', $endDate);
						    			}
						    		})->where('paytype', $paytype)->where('deleted_at', null)->withTrashed()->pluck('id');
				if($mode == 'Project'){
			    	$orderids = Order::where('project_id', $proj_id)->pluck('id');
				    $details = OrderDetails::whereIn('payment_id', $paymentIds)->whereIn('order_id', $orderids)
				    				->when($matview != 'All', function ($query)  {
						                return $query->where('order_status','!=', 'mob');
						            })
				    				->groupBy('payment_id')->get();
				}
				else{
					$details = OrderDetails::whereIn('payment_id', $paymentIds)
									->when($matview != 'All', function ($query)  {
						                return $query->where('order_status','!=', 'mob');
						            })
									->groupBy('payment_id')->get();
				}
			    $total = $details->sum('total_price');
			}
			foreach($details as $det){
				$payment = Payment::where('id', $det->payment_id)->first();
				$det->ref = '';
				$det->created_at = '';
				$det->description = '';
				$det->amount = '';
				if($payment){			
					$det->ref = $payment->ref;
					$det->description = "PO#".$det->id;
					$det->amount = $payment->total;
					$det->created_at = $payment->created_at;
				}
			}
		}


    	$response['status'] = 'Success';
		$response['data'] = [
		    'details' => $details,
		    'total' => $total
		];
		$response['code'] = 200;

		return Response::json($response);
    }

    public function exportPLReport(Request $request) 
    {

        $export = new PLReportExport($request);
        //\Log::info($request);
        return Excel::download($export, 'pl-reports.xls');
    }

    public function exportMaterialReport(Request $request) 
    {

        $export = new MaterialReportExport($request);
        //\Log::info($request);
        return Excel::download($export, 'material-reports.xls');
    }


	public function ifDocsExist($array, $data) {
		$counter = 0;

		if(count($array) > 0) {
			foreach($array as $arr) {
				if($arr['id'] === $data) {
					$counter++;
				}
			}
		}

		if($counter > 0) {
			return true;
		}

		return false;
	}


	public function getDocIndex($array, $data) {
		$arrIndex = 0;

		if(count($array) > 0) {
			foreach($array as $i => $arr) {
				if($arr['id'] === $data) {
					$arrIndex = $i;
				}
			}
		}

		return $arrIndex;
  }


  public function sendPushNotification($user_id, $message = null, $_data=[], $label = null, $log_id = null) {
      // save to logs_app_notification
      $data = [];
      if(!empty($_data)){
          $_data['client_id'] = $user_id;
          $_data['message'] = $message;
          $_data['message_cn'] = "";

          $data = $this->logsAppNotification->saveToDb($_data);
      }

//      $title = "";
//      if($_data['type'] == "Documents Received" || $_data['type'] == "Documents Needed"){
//          $title = $_data['type']. ":". PHP_EOL;
//      }

      if($data) {
          if ($label !== null) {
              //$job = (new LogsPushNotification($user_id, $message, $data->id))->delay(now()->addMinutes(120));
              $job = (new LogsPushNotification($user_id, $message, $data->id))->delay(now()->addMinutes(10));
          } else {
              $job = (new LogsPushNotification($user_id, $message, $data->id));
          }

          $jobId = $this->dispatch($job);


          if ($label !== null && $log_id !== null) {
              $this->cModel->saveToDb(
                  [
                      "log_id" => $log_id,
                      "job_id" => $jobId
                  ]
              );

//              $checkLogNotif = DB::table('logs_notification as ln')
//                  ->leftJoin('jobs', 'ln.job_id', '=', 'jobs.id')
//                  ->where('ln.log_id', $log_id)
//                  ->where('status', 1)
//                  ->first();
//
//              if ($checkLogNotif) {
//                  DB::table('jobs')->where('id', $checkLogNotif->job_id)->delete();
//                  DB::table('logs_notification')
//                      ->where('log_id', $log_id)
//                      ->where('job_id', $checkLogNotif->job_id)
//                      ->update([
//                          'status' => 0
//                      ]);
//              }
//
//              DB::table('logs_notification')->insert([
//                  'log_id' => $log_id,
//                  'job_id' => $jobID,
//                  'status' => 1
//              ]);
          }
      }
	}


	public function getClientReports($id) {
		$cr = ClientReport::where('client_service_id', $id)->select('service_procedure_id')->get();

		$response['data'] = $cr;
		return Response::json($response);
	}


	public function sendNotif() {
		$push = new PushNotification('fcm');
    $push->setUrl('https://fcm.googleapis.com/fcm/send')
    ->setMessage([
      'notification' => [
        'title'=>'Test Title',
        'body' => 'Web push notification test',
        'sound' => 'default'
      ]
    ])
    ->setConfig(['dry_run' => false,'priority' => 'high'])
    ->setApiKey('AAAAIynhqO8:APA91bH5P-SGimP4b0jazCrC8ya7bV9LoR57wWB9zLqatXfRyxSIdKs2_q4-e01Ofce6oxW-7YQOGlk4Sov4WwiUAE7qojRu-3xb9429ve0Ufkh4JDMaod7cKBAxbypFUPJNKX0yoe98')
    ->setDevicesToken('eYCA9ybQBak:APA91bGeormdqS1wOYJ9lrGv9DhBH9HN2VUpB-0odTjr51pwdZo_bWBRrUS8oMtQ7A2mXQV46U-Ib55CtvjT7Ff-opzvsuqrPKEif8DDWRRZYUh7g5nG5yfY8Dk64WeulGla-w9Y7q3y')
    ->send();
	}
}
