<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'orders';

    protected $fillable = ['user_id','project_id', 'task_id', 'supplier_id', 'category_id', 'remarks', 'needed_date', 'needed_time_from', 'needed_time_to'];

    public function details() {
        return $this->hasMany('App\OrderDetails', 'id', 'order_id');
    }

}
