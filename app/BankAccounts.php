<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccounts extends Model
{

    protected $table = 'bank_accounts';
    public $timestamps = false;
    protected $fillable = ['name', 'type', 'detail_type', 'currency', 'balance'];

}
