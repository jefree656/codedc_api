<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderDetails extends Model
{

    protected $table = 'purchase_order_details';

    protected $fillable = ['material_id', 'purchase_order_id', 'project_id', 'category_id', 'inventory_id','supplier_id',  'qty','unit','weight','order_status','notes','unit_price','subtotal', 'received_at', 'received_by' ,'shipped_by', 'used', 'for_delivery', 'delivery_date', 'car_details', 'delivered_by', 'received_by2','delivered_at'];

    // public function order() {
    // 	return $this->belongsTo('App\Order', 'order_id', 'order_id');
    // }

}
