<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{

    protected $table = 'order_details';

    protected $fillable = ['product_id', 'order_id', 'supplier_id', 'payment_id', 'container_id', 'carrier', 'notes', 'po_status', 'qty','preset_unit','preset_qty','order_status','remarks','unit_price','total_price', 'weight', 'length', 'width', 'height', 'requested_at', 'requested_by', 'prepared_at', 'prepared_by' ,'shipped_by', 'paid', 'is_receive', 'for_delivery', 'delivery_date', 'car_details', 'delivered_by', 'received_by','delivered_at', 'paynotes', 'hidden'];

    public function order() {
    	return $this->belongsTo('App\Order', 'order_id', 'order_id');
    }

}
