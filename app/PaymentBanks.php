<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentBanks extends Model
{

	protected $table = 'payment_banks';
    public $timestamps = false;
    protected $fillable = ['id', 'payment_id', 'bank_id', 'amount'];

}
