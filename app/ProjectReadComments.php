<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectReadComments extends Model
{

    protected $table = 'project_read_comments';
		public $timestamps = false;
    protected $fillable = ['comment_id', 'user_id', 'is_read'];


}
